// --- Day 18: Snailfish ---

// You descend into the ocean trench and encounter some snailfish.
// They say they saw the sleigh keys!
// They'll even tell you which direction the keys went if
// you help one of the smaller snailfish with his math homework.

// Snailfish numbers aren't like regular numbers.
// Instead, every snailfish number is a pair - an ordered list of two elements.
// Each element of the pair can be either a regular number or another pair.

// Pairs are written as [x,y], where x and y are the elements within the pair.
// Here are some example snailfish numbers, one snailfish number per line:

// [1,2]
// [[1,2],3]
// [9,[8,7]]
// [[1,9],[8,5]]
// [[[[1,2],[3,4]],[[5,6],[7,8]]],9]
// [[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]
// [[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]

// This snailfish homework is about addition. To add two snailfish numbers,
// form a pair from the left and right parameters of the addition operator.
// For example, [1,2] + [[3,4],5] becomes [[1,2],[[3,4],5]].

// There's only one problem:
// snailfish numbers must always be reduced, and the process of adding two snailfish numbers
// can result in snailfish numbers that need to be reduced.

// To reduce a snailfish number,
// you must repeatedly do the first action in this list that applies to the snailfish number:

//     If any pair is nested inside four pairs, the leftmost such pair explodes.
//     If any regular number is 10 or greater, the leftmost such regular number splits.

// Once no action in the above list applies, the snailfish number is reduced.

// During reduction, at most one action applies,
// after which the process returns to the top of the list of actions.
// For example, if split produces a pair that meets the explode criteria,
// that pair explodes before other splits occur.

// To explode a pair,
// the pair's left value is added to the first regular number to the left of the exploding pair (if any), and
// the pair's right value is added to the first regular number to the right of the exploding pair (if any).
// Exploding pairs will always consist of two regular numbers.
// Then, the entire exploding pair is replaced with the regular number 0.

// Here are some examples of a single explode action:

//     [[[[[9,8],1],2],3],4] becomes [[[[0,9],2],3],4]
//          (the 9 has no regular number to its left, so it is not added to any regular number).
//     [7,[6,[5,[4,[3,2]]]]] becomes [7,[6,[5,[7,0]]]]
//          (the 2 has no regular number to its right, and so it is not added to any regular number).
//     [[6,[5,[4,[3,2]]]],1] becomes [[6,[5,[7,0]]],3].
//     [[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]] becomes [[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]
//           (the pair [3,2] is unaffected because the pair [7,3] is further to the left;
//           [3,2] would explode on the next action).
//     [[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]] becomes [[3,[2,[8,0]]],[9,[5,[7,0]]]].

// To split a regular number, replace it with a pair;
// the left element of the pair should be the regular number divided by two and rounded down, while
// the right element of the pair should be the regular number divided by two and rounded up.
// For example, 10 becomes [5,5], 11 becomes [5,6], 12 becomes [6,6], and so on.

// Here is the process of finding the reduced result of [[[[4,3],4],4],[7,[[8,4],9]]] + [1,1]:

// after addition: [[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]
// after explode:  [[[[0,7],4],[7,[[8,4],9]]],[1,1]]
// after explode:  [[[[0,7],4],[15,[0,13]]],[1,1]]
// after split:    [[[[0,7],4],[[7,8],[0,13]]],[1,1]]
// after split:    [[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]
// after explode:  [[[[0,7],4],[[7,8],[6,0]]],[8,1]]

// Once no reduce actions apply, the snailfish number that remains is
// the actual result of the addition operation: [[[[0,7],4],[[7,8],[6,0]]],[8,1]].

// The homework assignment involves adding up a list of snailfish numbers (your puzzle input).
// The snailfish numbers are each listed on a separate line.
// Add the first snailfish number and the second, then
// add that result and the third, then
// add that result and the fourth,
// and so on until all numbers in the list have been used once.

// For example, the final sum of this list is [[[[1,1],[2,2]],[3,3]],[4,4]]:

// [1,1]
// [2,2]
// [3,3]
// [4,4]

// The final sum of this list is [[[[3,0],[5,3]],[4,4]],[5,5]]:

// [1,1]
// [2,2]
// [3,3]
// [4,4]
// [5,5]

// The final sum of this list is [[[[5,0],[7,4]],[5,5]],[6,6]]:

// [1,1]
// [2,2]
// [3,3]
// [4,4]
// [5,5]
// [6,6]

// Here's a slightly larger example:

// [[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
// [7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
// [[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
// [[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
// [7,[5,[[3,8],[1,4]]]]
// [[2,[2,2]],[8,[8,1]]]
// [2,9]
// [1,[[[9,3],9],[[9,0],[0,7]]]]
// [[[5,[7,4]],7],1]
// [[[[4,2],2],6],[8,7]]

// The final sum [[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]] is found after adding up the above snailfish numbers:

//   [[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
// + [7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
// = [[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]

//   [[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]
// + [[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
// = [[[[6,7],[6,7]],[[7,7],[0,7]]],[[[8,7],[7,7]],[[8,8],[8,0]]]]

//   [[[[6,7],[6,7]],[[7,7],[0,7]]],[[[8,7],[7,7]],[[8,8],[8,0]]]]
// + [[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
// = [[[[7,0],[7,7]],[[7,7],[7,8]]],[[[7,7],[8,8]],[[7,7],[8,7]]]]

//   [[[[7,0],[7,7]],[[7,7],[7,8]]],[[[7,7],[8,8]],[[7,7],[8,7]]]]
// + [7,[5,[[3,8],[1,4]]]]
// = [[[[7,7],[7,8]],[[9,5],[8,7]]],[[[6,8],[0,8]],[[9,9],[9,0]]]]

//   [[[[7,7],[7,8]],[[9,5],[8,7]]],[[[6,8],[0,8]],[[9,9],[9,0]]]]
// + [[2,[2,2]],[8,[8,1]]]
// = [[[[6,6],[6,6]],[[6,0],[6,7]]],[[[7,7],[8,9]],[8,[8,1]]]]

//   [[[[6,6],[6,6]],[[6,0],[6,7]]],[[[7,7],[8,9]],[8,[8,1]]]]
// + [2,9]
// = [[[[6,6],[7,7]],[[0,7],[7,7]]],[[[5,5],[5,6]],9]]

//   [[[[6,6],[7,7]],[[0,7],[7,7]]],[[[5,5],[5,6]],9]]
// + [1,[[[9,3],9],[[9,0],[0,7]]]]
// = [[[[7,8],[6,7]],[[6,8],[0,8]]],[[[7,7],[5,0]],[[5,5],[5,6]]]]

//   [[[[7,8],[6,7]],[[6,8],[0,8]]],[[[7,7],[5,0]],[[5,5],[5,6]]]]
// + [[[5,[7,4]],7],1]
// = [[[[7,7],[7,7]],[[8,7],[8,7]]],[[[7,0],[7,7]],9]]

//   [[[[7,7],[7,7]],[[8,7],[8,7]]],[[[7,0],[7,7]],9]]
// + [[[[4,2],2],6],[8,7]]
// = [[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]

// To check whether it's the right answer, the snailfish teacher only checks the magnitude of the final sum.
// The magnitude of a pair is 3 times the magnitude of its left element plus 2 times the magnitude of its right element.
// The magnitude of a regular number is just that number.

// For example,
// the magnitude of [9,1] is 3*9 + 2*1 = 29;
// the magnitude of [1,9] is 3*1 + 2*9 = 21.
// Magnitude calculations are recursive: the magnitude of [[9,1],[1,9]] is 3*29 + 2*21 = 129.

// Here are a few more magnitude examples:

//     [[1,2],[[3,4],5]] becomes 143.
//     [[[[0,7],4],[[7,8],[6,0]]],[8,1]] becomes 1384.
//     [[[[1,1],[2,2]],[3,3]],[4,4]] becomes 445.
//     [[[[3,0],[5,3]],[4,4]],[5,5]] becomes 791.
//     [[[[5,0],[7,4]],[5,5]],[6,6]] becomes 1137.
//     [[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]] becomes 3488.

// So, given this example homework assignment:

// [[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
// [[[5,[2,8]],4],[5,[[9,9],0]]]
// [6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
// [[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
// [[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
// [[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
// [[[[5,4],[7,7]],8],[[8,3],8]]
// [[9,3],[[9,9],[6,[4,9]]]]
// [[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
// [[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]

// The final sum is:

// [[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]

// The magnitude of this final sum is 4140.

// Add up all of the snailfish numbers from the homework assignment in the order they appear.
// What is the magnitude of the final sum?

//Notes:
// - Reduced numbers have at most 4 levels, and no numbers >= 10
// - Adding adds 1 level, so looking to at most 5 levels suffices
// - reductions works loke this:
//    loop:
//       reading from the left: if a number is at level 5:
//           explode it
//           continue
//       reading from the left, if a digit >= 10:
//           split it.
//           continue
//       break

use num_integer::Integer;
use std::fs;
use std::io::{self, BufRead};
use std::time::Instant;

const INPUTFN: &str = "input18.txt";

fn main() {
    let file = fs::File::open(INPUTFN).expect(&format!("Cannot find file '{}'\n", INPUTFN));
    let input: Vec<String> = io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect();
    let input2 = input.iter().map(|s| s.as_ref()).collect::<Vec<&str>>();
    let now = Instant::now();
    println!(
        "Part1: {} in {} ms",
        part1(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
    let now = Instant::now();
    println!(
        "Part2: {} in {} ms",
        part2(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
}

fn part1(input: &Vec<&str>) -> u64 {
    let mut num1: Option<Number> = None;
    for line in input {
        let other = Number::from_str(line);
        let anum = num1.map_or_else(|| other.clone(), |v: Number| v.add(&other));
        num1 = Some(anum);
    }
    let added = num1.unwrap();
    added.magnitude()
}

fn part2(input: &Vec<&str>) -> u64 {
    let numbers: Vec<Number> = input.iter().map(|s| Number::from_str(s)).collect();
    let mut high = 0;
    for x in &numbers {
        for y in &numbers {
            let add1 = x.add(y).magnitude();
            let add2 = y.add(x).magnitude();
            high = high.max(add1).max(add2);
        }
    }
    high
}

/// Number looks a lot like a Vec<char> except that
/// a number character can be larger then 9, e.g. after explosion
#[derive(PartialEq, Debug, Clone)]
struct Number {
    seq: Vec<NumC>,
}

use crate::NumC::{Close, Comma, Open, D};

#[derive(PartialEq, Debug, Copy, Clone)]
enum NumC {
    D(u8),
    Open,
    Comma,
    Close,
}

impl Number {
    /// This function does a 1 tot 1 mapping from string to a vector of NumC
    /// Only regular numbers < 10 are expected in the input, but there are no checks
    /// So an invalid sequence can get 'parsed'
    fn from_str(input: &str) -> Number {
        let mut result = vec![];
        let mut value: Option<u8> = None;
        for (n, c) in input.chars().enumerate() {
            match c {
                '[' => {
                    result.push(Open);
                }
                ']' => {
                    if let Some(v) = value {
                        result.push(D(v));
                        value = None;
                    }
                    result.push(Close);
                }
                ',' => {
                    if let Some(v) = value {
                        result.push(D(v));
                        value = None;
                    }
                    result.push(Comma);
                }
                cc if cc.is_digit(10) => {
                    let digit = cc.to_digit(10).unwrap() as u8;
                    let v = value.map_or(digit, |v| v * 10 + digit);
                    value = Some(v);
                }
                _ => panic!("Illegal character at position {}", n),
            }
        }
        Number { seq: result }
    }

    fn add(&self, other: &Number) -> Self {
        let mut added: Vec<NumC> = vec![];
        added.push(Open);
        added.extend(&self.seq);
        added.push(Comma);
        added.extend(&other.seq);
        added.push(Close);
        let mut num = Number { seq: added };
        num.reduce();
        num
    }

    /// Magnitude determines the magnitude of self
    fn magnitude(&self) -> u64 {
        // println!("{:?}", self);
        self.magn_part(0).0
    }

    /// magn_part determines the magnitude of the Number starting
    /// at index start in self
    /// The magnitude is returned as well as the index of the NumC __after__ the Close
    /// At entry,start must be pointing at an Open
    fn magn_part(&self, start: usize) -> (u64, usize) {
        // Base case: we have not a pair but a D(v)
        if let D(v) = self.seq[start] {
            return (v as u64, start + 1); // The Close or Comma after the D(v)
        }
        // Here we are dealing with a pair
        let (left, end1) = self.magn_part(start + 1);
        assert!(self.seq[end1] == Comma);
        let (right, end2) = self.magn_part(end1 + 1);
        assert!(self.seq[end2] == Close);
        (3 * left as u64 + 2 * right as u64, end2 + 1) // The Close or Comma after the own Close
    }

    /// Reduce a Number using explosions and splits
    /// After this, Number is in reduced form
    fn reduce(&mut self) {
        loop {
            if self.do_1_explosion() {
                continue;
            }
            if self.do_split() {
                continue;
            }
            break;
        }
    }

    /// Do a single explosion, if need be
    /// Returns the fact that an explosion was performed
    fn do_1_explosion(&mut self) -> bool {
        let mut lvl = 0;
        let mut pos = None;
        for (n, c) in self.seq.iter().enumerate() {
            lvl = match *c {
                Open => lvl + 1,
                Close => lvl - 1,
                _ => lvl,
            };
            if lvl == 5 {
                pos = Some(n);
                break;
            }
        }
        if pos.is_none() {
            return false;
        }
        let pos = pos.unwrap();
        //         [ N , N , ]
        //   pos + 0 1 2 3 4 5
        let v1 = match self.seq[pos + 1] {
            // The left value to add to the previous number
            D(v) => v,
            _ => panic!("No value at pos + 1"),
        };
        let v2 = match self.seq[pos + 3] {
            // The right value to add to the next number
            D(v) => v,
            _ => panic!("No value at pos + 3"),
        };

        if let Some(pos_prev) = self.seq[0..pos].iter().rposition(|c| matches!(*c, D(_))) {
            if let D(vleft) = self.seq[pos_prev] {
                // The old previous number
                self.seq[pos_prev] = D(v1 + vleft);
            }
        };

        if let Some(pos_next) = self.seq[pos + 5..].iter().position(|c| matches!(*c, D(_))) {
            let pos_next = pos + 5 + pos_next;
            if let D(vright) = self.seq[pos_next] {
                // The old next number
                self.seq[pos_next] = D(v2 + vright);
            }
        };
        // Exchange seq[pos..pos+6] with a 0
        {
            self.seq.drain(pos..pos + 5);
        } // Make sure the elements are dropped
        self.seq.insert(pos, D(0));
        true
    }

    fn do_split(&mut self) -> bool {
        if let Some(pos) = self.seq.iter().position(|c| matches!(*c, D(v) if v > 9)) {
            if let D(v0) = self.seq[pos] {
                if v0 > 9 {
                    let v1 = v0.div_floor(&2);
                    let v2 = v0.div_ceil(&2);
                    self.seq.remove(pos);
                    // Insert backward: the last item ends up in the front
                    self.seq.insert(pos, Close);
                    self.seq.insert(pos, D(v2));
                    self.seq.insert(pos, Comma);
                    self.seq.insert(pos, D(v1));
                    self.seq.insert(pos, Open);
                    return true;
                }
            }
        }
        false
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXPLODE1: (&str, &str) = ("[[[[[9,8],1],2],3],4]", "[[[[0,9],2],3],4]");
    const EXPLODE2: (&str, &str) = ("[7,[6,[5,[4,[3,2]]]]]", "[7,[6,[5,[7,0]]]]");
    const EXPLODE3: (&str, &str) = ("[[6,[5,[4,[3,2]]]],1]", "[[6,[5,[7,0]]],3]");
    const EXPLODE4: (&str, &str) = (
        "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]",
        "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]",
    );
    const EXPLODE5: (&str, &str) = (
        "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]",
        "[[3,[2,[8,0]]],[9,[5,[7,0]]]]",
    );
    const SPLIT1: (&str, &str) = (
        "[[[[0,7],4],[15,[0,13]]],[1,1]]",
        "[[[[0,7],4],[[7,8],[0,13]]],[1,1]]",
    );
    const SPLIT2: (&str, &str) = (
        "[[[[0,7],4],[[7,8],[0,13]]],[1,1]]",
        "[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]",
    );

    const REDUCE1: (&str, &str) = (
        "[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]",
        "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]",
    );

    const ADD1: (&str, &str) = (
        "[[[[4,3],4],4],[7,[[8,4],9]]]
         [1,1]",
        "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]",
    );

    const MAGNITUDE1: (&str, u64) = ("[9,1]", 29);
    const MAGNITUDE2: (&str, u64) = ("[[9,1],[1,9]]", 129);
    const MAGNITUDE3: (&str, u64) = ("[[1,2],[[3,4],5]]", 143);
    const MAGNITUDE4: (&str, u64) = ("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]", 1384);
    const MAGNITUDE5: (&str, u64) = ("[[[[1,1],[2,2]],[3,3]],[4,4]]", 445);
    const MAGNITUDE6: (&str, u64) = ("[[[[3,0],[5,3]],[4,4]],[5,5]]", 791);
    const MAGNITUDE7: (&str, u64) = ("[[[[5,0],[7,4]],[5,5]],[6,6]]", 1137);
    const MAGNITUDE8: (&str, u64) = (
        "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]",
        3488,
    );

    const ASSIGNMENT1: &str = "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
    [[[5,[2,8]],4],[5,[[9,9],0]]]
    [6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
    [[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
    [[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
    [[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
    [[[[5,4],[7,7]],8],[[8,3],8]]
    [[9,3],[[9,9],[6,[4,9]]]]
    [[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
    [[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]";

    #[test]
    fn explodes1() {
        let (num1, num2) = test_explode(EXPLODE1);
        assert_eq!(num2, num1);
    }
    #[test]
    fn explodes2() {
        let (num1, num2) = test_explode(EXPLODE2);
        assert_eq!(num2, num1);
    }

    #[test]
    fn explodes3() {
        let (num1, num2) = test_explode(EXPLODE3);
        assert_eq!(num2, num1);
    }

    #[test]
    fn explodes4() {
        let (num1, num2) = test_explode(EXPLODE4);
        assert_eq!(num2, num1);
    }

    #[test]
    fn explodes5() {
        let (num1, num2) = test_explode(EXPLODE5);
        assert_eq!(num2, num1);
    }

    #[test]
    fn split1() {
        let (num1, num2) = test_split(SPLIT1);
        assert_eq!(num2, num1);
    }

    #[test]
    fn split2() {
        let (num1, num2) = test_split(SPLIT2);
        assert_eq!(num2, num1);
    }

    #[test]
    fn reduce1() {
        let (num1, num2) = test_reduce(REDUCE1);
        assert_eq!(num2, num1);
    }

    #[test]
    fn add1() {
        let (num1, num2) = test_add(ADD1);
        assert_eq!(num2, num1);
    }

    #[test]
    fn magn1() {
        let (inp, num2) = MAGNITUDE1;
        let num1 = Number::from_str(inp);
        assert_eq!(num2, num1.magnitude());
    }

    #[test]
    fn magn2() {
        let (inp, num2) = MAGNITUDE2;
        let num1 = Number::from_str(inp);
        assert_eq!(num2, num1.magnitude());
    }

    #[test]
    fn magn3() {
        let (inp, num2) = MAGNITUDE3;
        let num1 = Number::from_str(inp);
        assert_eq!(num2, num1.magnitude());
    }

    #[test]
    fn magn4() {
        let (inp, num2) = MAGNITUDE4;
        let num1 = Number::from_str(inp);
        assert_eq!(num2, num1.magnitude());
    }

    #[test]
    fn magn5() {
        let (inp, num2) = MAGNITUDE5;
        let num1 = Number::from_str(inp);
        assert_eq!(num2, num1.magnitude());
    }

    #[test]
    fn magn6() {
        let (inp, num2) = MAGNITUDE6;
        let num1 = Number::from_str(inp);
        assert_eq!(num2, num1.magnitude());
    }

    #[test]
    fn magn7() {
        let (inp, num2) = MAGNITUDE7;
        let num1 = Number::from_str(inp);
        assert_eq!(num2, num1.magnitude());
    }

    #[test]
    fn magn8() {
        let (inp, num2) = MAGNITUDE8;
        let num1 = Number::from_str(inp);
        assert_eq!(num2, num1.magnitude());
    }

    #[test]
    fn test_part1() {
        let commands: Vec<&str> = ASSIGNMENT1.lines().map(|s| s.trim()).collect();
        assert_eq!(4140, part1(&commands));
    }

    #[test]
    fn test_part2() {
        let commands: Vec<&str> = ASSIGNMENT1.lines().map(|s| s.trim()).collect();
        assert_eq!(3993, part2(&commands));
    }

    fn test_explode(input: (&str, &str)) -> (Number, Number) {
        let mut num1 = Number::from_str(input.0);
        let num2 = Number::from_str(input.1);
        num1.do_1_explosion();
        return (num1, num2);
    }

    fn test_split(input: (&str, &str)) -> (Number, Number) {
        let mut num1 = Number::from_str(input.0);
        let num2 = Number::from_str(input.1);
        num1.do_split();
        return (num1, num2);
    }

    fn test_reduce(input: (&str, &str)) -> (Number, Number) {
        let mut num1 = Number::from_str(input.0);
        let num2 = Number::from_str(input.1);
        num1.reduce();
        return (num1, num2);
    }

    fn test_add(input: (&str, &str)) -> (Number, Number) {
        let mut num1: Option<Number> = None;
        for line in input.0.lines() {
            let line = line.trim();
            println!("{}", line);
            let other = Number::from_str(line);
            println!("{:?}", other);
            let anum = num1.map_or_else(|| other.clone(), |v: Number| v.add(&other));
            num1 = Some(anum);
        }
        let num2 = Number::from_str(input.1);
        (num1.unwrap(), num2)
    }
}

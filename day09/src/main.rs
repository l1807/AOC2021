// --- Day 9: Smoke Basin ---

// These caves seem to be lava tubes. Parts are even still volcanically active;
// small hydrothermal vents release smoke into the caves that slowly settles like rain.

// If you can model how the smoke flows through the caves, you might be able to avoid it and be that much safer.
// The submarine generates a heightmap of the floor of the nearby caves for you (your puzzle input).

// Smoke flows to the lowest point of the area it's in.
// For example, consider the following heightmap:

// 2199943210
// 3987894921
// 9856789892
// 8767896789
// 9899965678

// Each number corresponds to the height of a particular location,
// where 9 is the highest and 0 is the lowest a location can be.

// Your first goal is to find the low points - the locations that are lower than any of its adjacent locations.
// Most locations have four adjacent locations (up, down, left, and right);
// locations on the edge or corner of the map have three or two adjacent locations, respectively.
// (Diagonal locations do not count as adjacent.)

// In the above example, there are four low points, all highlighted:
// two are in the first row (a 1 and a 0), one is in the third row (a 5),
// and one is in the bottom row (also a 5).
// All other locations on the heightmap have some lower adjacent location, and so are not low points.

// The risk level of a low point is 1 plus its height.
// In the above example, the risk levels of the low points are 2, 1, 6, and 6.
// The sum of the risk levels of all low points in the heightmap is therefore 15.

// Find all of the low points on your heightmap. What is the sum of the risk levels of all low points on your heightmap?

// --- Part Two ---

// Next, you need to find the largest basins so you know what areas are most important to avoid.

// A basin is all locations that eventually flow downward to a single low point.
// Therefore, every low point has a basin, although some basins are very small.
// Locations of height 9 do not count as being in any basin, and all other locations will always be part of exactly one basin.

// The size of a basin is the number of locations within the basin, including the low point.
// The example above has four basins.

// The top-left basin, size 3:

// 2199943210
// 3987894921
// 9856789892
// 8767896789
// 9899965678

// The top-right basin, size 9:

// 2199943210
// 3987894921
// 9856789892
// 8767896789
// 9899965678

// The middle basin, size 14:

// 2199943210
// 3987894921
// 9856789892
// 8767896789
// 9899965678

// The bottom-right basin, size 9:

// 2199943210
// 3987894921
// 9856789892
// 8767896789
// 9899965678

// Find the three largest basins and multiply their sizes together.
// In the above example, this is 9 * 14 * 9 = 1134.

// What do you get if you multiply together the sizes of the three largest basins?

use std::collections::VecDeque;
use std::fs;
use std::io::{self, BufRead};

const INPUTFN: &str = "input9.txt";

fn main() {
    let file = fs::File::open(INPUTFN).expect(&format!("Cannot find file '{}'\n", INPUTFN));
    let input: Vec<String> = io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect();
    let input2 = input.iter().map(|s| s.as_ref()).collect::<Vec<&str>>();
    println!("Part1: {}", part1(&input2));
    println!("Part2: {}", part2(&input2));
}

fn part1(input: &Vec<&str>) -> i32 {
    let grid = build_grid(input);
    let lows = get_lows(&grid);
    lows.iter().map(|(x, y)| grid[*y][*x] as i32 + 1).sum()
}

fn part2(input: &Vec<&str>) -> i32 {
    let grid = build_grid(input);
    let mut basins = get_basin_sizes(&grid);
    basins.sort();
    basins.reverse();
    basins[0] * basins[1] * basins[2]
}

fn build_grid(input: &Vec<&str>) -> Vec<Vec<u8>> {
    let ysz = input.len();
    let mut grid: Vec<Vec<u8>> = Vec::with_capacity(ysz);
    for &line in input {
        grid.push(
            line.chars()
                .map(|c| c.to_digit(10).unwrap() as u8)
                .collect(),
        )
    }
    grid
}

fn get_lows(grid: &Vec<Vec<u8>>) -> Vec<(usize, usize)> {
    let mut lows: Vec<(usize, usize)> = vec![];
    let xsz = grid[0].len();
    let ysz = grid.len();
    for y in 0..ysz {
        for x in 0..xsz {
            let val = grid[y][x];
            let val_u = if y > 0 { grid[y - 1][x] } else { u8::MAX };
            let val_d = if y < ysz - 1 { grid[y + 1][x] } else { u8::MAX };
            let val_l = if x > 0 { grid[y][x - 1] } else { u8::MAX };
            let val_r = if x < xsz - 1 { grid[y][x + 1] } else { u8::MAX };
            if val < val_u && val < val_d && val < val_l && val < val_r {
                // This is a low point
                lows.push((x, y))
            }
        }
    }
    lows
}

fn get_basin_sizes(grid: &Vec<Vec<u8>>) -> Vec<i32> {
    let szx = grid[0].len();
    let szy = grid.len();
    let mut basingrid: Vec<Vec<i32>> = Vec::with_capacity(szy);
    for _ in 0..grid.len() {
        let row: Vec<i32> = vec![-1; szx];
        basingrid.push(row)
    }

    let lows = get_lows(grid);
    let mut basins: Vec<i32> = Vec::new();
    for (n, low) in lows.iter().enumerate() {
        let mut que: VecDeque<(usize, usize)> = VecDeque::new(); // places to visit
        let mut count = 0;
        que.push_back(*low);
        while let Some((x, y)) = que.pop_front() {
            // Check for end of search path
            if grid[y][x] == 9 || basingrid[y][x] >= 0 {
                continue;
            }
            // Add this point to the basin
            basingrid[y][x] = n as i32;
            count += 1;
            // Try to expand the basin
            if x > 0 {
                que.push_back((x - 1, y));
            }
            if x < szx - 1 {
                que.push_back((x + 1, y));
            }
            if y > 0 {
                que.push_back((x, y - 1));
            }
            if y < szy - 1 {
                que.push_back((x, y + 1));
            }
        }
        basins.push(count);
    }
    basins
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        let input = "2199943210
             3987894921
             9856789892
             8767896789
             9899965678";
        let commands: Vec<&str> = input.lines().map(|s| s.trim()).collect();
        assert_eq!(15, part1(&commands));
    }

    #[test]
    fn test2() {
        let input = "2199943210
             3987894921
             9856789892
             8767896789
             9899965678";
        let commands: Vec<&str> = input.lines().map(|s| s.trim()).collect();
        assert_eq!(1134, part2(&commands));
    }
}

// --- Day 15: Chiton ---

// You've almost reached the exit of the cave, but the walls are getting closer together.
// Your submarine can barely still fit, though;
// the main problem is that the walls of the cave are covered in chitons,
// and it would be best not to bump any of them.

// The cavern is large, but has a very low ceiling, restricting your motion to two dimensions.
// The shape of the cavern resembles a square;
// a quick scan of chiton density produces a map of risk level throughout the cave (your puzzle input).
// For example:

// 1163751742
// 1381373672
// 2136511328
// 3694931569
// 7463417111
// 1319128137
// 1359912421
// 3125421639
// 1293138521
// 2311944581

// You start in the top left position, your destination is the bottom right position,
// and you cannot move diagonally.
// The number at each position is its risk level;
// to determine the total risk of an entire path, add up the risk levels of each position you enter
// (that is, don't count the risk level of your starting position unless you enter it;
// leaving it adds no risk to your total).

// Your goal is to find a path with the lowest total risk.
// In this example, a path with the lowest total risk is highlighted here:

// 1163751742
// 1381373672
// 2136511328
// 3694931569
// 7463417111
// 1319128137
// 1359912421
// 3125421639
// 1293138521
// 2311944581

// The total risk of this path is 40 (the starting position is never entered, so its risk is not counted).

// What is the lowest total risk of any path from the top left to the bottom right?

// --- Part Two ---

// Now that you know how to find low-risk paths in the cave, you can try to find your way out.

// The entire cave is actually five times larger in both dimensions than you thought;
// the area you originally scanned is just one tile in a 5x5 tile area that forms the full map.
// Your original map tile repeats to the right and downward;
// each time the tile repeats to the right or downward,
// all of its risk levels are 1 higher than the tile immediately up or left of it.
// However, risk levels above 9 wrap back around to 1.
// So, if your original map had some position with a risk level of 8,
// then that same position on each of the 25 total tiles would be as follows:

// 8 9 1 2 3
// 9 1 2 3 4
// 1 2 3 4 5
// 2 3 4 5 6
// 3 4 5 6 7

// Each single digit above corresponds to the example position with a value of 8 on the top-left tile.
// Because the full map is actually five times larger in both dimensions,
// that position appears a total of 25 times, once in each duplicated tile, with the values shown above.

// Here is the full five-times-as-large version of the first example above,
// with the original map in the top left corner highlighted:

// (....)

// Equipped with the full map, you can now find a path from
// the top left corner to the bottom right corner with the lowest total risk:

// (....)

// The total risk of this path is 315 (the starting position is still never entered, so its risk is not counted).

// Using the full map, what is the lowest total risk of any path from the top left to the bottom right?

use std::collections::{HashMap, HashSet}; //HashMap, HashSet, VecDeque};
use std::fs;
use std::io::{self, BufRead};
use std::time::Instant;

const INPUTFN: &str = "input15.txt";

fn main() {
    let file = fs::File::open(INPUTFN).expect(&format!("Cannot find file '{}'\n", INPUTFN));
    let input: Vec<String> = io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect();
    let input2 = input.iter().map(|s| s.as_ref()).collect::<Vec<&str>>();
    let now = Instant::now();
    println!(
        "Part1: {} in {} ms",
        part1(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
    let now = Instant::now();
    println!(
        "Part2: {} in {} ms",
        part2(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
}

fn part1(input: &Vec<&str>) -> i32 {
    let maze = Grid::from_strings(&input);
    maze.find_min_risk() as i32
}

fn part2(input: &Vec<&str>) -> i32 {
    let maze = Grid::from_strings(&input);
    let maze = maze.expand(5);
    maze.find_min_risk() as i32
}

#[derive(Debug, Clone)]
struct Grid {
    start: Point,
    end: Point,
    xsz: usize,
    ysz: usize,
    pts: HashMap<Point, PointData>,
}

#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone)]
struct Point {
    x: usize,
    y: usize,
}

#[derive(Debug, Clone)]
struct PointData {
    risk: u32,         // risk added to get to this point
    buren: Vec<Point>, // All accessible neighbours
}

impl Grid {
    fn from_strings(input: &Vec<&str>) -> Grid {
        let mut map: HashMap<Point, PointData> = HashMap::new();
        let ysz = input.len();
        let xsz = input[0].len();
        for (y, &line) in input.iter().enumerate() {
            for (x, riskc) in line.chars().enumerate() {
                let risk = riskc.to_digit(10).unwrap();
                let buren = Self::find_neighbours(
                    &Point { x, y },
                    &Point { x: 0, y: 0 },
                    &Point {
                        x: xsz - 1,
                        y: ysz - 1,
                    },
                );
                map.insert(Point { x, y }, PointData { risk, buren });
            }
        }
        Grid {
            start: Point { x: 0, y: 0 },
            end: Point {
                x: xsz - 1,
                y: ysz - 1,
            },
            xsz,
            ysz,
            pts: map,
        }
    }

    fn find_neighbours(p: &Point, pmin: &Point, pmax: &Point) -> Vec<Point> {
        let mut buren: Vec<Point> = vec![];
        if p.x < pmax.x {
            buren.push(Point { x: p.x + 1, y: p.y });
        }
        if p.y < pmax.y {
            buren.push(Point { x: p.x, y: p.y + 1 });
        }
        if p.x > pmin.x {
            buren.push(Point { x: p.x - 1, y: p.y });
        }
        if p.y > pmin.y {
            buren.push(Point { x: p.x, y: p.y - 1 });
        }
        buren
    }

    // Expand the grid factor times in2 directions
    fn expand(&self, factor: usize) -> Self {
        let mut table = HashMap::new();
        let maxp = Point {
            x: self.xsz * factor - 1,
            y: self.ysz * factor - 1,
        };
        for nx in 0..factor {
            for ny in 0..factor {
                let new_points = self.shift_table(nx, ny, &maxp);
                table.extend(new_points.into_iter());
            }
        }
        Grid {
            start: self.start,
            end: Point {
                x: self.end.x + (factor - 1) * self.xsz,
                y: self.end.y + (factor - 1) * self.ysz,
            },
            xsz: self.xsz * factor,
            ysz: self.ysz * factor,
            pts: table,
        }
    }

    /// shift_table creates a single new part of an expanded grid.__rust_force_expr!
    /// Only a new map from point to pointdata us created
    fn shift_table(&self, nx: usize, ny: usize, maxp: &Point) -> HashMap<Point, PointData> {
        let dx = nx * self.xsz;
        let dy = ny * self.ysz;
        let minp = Point { x: 0, y: 0 };
        let mut new_pts: HashMap<Point, PointData> = HashMap::with_capacity(self.pts.len());

        for (p, pd) in &self.pts {
            let newp = Point {
                x: p.x + dx,
                y: p.y + dy,
            };
            let risk = (pd.risk + (nx + ny) as u32 - 1) % 9 + 1;
            let buren = Self::find_neighbours(&newp, &minp, &maxp);
            new_pts.insert(newp, PointData { risk, buren });
        }
        // println!("Shift: {},{}: minp: {:?}, maxp: {:?}", nx, ny, minp, maxp);
        // for (p, pd) in new_pts.iter() {
        //     println!("({}, {}) -> {:?}", p.x, p.y, pd)
        // }
        new_pts
    }

    /// find_min_risk finds the path with minimum risk through te maze
    /// This is a Dijkstra search where each edge's weight is defined by the riskon entry
    /// end criteria for a path
    ///  - end reached: store minimum risk
    ///  - point on current path
    ///  - accumulated risk larger then current minimum risk
    /// To reduce the time to find a next visit-node (naively done by comparing the risks of ALL unvisited nodes),
    /// maintain a list of all nodes that received risk less then infinity.
    /// If a node is condsidered as a neighbour (buur), put a point in the considered set.
    /// Only searh for new nodes in the considered set. If a node is visite, it is removed again.

    fn find_min_risk(&self) -> u32 {
        let mut risks: HashMap<Point, u32> = self.pts.keys().map(|&k| (k, u32::MAX)).collect();
        let mut unvisited: HashSet<Point> = self.pts.keys().map(|&k| k).collect();
        let mut considered: HashSet<Point> = HashSet::new();
        let mut predecessor: HashMap<Point, Option<Point>> =
            self.pts.keys().map(|&k| (k, None)).collect();

        let mut curp = self.start;
        let mut cur_risk = 0;
        *risks.get_mut(&curp).unwrap() = 0;
        unvisited.remove(&curp);
        while !unvisited.is_empty() {
            let pd = &self.pts[&curp];
            for p in &pd.buren {
                if unvisited.contains(&p) {
                    let p_risk = risks.get_mut(&p).unwrap();
                    if *p_risk > cur_risk + self.pts[p].risk {
                        *p_risk = cur_risk + self.pts[p].risk;
                        *predecessor.get_mut(&p).unwrap() = Some(curp);
                        considered.insert(*p);
                    }
                }
            }
            // println!("{:?} -> {:?}", curp, considered);
            let (&newp, newr) = considered
                .iter()
                .map(|p| (p, risks[p]))
                .min_by(|(_, v1), (_, v2)| v1.cmp(v2))
                .unwrap();
            if newp == self.end {
                //self.print_risks(&risks);
                // self.print_path(&predecessor, &self.end);
                return newr;
            }
            curp = newp;
            cur_risk = newr;
            unvisited.remove(&curp);
            considered.remove(&curp);
        }
        panic!("No path found");
    }

    #[allow(dead_code)]
    fn print_table(&self) {
        println!("Table: {} x {}\n", self.xsz, self.ysz);
        for y in 0..self.ysz {
            print!("{:2}: ", y);
            for x in 0..self.xsz {
                let p = Point { x, y };
                if x > 0 && x % 10 == 0 {
                    print!(" ");
                }
                print!(" {:1}", self.pts[&p].risk);
            }
            println!("");
        }
    }

    #[allow(dead_code)]
    fn print_risks(&self, risks: &HashMap<Point, u32>) {
        println!("");
        for y in 0..self.ysz {
            print!("{:2}: ", y);
            for x in 0..self.xsz {
                let p = Point { x, y };
                if x > 0 && x % 10 == 0 {
                    print!(" | ");
                }
                print!(" {:3}", risks[&p]);
            }
            println!("");
        }
    }

    #[allow(dead_code)]
    fn print_path(&self, pres: &HashMap<Point, Option<Point>>, end: &Point) {
        println!("\nPath: ");
        let mut path: Vec<Point> = Vec::new();
        let mut curp = end;
        path.push(*end);
        while let Some(Some(p)) = pres.get(curp) {
            curp = p;
            path.push(*curp);
        }
        for p in path.iter().rev() {
            println!("{:?}", p);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT1: &str = "1163751742
    1381373672
    2136511328
    3694931569
    7463417111
    1319128137
    1359912421
    3125421639
    1293138521
    2311944581";

    #[test]
    fn test1() {
        let commands: Vec<&str> = TEST_INPUT1.lines().map(|s| s.trim()).collect();
        assert_eq!(40, part1(&commands));
    }

    #[test]
    fn test2() {
        let commands: Vec<&str> = TEST_INPUT1.lines().map(|s| s.trim()).collect();
        assert_eq!(315, part2(&commands));
    }
}

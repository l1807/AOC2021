// --- Day 4: Giant Squid ---

// You're already almost 1.5km (almost a mile) below the surface of the ocean,
// already so deep that you can't see any sunlight.
// What you can see, however, is a giant squid that has attached itself to the outside of your submarine.
// Maybe it wants to play bingo?

// Bingo is played on a set of boards each consisting of a 5x5 grid of numbers.
// Numbers are chosen at random, and the chosen number is marked on all boards on which it appears.
// (Numbers may not appear on all boards.)
// If all numbers in any row or any column of a board are marked, that board wins. (Diagonals don't count.)

// The submarine has a bingo subsystem to help passengers (currently, you and the giant squid) pass the time.
// It automatically generates a random order in which to draw numbers and
// a random set of boards (your puzzle input). For example:

// 7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

// 22 13 17 11  0
//  8  2 23  4 24
// 21  9 14 16  7
//  6 10  3 18  5
//  1 12 20 15 19

//  3 15  0  2 22
//  9 18 13 17  5
// 19  8  7 25 23
// 20 11 10 24  4
// 14 21 16 12  6

// 14 21 17 24  4
// 10 16 15  9 19
// 18  8 23 26 20
// 22 11 13  6  5
//  2  0 12  3  7

// After the first five numbers are drawn (7, 4, 9, 5, and 11), there are no winners,
// but the boards are marked as follows (shown here adjacent to each other to save space):

// 22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
//  8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
// 21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
//  6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
//  1 12 20 15 19        14 21 16 12  6         2  0 12  3  7

// After the next six numbers are drawn (17, 23, 2, 0, 14, and 21), there are still no winners:

// 22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
//  8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
// 21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
//  6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
//  1 12 20 15 19        14 21 16 12  6         2  0 12  3  7

// Finally, 24 is drawn:

// 22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
//  8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
// 21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
//  6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
//  1 12 20 15 19        14 21 16 12  6         2  0 12  3  7

// At this point, the third board wins because it has at least one complete row or column of marked numbers
// (in this case, the entire top row is marked: 14 21 17 24 4).

// The score of the winning board can now be calculated.
// Start by finding the sum of all unmarked numbers on that board; in this case, the sum is 188.
// Then, multiply that sum by the number that was just called when the board won, 24,
// to get the final score, 188 * 24 = 4512.

// To guarantee victory against the giant squid, figure out which board will win first.
// What will your final score be if you choose that board?

// --- Part Two ---
// On the other hand, it might be wise to try a different strategy: let the giant squid win.
// You aren't sure how many bingo boards a giant squid could play at once,
// so rather than waste time counting its arms, the safe thing to do is
// to figure out which board will win last and choose that one.
// That way, no matter which boards it picks, it will win for sure.

// In the above example, the second board is the last to win,
// which happens after 13 is eventually called and its middle column is completely marked.
// If you were to keep playing until this point, the second board would have
// a sum of unmarked numbers equal to 148 for a final score of 148 * 13 = 1924.

// Figure out which board will win last.
// Once it wins, what would its final score be?

use std::collections::HashSet;
use std::fs;
use std::io::{self, BufRead};

const INPUTFN: &str = "input4.txt";

fn main() {
    let file = fs::File::open(INPUTFN).expect(&format!("Cannot find file '{}'\n", INPUTFN));
    let input: Vec<String> = io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect();
    let input2 = input.iter().map(|s| s.as_ref()).collect::<Vec<&str>>();
    println!("Part1: {}", part1(&input2));
    println!("Part2: {}", part2(&input2));
}

fn part1(input: &Vec<&str>) -> i32 {
    let rnd_nrs = get_random_numbers(input);

    let mut boards = getboards(input);
    play1(&mut boards, &rnd_nrs)
}

fn part2(input: &Vec<&str>) -> i32 {
    let rnd_nrs = get_random_numbers(input);
    let mut boards = getboards(input);

    play2(&mut boards, &rnd_nrs)
}

fn get_random_numbers(input: &Vec<&str>) -> Vec<i32> {
    input[0]
        .split(',')
        .map(|s| s.trim().parse::<i32>().unwrap())
        .collect()
}

fn getboards(input: &Vec<&str>) -> Vec<Board> {
    // Collect results here
    let mut boards: Vec<Board> = Vec::new();

    // lines for a single board
    let mut board_data: Vec<&str> = Vec::new();
    for line in input.iter().skip(1) {
        if line.trim().len() == 0 {
            if board_data.len() > 0 {
                boards.push(Board::new(&board_data));
                board_data = Vec::new();
            }
            continue;
        }
        board_data.push(line);
    }
    if board_data.len() > 0 {
        // :Load final board
        boards.push(Board::new(&board_data));
    }
    boards
}

fn play1(boards: &mut Vec<Board>, nums: &Vec<i32>) -> i32 {
    for num in nums.iter() {
        for board in boards.iter_mut() {
            if let Some(score) = board.play(*num) {
                return score.score * score.last_num;
            }
        }
    }
    0
}

fn play2(boards: &mut Vec<Board>, nums: &Vec<i32>) -> i32 {
    // Keep track of the indices of all boards that are complete
    let mut incomplete: HashSet<usize> = HashSet::with_capacity(boards.len());
    for n in 0..boards.len() {
        incomplete.insert(n);
    }

    for num in nums.iter() {
        for (b, board) in boards.iter_mut().enumerate() {
            if incomplete.contains(&b) {
                if let Some(score) = board.play(*num) {
                    incomplete.remove(&b);
                    if incomplete.is_empty() {
                        return score.score * score.last_num;
                    }
                }
            }
        }
    }
    0
}

#[derive(Debug)]
struct Board {
    sz: usize,
    rows: Vec<Vec<i32>>,
    done: Vec<Vec<bool>>,
}

struct Score {
    last_num: i32,
    score: i32,
}

impl Board {
    /// returns a new Board based on input.
    /// Input shall consist of size non-empty lines, each containing
    /// size numbers, seperated by whitespace
    fn new(input: &Vec<&str>) -> Self {
        let cleaned: Vec<&str> = input
            .iter()
            .map(|&s| s.trim())
            .filter(|&s| s.len() > 0)
            .collect();
        // eprintln!("Cleaned: {:?}", cleaned);
        let size = cleaned.len();
        let rows: Vec<Vec<i32>> = cleaned
            .iter()
            //.inspect(|&e| eprintln!("clean line: {}", *e))
            .map(|&s| {
                s.split_whitespace()
                    .map(|s| s.parse::<i32>().unwrap())
                    .collect::<Vec<i32>>()
            })
            .collect();
        let mut done: Vec<Vec<bool>> = Vec::new();
        for _ in 0..size {
            let done_row: Vec<bool> = vec![false; size];
            done.push(done_row);
        }
        Self {
            sz: size,
            rows: rows,
            done: done,
        }
    }

    /// Play 1 number. If the board clears, calculate Some(Score),
    /// else return None
    fn play(&mut self, inp: i32) -> Option<Score> {
        let mut done = false;
        'outer: for r in 0..self.sz {
            for c in 0..self.sz {
                if self.rows[r][c] == inp {
                    self.done[r][c] = true;
                    done = self.test_done(r, c);
                    break 'outer;
                }
            }
        }
        if done {
            let mut sum = 0;
            for r in 0..self.sz {
                for c in 0..self.sz {
                    if !self.done[r][c] {
                        sum += self.rows[r][c];
                    }
                }
            }
            let score: Score = Score {
                score: sum,
                last_num: inp,
            };
            return Some(score);
        }
        None
    }

    fn test_done(&self, row: usize, col: usize) -> bool {
        let mut done = true;
        for c in 0..self.sz {
            if !self.done[row][c] {
                done = false;
                break;
            }
        }
        if done {
            return true;
        }
        let mut done = true;
        for r in 0..self.sz {
            if !self.done[r][col] {
                done = false;
                break;
            }
        }
        if done {
            return true;
        }
        false
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        let input = "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

        22 13 17 11  0
         8  2 23  4 24
        21  9 14 16  7
         6 10  3 18  5
         1 12 20 15 19
        
         3 15  0  2 22
         9 18 13 17  5
        19  8  7 25 23
        20 11 10 24  4
        14 21 16 12  6
        
        14 21 17 24  4
        10 16 15  9 19
        18  8 23 26 20
        22 11 13  6  5
         2  0 12  3  7";
        let commands: Vec<&str> = input.lines().map(|s| s.trim()).collect();
        assert_eq!(4512, part1(&commands));
    }

    #[test]
    fn test2() {
        let input = "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

        22 13 17 11  0
         8  2 23  4 24
        21  9 14 16  7
         6 10  3 18  5
         1 12 20 15 19
        
         3 15  0  2 22
         9 18 13 17  5
        19  8  7 25 23
        20 11 10 24  4
        14 21 16 12  6
        
        14 21 17 24  4
        10 16 15  9 19
        18  8 23 26 20
        22 11 13  6  5
         2  0 12  3  7";
        let commands: Vec<&str> = input.lines().map(|s| s.trim()).collect();
        assert_eq!(1924, part2(&commands));
    }
}

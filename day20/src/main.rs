// --- Day 20: Trench Map ---

// With the scanners fully deployed, you turn their attention to mapping the floor of the ocean trench.

// When you get back the image from the scanners, it seems to just be random noise. 
// Perhaps you can combine an image enhancement algorithm and 
// the input image (your puzzle input) to clean it up a little.

// For example:

// ..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..##
// #..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###
// .######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#.
// .#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#.....
// .#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#..
// ...####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.....
// ..##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

// #..#.
// #....
// ##..#
// ..#..
// ..###

// The first section is the image enhancement algorithm. 
// It is normally given on a single line, but 
// it has been wrapped to multiple lines in this example for legibility. 
//The second section is the input image, 
// a two-dimensional grid of light pixels (#) and dark pixels (.).

// The image enhancement algorithm describes how to enhance an image by 
// simultaneously converting all pixels in the input image into an output image. 
// Each pixel of the output image is determined by looking at 
// a 3x3 square of pixels centered on the corresponding input image pixel. 
// So, to determine the value of the pixel at (5,10) in the output image, 
// nine pixels from the input image need to be considered:
// (4,9), (4,10), (4,11), (5,9), (5,10), (5,11), (6,9), (6,10), and (6,11). 
// These nine input pixels are combined into a single binary number that 
// is used as an index in the image enhancement algorithm string.

// For example, to determine the output pixel that corresponds to 
// the very middle pixel of the input image, the nine pixels marked by [...] would need to be considered:

// # . . # .
// #[. . .].
// #[# . .]#
// .[. # .].
// . . # # #

// Starting from the top-left and reading across each row, 
// these pixels are ..., then #.., then .#.; combining these forms ...#...#.. 
// By turning dark pixels (.) into 0 and light pixels (#) into 1, 
// the binary number 000100010 can be formed, which is 34 in decimal.

// The image enhancement algorithm string is exactly 512 characters long, 
// enough to match every possible 9-bit binary number. 
// The first few characters of the string (numbered starting from zero) are as follows:

// 0         10        20        30  34    40        50        60        70
// |         |         |         |   |     |         |         |         |
// ..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..##

// In the middle of this first group of characters, the character at index 34 can be found: #. 
// So, the output pixel in the center of the output image should be #, a light pixel.

// This process can then be repeated to calculate every pixel of the output image.

// Through advances in imaging technology, 
// the images being operated on here are infinite in size. 
// Every pixel of the infinite output image needs to be calculated exactly 
// based on the relevant pixels of the input image. 
// The small input image you have is only a small region of the actual infinite input image; 
// the rest of the input image consists of dark pixels (.). 
// For the purposes of the example, to save on space, only a portion of the infinite-sized input and output images will be shown.

// The starting input image, therefore, looks something like this, 
// with more dark pixels (.) extending forever in every direction not shown here:

// ...............
// ...............
// ...............
// ...............
// ...............
// .....#..#......
// .....#.........
// .....##..#.....
// .......#.......
// .......###.....
// ...............
// ...............
// ...............
// ...............
// ...............

// By applying the image enhancement algorithm to every pixel simultaneously, 
// the following output image can be obtained:

// ...............
// ...............
// ...............
// ...............
// .....##.##.....
// ....#..#.#.....
// ....##.#..#....
// ....####..#....
// .....#..##.....
// ......##..#....
// .......#.#.....
// ...............
// ...............
// ...............
// ...............

// Through further advances in imaging technology, 
// the above output image can also be used as an input image! 
// This allows it to be enhanced a second time:

// ...............
// ...............
// ...............
// ..........#....
// ....#..#.#.....
// ...#.#...###...
// ...#...##.#....
// ...#.....#.#...
// ....#.#####....
// .....#.#####...
// ......##.##....
// .......###.....
// ...............
// ...............
// ...............

// Truly incredible - now the small details are really starting to come through. 
// After enhancing the original input image twice, 35 pixels are lit.

// Start with the original input image and apply the image enhancement algorithm twice, 
// being careful to account for the infinite size of the images. 
// How many pixels are lit in the resulting image?

// Notes: 
// - In the example, algo bit 0 (for the infinite edges) is Off --> the infinite edge stauys off
//   In the input, algo bit 0 is On --> the infite edge lights up.
//   Algo bit 511 is Off, so after step 2 the infinite edge is off again.
//   So we must be able to encode the state beyond the edge
// - The picture grows 1 pixels on all sides at every step, before entering infinity.
// So for part 1 it is  easiest to let the picture start at (4,4) and end at bottom-right + (4,4), 
//   to make sure all relevant pixels are in range, including the interesting pixels of infinity 

use std::collections::{HashMap, HashSet}; //HashMap, HashSet, VecDeque};
use std::fs;
use std::io::{self, BufRead};
use std::time::Instant;

const INPUTFN: &str = "input20.txt";

fn main() {
    let file = fs::File::open(INPUTFN).expect(&format!("Cannot find file '{}'\n", INPUTFN));
    let input: Vec<String> = io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect();
    let input2 = input.iter().map(|s| s.as_ref()).collect::<Vec<&str>>();
    let now = Instant::now();
    println!(
        "Part1: {} in {} ms",
        part1(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
    let now = Instant::now();
    println!(
        "Part2: {} in {} ms",
        part2(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
}

fn part1(input: &Vec<&str>) -> usize {
    let algo: Vec<Pxl> = input[0].chars().map(|c| Pxl::from_c(&c)).collect();
    // for n1 in (0..512).step_by(32) {
    //     print!("{:3}: ", n1);
    //     for n2 in (0..32).step_by(4) {
    //         for n3 in 0..4 {
    //             print!("{}", algo[n1 + n2 + n3])
    //         }
    //         print!(" ");
    //     }
    //     println!();
    // }
    let pict = Picture::from_str(&input[2..], 3);
    pict.show();
    let pict = pict.enhance(&algo);
    pict.show();
    let pict = pict.enhance(&algo);
    pict.show();
    pict.count_on()
}

fn part2(input: &Vec<&str>) -> usize {
    let algo: Vec<Pxl> = input[0].chars().map(|c| Pxl::from_c(&c)).collect();
    let mut pict = Picture::from_str(&input[2..], 3);
    for _ in 0..50 {
        pict = pict.enhance(&algo);
    }
    pict.count_on()
}

#[derive(Copy, Clone, PartialEq)]
enum Pxl {
    On,
    Off,
}

struct Picture {
    pxls: Vec<Vec<Pxl>>,    // The picture itself
    szx: usize,             // x size of the picture
    szy:usize,              // y size of the picture
    inf: Pxl,               // The value of infintity pixels outside the picture: all On or all Off
}

impl Picture {
    /// Create a picture from an iterable of strings
    /// Infinity will be set to Off
    fn from_str(inp: &[&str], margin: usize) -> Self {
        let szx = 2 * margin + inp[0].len();
        let szy = 2 * margin + inp.len();
        let mut pxls = Vec::with_capacity(szy);

        let edge_line = vec![Pxl::Off; szx];
        let edge = ".".repeat(margin);

        for _ in 0..margin {
            pxls.push(edge_line.clone());
        }
        for &line in inp {
            let pxl_line: Vec<Pxl> = edge.chars().chain(line.chars()).chain(edge.chars()).map(|c|Pxl::from_c(&c)).collect();
            pxls.push(pxl_line);
        }
        for _ in 0..margin {
            pxls.push(edge_line.clone());
        }
        Picture {pxls, szx, szy, inf: Pxl::Off }
    }

    /// Create a picture of given size, all pixels Off
    fn new(szx: usize, szy: usize) -> Self {
        let line: Vec<Pxl> = std::iter::repeat(Pxl::Off).take(szx).collect(); 
        let pxls  = std::iter::repeat(line).take(szy).collect::<Vec<Vec<Pxl>>>();
        Self { pxls, szx, szy, inf: Pxl::Off }
    }

    /// Return an enhanced copy of self
    /// The new picture will grow 1 pixel at each side
    /// Infinity can change, depending on algo[0] and algo[1<<9-1] 
    fn enhance(&self, algo: &Vec<Pxl>) -> Self {
        let mut result = Picture::new(self.szx+2, self.szy+2);
        for x in 0..result.szx {
            for y in 0..result.szy {
                result.pxls[y][x] = result.enhance_pxl(&self, x as isize, y as isize, &algo);
            }
        }
        result.inf = match self.inf {
            Pxl::On => algo[(1<<9)-1],
            Pxl::Off => algo[0],
        };
        result
    }

    /// Enhance the pixel at (x, y) of self
    /// The original is used as source, and each of its pixels sits at (x+1, y+1) in the original
    /// For each pixel (x, y) of self the following original pixels are inspected:
    /// (x-2, y-2), (x-1, y-2) (x, y-2)
    /// (x-2, y-1), (x-1, y-1),(x, y-1)
    /// (x-2, y  ), (x-1, y  ),(x, y  ); 
    /// If any of these coordinates are < 0 or >= original.self.szx or szy, original.inf is used
    fn enhance_pxl(&self, original: &Picture, x: isize, y: isize, algo: &Vec<Pxl>) -> Pxl {
        let mut algo_index = 0;
        // print!("**orig_size: ({},{})", original.szx, original.szy);
        for offsety in -2..=0_isize {
            for offsetx in -2..=0_isize {
                let pxl = if x + offsetx < 0 || 
                             x + offsetx >= original.szx as isize ||
                             y + offsety < 0 ||
                             y + offsety >= original.szy as isize  { original.inf } 
                        else { original.pxls[(y+offsety) as usize][(x+offsetx) as usize]};
                algo_index = (algo_index << 1) + (if pxl == Pxl::On { 1 } else { 0 });
                // print!(" ({}, {}): {} ", x + offsetx, y + offsety, algo_index);
            }
        }
        // println!(" --> ({},{}): {}: {}", x, y, algo_index, algo[algo_index]);

        algo[algo_index]
    }

    fn count_on(&self) -> usize {
        let mut cnt = 0;
        for line in self.pxls.iter() {
            cnt += line.iter().filter(|&p| *p == Pxl::On).count()
        }
        cnt
    }

    /// Print the picture to stdout
    fn show(&self) {
        println!();
        for  line in &self.pxls {
            for &pxl in line {
                print!("{}", pxl);
            }
            println!();
        }
        println!("Infinity: {}", self.inf)
    }
}


impl Pxl {
    fn from_c(c: &char) -> Self {
        match c {
            '#' => Self::On,
            _ => Self::Off,
        }
    }
}

impl std::fmt::Display for Pxl {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
      write!(f, "{}", if *self == Pxl::On { '#' }  else {'.'})
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT1: &str = 
    "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#
    
    #..#.
    #....
    ##..#
    ..#..
    ..###";

    #[test]
    fn test1() {
        let commands: Vec<&str> = TEST_INPUT1.lines().map(|s| s.trim()).collect();
        assert_eq!(35, part1(&commands));
    }

    #[test]
    fn test2() {
        let commands: Vec<&str> = TEST_INPUT1.lines().map(|s| s.trim()).collect();
        assert_eq!(3351, part2(&commands));
    }
}

// --- Day 5: Hydrothermal Venture ---

// You come across a field of hydrothermal vents on the ocean floor!
// These vents constantly produce large, opaque clouds, so it would be best to avoid them if possible.

// They tend to form in lines;
// the submarine helpfully produces a list of nearby lines of vents (your puzzle input) for you to review.
// For example:

// 0,9 -> 5,9
// 8,0 -> 0,8
// 9,4 -> 3,4
// 2,2 -> 2,1
// 7,0 -> 7,4
// 6,4 -> 2,0
// 0,9 -> 2,9
// 3,4 -> 1,4
// 0,0 -> 8,8
// 5,5 -> 8,2

// Each line of vents is given as a line segment in the format x1,y1 -> x2,y2
// where x1,y1 are the coordinates of one end the line segment
// and x2,y2 are the coordinates of the other end.
// These line segments include the points at both ends. In other words:

//     An entry like 1,1 -> 1,3 covers points 1,1, 1,2, and 1,3.
//     An entry like 9,7 -> 7,7 covers points 9,7, 8,7, and 7,7.

// For now, only consider horizontal and vertical lines: lines where either x1 = x2 or y1 = y2.

// So, the horizontal and vertical lines from the above list would produce the following diagram:

// .......1..
// ..1....1..
// ..1....1..
// .......1..
// .112111211
// ..........
// ..........
// ..........
// ..........
// 222111....

// In this diagram, the top left corner is 0,0 and the bottom right corner is 9,9.
// Each position is shown as the number of lines which cover that point or . if no line covers that point.
// The top-left pair of 1s, for example, comes from 2,2 -> 2,1;
// the very bottom row is formed by the overlapping lines 0,9 -> 5,9 and 0,9 -> 2,9.

// To avoid the most dangerous areas, you need to determine the number of points where at least two lines overlap.
//In the above example, this is anywhere in the diagram with a 2 or larger - a total of 5 points.

// Consider only horizontal and vertical lines.
// At how many points do at least two lines overlap?

// --- Part Two ---

// Unfortunately, considering only horizontal and vertical lines doesn't give you the full picture;
// you need to also consider diagonal lines.

// Because of the limits of the hydrothermal vent mapping system,
// the lines in your list will only ever be horizontal, vertical, or a diagonal line at exactly 45 degrees.
// In other words:
//    An entry like 1,1 -> 3,3 covers points 1,1, 2,2, and 3,3.
//    An entry like 9,7 -> 7,9 covers points 9,7, 8,8, and 7,9.
//
// Considering all lines from the above example would now produce the following diagram:

// 1.1....11.
// .111...2..
// ..2.1.111.
// ...1.2.2..
// .112313211
// ...1.2....
// ..1...1...
// .1.....1..
// 1.......1.
// 222111....

// You still need to determine the number of points where at least two lines overlap.
// In the above example, this is still anywhere in the diagram with a 2 or larger - now a total of 12 points.

// Consider all of the lines. At how many points do at least two lines overlap?

use std::cmp;
use std::collections::HashMap;
use std::fs;
use std::io::{self, BufRead};

const INPUTFN: &str = "input5.txt";

fn main() {
    let file = fs::File::open(INPUTFN).expect(&format!("Cannot find file '{}'\n", INPUTFN));
    let input: Vec<String> = io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect();
    let input2 = input.iter().map(|s| s.as_ref()).collect::<Vec<&str>>();
    println!("Part1: {}", part1(&input2));
    println!("Part2: {}", part2(&input2));
}

fn part1(input: &Vec<&str>) -> i32 {
    part_all(input, true)
}

fn part2(input: &Vec<&str>) -> i32 {
    part_all(input, false)
}

fn part_all(input: &Vec<&str>, horzvert: bool) -> i32 {
    let minx = 0_usize;
    let mut maxx = usize::MIN;
    let miny = 0_usize;
    let mut maxy = usize::MIN;
    let mut ventlines: Vec<VentLine> = Vec::with_capacity(input.len());
    for line in input.iter() {
        let vl = VentLine::from_str(line);
        if horzvert && !vl.is_hor_vert() {
            continue;
        }
        maxx = cmp::max(maxx, vl.x_max());
        maxy = cmp::max(maxy, vl.y_max());
        ventlines.push(vl);
    }
    //for vl in &ventlines {
    //    eprintln!("ventline: {:?}", vl);
    //}
    let mut grid: HashMap<Point, usize> = HashMap::with_capacity((maxx + 1) * (maxy + 1));
    for vl in ventlines.iter() {
        for p in vl.gridset().iter() {
            let number = grid.entry(*p).or_insert(0);
            *number += 1;
        }
    }
    //eprintln!("Grid: {:?}", grid);
    grid.iter().filter(|(_, &num)| num >= 2).count() as i32
}

#[derive(Hash, Eq, PartialEq, Debug, Copy, Clone)]
struct Point {
    x: usize,
    y: usize,
}

impl Point {
    /// from_string produces a Point from a string like "x1,y1"
    fn from_str(input: &str) -> Self {
        if let Some((x, y)) = input.split_once(",") {
            let x = x.parse::<usize>().unwrap();
            let y = y.parse::<usize>().unwrap();
            Point { x: x, y: y }
        } else {
            Point { x: 0, y: 0 }
        }
    }
}

#[derive(Debug)]
struct VentLine {
    end1: Point,
    end2: Point,
}

impl VentLine {
    /// from_string produces a VentLine from a string like "x1,y1 -> x2,y2"
    fn from_str(input: &str) -> Self {
        let (left, right) = input.split_once(" -> ").unwrap();
        let end1 = Point::from_str(left);
        let end2 = Point::from_str(right);
        Self {
            end1: end1,
            end2: end2,
        }
    }

    /// Returns the lowest x of the VentLine
    fn x_min(&self) -> usize {
        cmp::min(self.end1.x, self.end2.x)
    }
    /// Returns the highest x of the VentLine
    fn x_max(&self) -> usize {
        cmp::max(self.end1.x, self.end2.x)
    }
    /// Returns the lowest y of the VentLine
    fn y_min(&self) -> usize {
        cmp::min(self.end1.y, self.end2.y)
    }
    /// Returns the highest y of the VentLine
    fn y_max(&self) -> usize {
        cmp::max(self.end1.y, self.end2.y)
    }

    /// Returns true if the VentLine is horizontal or vertical
    fn is_hor_vert(&self) -> bool {
        self.end1.x == self.end2.x || self.end1.y == self.end2.y
    }

    /// Returns the set of points that are visitedby the VentLine
    fn gridset(&self) -> Vec<Point> {
        let mut result = vec![];
        let deltax: isize = self.end2.x as isize - self.end1.x as isize;
        let deltay: isize = self.end2.y as isize - self.end1.y as isize;

        if deltax == 0 {
            // vertical
            for y in self.y_min()..=self.y_max() {
                result.push(Point {
                    x: self.end1.x,
                    y: y,
                })
            }
        } else if deltay == 0 {
            // horizontal
            for x in self.x_min()..=self.x_max() {
                result.push(Point {
                    x: x,
                    y: self.end1.y,
                })
            }
        } else if deltax == deltay {
            // diagonal 2 directions
            for x in self.x_min()..=self.x_max() {
                result.push(Point {
                    x: x,
                    y: self.y_min() + (x - self.x_min()),
                });
            }
        } else if deltax == -deltay {
            // diagonal 2 other directions
            for x in self.x_min()..=self.x_max() {
                result.push(Point {
                    x: x,
                    y: self.y_max() - (x - self.x_min()),
                });
            }
        } else {
            eprintln!("Error: deltax {}, deltay: {}", deltax, deltay);
        }
        result
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        let input = "0,9 -> 5,9
        8,0 -> 0,8
        9,4 -> 3,4
        2,2 -> 2,1
        7,0 -> 7,4
        6,4 -> 2,0
        0,9 -> 2,9
        3,4 -> 1,4
        0,0 -> 8,8
        5,5 -> 8,2";
        let commands: Vec<&str> = input.lines().map(|s| s.trim()).collect();
        assert_eq!(5, part1(&commands));
    }

    #[test]
    fn test2() {
        let input = "0,9 -> 5,9
        8,0 -> 0,8
        9,4 -> 3,4
        2,2 -> 2,1
        7,0 -> 7,4
        6,4 -> 2,0
        0,9 -> 2,9
        3,4 -> 1,4
        0,0 -> 8,8
        5,5 -> 8,2";
        let commands: Vec<&str> = input.lines().map(|s| s.trim()).collect();
        assert_eq!(12, part2(&commands));
    }
}

// --- Day 8: Seven Segment Search ---

// You barely reach the safety of the cave when the whale smashes into the cave mouth, collapsing it.
// Sensors indicate another exit to this cave at a much greater depth, so you have no choice but to press on.
// As your submarine slowly makes its way through the cave system,
// you notice that the four-digit seven-segment displays in your submarine are malfunctioning;
// they must have been damaged during the escape. You'll be in a lot of trouble without them, so you'd better figure out what's wrong.

// Each digit of a seven-segment display is rendered by turning on or off any of seven segments named a through g:

//   0:      1:      2:      3:      4:
//  aaaa    ....    aaaa    aaaa    ....
// b    c  .    c  .    c  .    c  b    c
// b    c  .    c  .    c  .    c  b    c
//  ....    ....    dddd    dddd    dddd
// e    f  .    f  e    .  .    f  .    f
// e    f  .    f  e    .  .    f  .    f
//  gggg    ....    gggg    gggg    ....

//   5:      6:      7:      8:      9:
//  aaaa    aaaa    aaaa    aaaa    aaaa
// b    .  b    .  .    c  b    c  b    c
// b    .  b    .  .    c  b    c  b    c
//  dddd    dddd    ....    dddd    dddd
// .    f  e    f  .    f  e    f  .    f
// .    f  e    f  .    f  e    f  .    f
//  gggg    gggg    ....    gggg    gggg

// So, to render a 1, only segments c and f would be turned on; the rest would be off.
// To render a 7, only segments a, c, and f would be turned on.

// The problem is that the signals which control the segments have been mixed up on each display.
// The submarine is still trying to display numbers by producing output on signal wires a through g,
// but those wires are connected to segments randomly.
// Worse, the wire/segment connections are mixed up separately for each four-digit display!
// (All of the digits within a display use the same connections, though.)

// So, you might know that only signal wires b and g are turned on,
// but that doesn't mean segments b and g are turned on:
// the only digit that uses two segments is 1, so it must mean segments c and f are meant to be on.
// With just that information, you still can't tell which wire (b/g) goes to which segment (c/f).
// For that, you'll need to collect more information.

// For each display, you watch the changing signals for a while,
// make a note of all ten unique signal patterns you see,
// and then write down a single four digit output value (your puzzle input).
// Using the signal patterns, you should be able to work out which pattern corresponds to which digit.

// For example, here is what you might see in a single entry in your notes:

// acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab |
// cdfeb fcadb cdfeb cdbaf

// (The entry is wrapped here to two lines so it fits; in your notes, it will all be on a single line.)

// Each entry consists of ten unique signal patterns, a | delimiter, and finally the four digit output value.
// Within an entry, the same wire/segment connections are used
// (but you don't know what the connections actually are).
// The unique signal patterns correspond to the ten different ways the submarine
// tries to render a digit using the current wire/segment connections.
// Because 7 is the only digit that uses three segments,
// dab in the above example means that to render a 7, signal lines d, a, and b are on.
// Because 4 is the only digit that uses four segments, eafb means that to render a 4, signal lines e, a, f, and b are on.

// Using this information, you should be able to work out
// which combination of signal wires corresponds to each of the ten digits.
// Then, you can decode the four digit output value.
// Unfortunately, in the above example, all of the digits in the output value
// (cdfeb fcadb cdfeb cdbaf) use five segments and are more difficult to deduce.

// For now, focus on the easy digits. Consider this larger example:

// be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb |
// fdgacbe cefdb cefbgd gcbe
// edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec |
// fcgedb cgb dgebacf gc
// fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef |
// cg cg fdcagb cbg
// fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega |
// efabcd cedba gadfec cb
// aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga |
// gecf egdcabf bgf bfgea
// fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf |
// gebdcfa ecba ca fadegcb
// dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf |
// cefg dcbef fcge gbcadfe
// bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd |
// ed bcgafe cdgba cbgef
// egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg |
// gbdfcae bgc cg cgb
// gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc |
// fgae cfgab fg bagce

// Because the digits 1, 4, 7, and 8 each use a unique number of segments,
// you should be able to tell which combinations of signals correspond to those digits.
// Counting only digits in the output values (the part after | on each line), in the above example,
// there are 26 instances of digits that use a unique number of segments (highlighted above).

// In the output values, how many times do digits 1, 4, 7, or 8 appear?

// --- Part Two ---

// Through a little deduction, you should now be able to determine the remaining digits.
// Consider again the first example above:

// acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab |
// cdfeb fcadb cdfeb cdbaf

// After some careful analysis, the mapping between signal wires and segments
// only make sense in the following configuration:

//  dddd
// e    a
// e    a
//  ffff
// g    b
// g    b
//  cccc

// So, the unique signal patterns would correspond to the following digits:

//     acedgfb: 8
//     cdfbe: 5
//     gcdfa: 2
//     fbcad: 3
//     dab: 7
//     cefabd: 9
//     cdfgeb: 6
//     eafb: 4
//     cagedb: 0
//     ab: 1

// Then, the four digits of the output value can be decoded:

//     cdfeb: 5
//     fcadb: 3
//     cdfeb: 5
//     cdbaf: 3

// Therefore, the output value for this entry is 5353.

// Following this same process for each entry in the second, larger example above, the output value of each entry can be determined:

//     fdgacbe cefdb cefbgd gcbe: 8394
//     fcgedb cgb dgebacf gc: 9781
//     cg cg fdcagb cbg: 1197
//     efabcd cedba gadfec cb: 9361
//     gecf egdcabf bgf bfgea: 4873
//     gebdcfa ecba ca fadegcb: 8418
//     cefg dcbef fcge gbcadfe: 4548
//     ed bcgafe cdgba cbgef: 1625
//     gbdfcae bgc cg cgb: 8717
//     fgae cfgab fg bagce: 4315

// Adding all of the output values in this larger example produces 61229.

// For each entry, determine all of the wire/segment connections and decode the four-digit output values. What do you get if you add up all of the output values?

// Notes:
// - Equal digits may be coded with different strings where the letters are in a different sequence.
//   Perhaps it is usefull to sort the letter for each digit for easier comparison
// - Part 1:  1 4 7 8 have 2, 4, 3, 7 segments resp. so 5 and 6 are not interesting, 0 and 1 do not occur
// - Part 2: decoding steps:
//      1 (cf) -> cf
//      7 (cfa) -> a!
//      4 (cf bd) -> bd
//      9 (enige 6-voudige zonder onbekende (eg)) -> e!
//      6 (6 voudige zonder 1 van cf) -> c! -> f!
//      0 (6 voudige zonder 1 van bd)  -> d! -> b!
//      Blijft over: g!

use lazy_static::lazy_static;
use std::collections::HashMap;
use std::fs;
use std::io::{self, BufRead};

const INPUTFN: &str = "input8.txt";

fn main() {
    let file = fs::File::open(INPUTFN).expect(&format!("Cannot find file '{}'\n", INPUTFN));
    let input: Vec<String> = io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect();
    let input2 = input.iter().map(|s| s.as_ref()).collect::<Vec<&str>>();
    println!("Part1: {}", part1(&input2));
    println!("Part2: {}", part2(&input2));
}

fn part1(input: &Vec<&str>) -> i32 {
    let mut result = 0;
    for &line in input.iter() {
        let (_, right) = line.split_once(" | ").unwrap();
        result += right
            .split_whitespace()
            .filter(|&s| s.len() != 5 && s.len() != 6)
            .count();
    }
    result as i32
}

fn part2(input: &Vec<&str>) -> i32 {
    let mut result = 0;
    for &line in input.iter() {
        let (left, right) = line.split_once(" | ").unwrap();
        let codemap = decode(left);
        // eprintln!("decoded: {:?}", codemap);
        result += get_value(right, codemap);
    }
    result
}

/// Decodes the left side of a problem line.
/// Returns a HashMap with code keys to the actually meant segment letter
/// i.e. code letters from input to standard letters as in the first 'drawing'
fn decode(left: &str) -> HashMap<char, char> {
    let mut result: HashMap<char, char> = HashMap::new();
    let digits_unsorted: Vec<&str> = left.split_whitespace().collect();

    let mut digits: Vec<String> = Vec::with_capacity(10);
    for &dg in digits_unsorted.iter() {
        let mut chars: Vec<char> = dg.chars().collect();
        chars.sort();
        digits.push(String::from_iter(chars))
    }

    let cf_1: Vec<char> = digits.iter().find(|&s| s.len() == 2).unwrap().chars().collect();
    let acf_7: Vec<char> = digits.iter().find(|&s| s.len() == 3).unwrap().chars().collect();
    let bdcf_4: Vec<char> = digits.iter().find(|&s| s.len() == 4).unwrap().chars().collect();
    let dg_len6: Vec<_> = digits.iter().filter(|&s| s.len() == 6).collect();
    let bd: Vec<char> = bdcf_4.iter().filter(|&c| !cf_1.contains(c)).map(|&c| c).collect();

    let mut missing6:Vec<char> = Vec::with_capacity(3);
    for &len6 in dg_len6.iter() {
        let missing = "abcdefg".find(|c| !len6.contains(c)).unwrap();
        let missing_char = "abcdefg".as_bytes()[missing] as char;
        missing6.push(missing_char);
    }

    // The a segement is in acf_7 but not in cf_1
    let a = *acf_7.iter().find(|&c| !cf_1.contains(c)).unwrap();
    result.insert(a, 'a');
    
    let mut abcdf = bdcf_4.clone();
    abcdf.push(a);
    for ch in &missing6 {
        if !abcdf.contains(ch) {
            // This 6length item misses a char that is not in abcdf --> e from the 9
            result.insert(*ch, 'e');
            continue;
        }
        if cf_1.contains(ch) {
            // The missing character is one of c or f -> c from the 6
            result.insert(*ch, 'c');
            let other = cf_1.iter().find(|&c| *c != *ch).unwrap();
            result.insert(*other, 'f');
            continue;
        }
        if bd.contains(ch) {
            // The missing character is one of b or d -> d from the 0
            result.insert(*ch, 'd');
            let other = bd.iter().find(|&c| *c != *ch).unwrap();
            result.insert(*other, 'b');
            continue;
        }
    } 
    for ch in "abcdefg".chars() {
        if !result.contains_key(&ch) {
            result.insert(ch, 'g');
        }
    }
    result 
}

fn get_value(input: &str, codemap: HashMap<char, char>) -> i32 {
    let digits: Vec<&str> = input.split_whitespace().collect();  // 4 digits
    let mut decoded: Vec<i32> = Vec::with_capacity(4);

    for &dg in digits.iter() {
        let mut chars: Vec<char> = dg.chars().map(|c| codemap[&c] ).collect();
        chars.sort();
        let key = String::from_iter(chars);
        let value = DIGITS.get(&key).unwrap();
        decoded.push(*value)
    }
    // eprintln!("digits: {:?}", decoded);
    decoded.iter().fold(0, |acc, v| acc*10 + v )
}

// DIGITS provides a map of sorted(!) strings to the encoded digit
lazy_static! {
    static ref DIGITS: HashMap<String, i32> = {
        let mut map = HashMap::new();
        map.insert("abcefg".into(), 0);
        map.insert("cf".into(), 1);
        map.insert("acdeg".into(), 2);
        map.insert("acdfg".into(), 3);
        map.insert("bcdf".into(), 4);
        map.insert("abdfg".into(), 5);
        map.insert("abdefg".into(), 6);
        map.insert("acf".into(), 7);
        map.insert("abcdefg".into(), 8);
        map.insert("abcdfg".into(), 9);
        map
    };
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        let input =
            "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
       edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
       fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
       fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
       aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
       fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
       dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
       bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
       egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
       gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";
        let commands: Vec<&str> = input.lines().map(|s| s.trim()).collect();
        assert_eq!(26, part1(&commands));
    }

    #[test]
    fn test2a() {
        let input =
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf";
        let commands: Vec<&str> = input.lines().map(|s| s.trim()).collect();
        assert_eq!(5353, part2(&commands));
    }

    #[test]
    fn test2b() {
        let input =
            "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
       edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
       fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
       fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
       aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
       fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
       dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
       bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
       egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
       gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";
        let commands: Vec<&str> = input.lines().map(|s| s.trim()).collect();
        assert_eq!(61229, part2(&commands));
    }

}

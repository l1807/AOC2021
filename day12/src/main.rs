// --- Day 12: Passage Pathing ---

// With your submarine's subterranean subsystems subsisting suboptimally,
// the only way you're getting out of this cave anytime soon is by finding a path yourself.
// Not just a path - the only way to know if you've found the best path is to find all of them.

// Fortunately, the sensors are still mostly working,
// and so you build a rough map of the remaining caves (your puzzle input). For example:

// start-A
// start-b
// A-c
// A-b
// b-d
// A-end
// b-end

// This is a list of how all of the caves are connected.
// You start in the cave named start, and your destination is the cave named end.
// An entry like b-d means that cave b is connected to cave d - that is, you can move between them.

// So, the above cave system looks roughly like this:

//     start
//     /   \
// c--A-----b--d
//     \   /
//      end

// Your goal is to find the number of distinct paths that start at start,
// end at end, and don't visit small caves more than once.
// There are two types of caves: big caves (written in uppercase, like A)
// and small caves (written in lowercase, like b).
// It would be a waste of time to visit any small cave more than once,
// but big caves are large enough that it might be worth visiting them multiple times.
// So, all paths you find should visit small caves at most once,
// and can visit big caves any number of times.

// Given these rules, there are 10 paths through this example cave system:

// start,A,b,A,c,A,end
// start,A,b,A,end
// start,A,b,end
// start,A,c,A,b,A,end
// start,A,c,A,b,end
// start,A,c,A,end
// start,A,end
// start,b,A,c,A,end
// start,b,A,end
// start,b,end

// (Each line in the above list corresponds to a single path; the caves visited by that path are listed in the order they are visited and separated by commas.)

// Note that in this cave system, cave d is never visited by any path: to do so, cave b would need to be visited twice (once on the way to cave d and a second time when returning from cave d), and since cave b is small, this is not allowed.

// Here is a slightly larger example:

// dc-end
// HN-start
// start-kj
// dc-start
// dc-HN
// LN-dc
// HN-end
// kj-sa
// kj-HN
// kj-dc

// The 19 paths through it are as follows:

// start,HN,dc,HN,end
// start,HN,dc,HN,kj,HN,end
// start,HN,dc,end
// start,HN,dc,kj,HN,end
// start,HN,end
// start,HN,kj,HN,dc,HN,end
// start,HN,kj,HN,dc,end
// start,HN,kj,HN,end
// start,HN,kj,dc,HN,end
// start,HN,kj,dc,end
// start,dc,HN,end
// start,dc,HN,kj,HN,end
// start,dc,end
// start,dc,kj,HN,end
// start,kj,HN,dc,HN,end
// start,kj,HN,dc,end
// start,kj,HN,end
// start,kj,dc,HN,end
// start,kj,dc,end

// Finally, this even larger example has 226 paths through it:

// fs-end
// he-DX
// fs-he
// start-DX
// pj-DX
// end-zg
// zg-sl
// zg-pj
// pj-he
// RW-he
// fs-DX
// pj-RW
// zg-RW
// start-pj
// he-WI
// zg-he
// pj-fs
// start-RW

// How many paths through this cave system are there that visit small caves at most once?

// Note: this is a path finding problem.
// - Push start to the queue, with empty state
// - while node in the queue
//      - record node as used (unless multi-accessable: then just don't record)
//        This is done automatically because the node is alreasy at the tail of its path
//      - end node?: record path found. Continue.
//      - any reachable nodes? No: continue.
//        An accessible node is adjacent to current and not in path or capital
//      - yes: Push them all into the queue. continue.
// - report found paths

// --- Part Two ---

// After reviewing the available paths, you realize you might have time to visit a single small cave twice.
// Specifically, big caves can be visited any number of times,
// a single small cave can be visited at most twice, and the remaining small caves can be visited at most once.
// However, the caves named start and end can only be visited exactly once each:
// once you leave the start cave, you may not return to it, and once you reach the end cave, the path must end immediately.

// Now, the 36 possible paths through the first example above are:

// start,A,b,A,b,A,c,A,end
// start,A,b,A,b,A,end
// start,A,b,A,b,end
// start,A,b,A,c,A,b,A,end
// start,A,b,A,c,A,b,end
// start,A,b,A,c,A,c,A,end
// start,A,b,A,c,A,end
// start,A,b,A,end
// start,A,b,d,b,A,c,A,end
// start,A,b,d,b,A,end
// start,A,b,d,b,end
// start,A,b,end
// start,A,c,A,b,A,b,A,end
// start,A,c,A,b,A,b,end
// start,A,c,A,b,A,c,A,end
// start,A,c,A,b,A,end
// start,A,c,A,b,d,b,A,end
// start,A,c,A,b,d,b,end
// start,A,c,A,b,end
// start,A,c,A,c,A,b,A,end
// start,A,c,A,c,A,b,end
// start,A,c,A,c,A,end
// start,A,c,A,end
// start,A,end
// start,b,A,b,A,c,A,end
// start,b,A,b,A,end
// start,b,A,b,end
// start,b,A,c,A,b,A,end
// start,b,A,c,A,b,end
// start,b,A,c,A,c,A,end
// start,b,A,c,A,end
// start,b,A,end
// start,b,d,b,A,c,A,end
// start,b,d,b,A,end
// start,b,d,b,end
// start,b,end

// The slightly larger example above now has 103 paths through it, and the even larger example now has 3509 paths through it.

// Given these new rules, how many paths through this cave system are there?

use std::collections::{HashMap, VecDeque};
use std::fs;
use std::io::{self, BufRead};
use std::time::Instant;

const INPUTFN: &str = "input12.txt";

fn main() {
    let file = fs::File::open(INPUTFN).expect(&format!("Cannot find file '{}'\n", INPUTFN));
    let input: Vec<String> = io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect();
    let input2 = input.iter().map(|s| s.as_ref()).collect::<Vec<&str>>();
    let now = Instant::now();
    println!(
        "Part1: {} in {} ms",
        part1(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
    let now = Instant::now();
    println!(
        "Part2: {} in {} ms",
        part2(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
}

/// State stores everything that must be recorded for a travel step.
/// In this case only the path traveled so far.
#[derive(Clone)]
struct State {
    path: Vec<String>, // The path traveled so far
    twice_done: bool,  // a small cave has been visited twice
}

fn part1(input: &Vec<&str>) -> i32 {
    let maze = build_maze(input);
    find_paths(&maze, false) as i32
}

fn part2(input: &Vec<&str>) -> i32 {
    let maze = build_maze(input);
    find_paths(&maze, true) as i32
}

/// build_maze builds a maze from the input data.
/// A maze is a HashMap with all nodes as the keys.
/// Each entry lists all nodes that can be reached from the key node.
/// For our bidirectional edges,this means that all edges occur twice in the maze
fn build_maze(input: &Vec<&str>) -> HashMap<String, Vec<String>> {
    let mut maze: HashMap<String, Vec<String>> = HashMap::new();
    for line in input.iter() {
        let (left, right) = line.split_once("-").unwrap();
        let targets = maze.entry(left.into()).or_insert(Vec::new());
        let trgt = right.into();
        if !targets.contains(&trgt) {
            targets.push(trgt)
        }
        let targets = maze.entry(right.into()).or_insert(Vec::new());
        let trgt = left.into();
        if !targets.contains(&trgt) {
            targets.push(trgt);
        }
    }
    return maze;
}

/// find_paths finds all paths.
/// the FIFO que holds the paths visited so far.
/// At ecah step:
/// - check for the end node
/// - find and push all reachable nodes that are capital or not yet in the path

fn find_paths(maze: &HashMap<String, Vec<String>>, may_twice: bool) -> usize {
    let mut paths: Vec<Vec<String>> = Vec::new();
    let mut que: VecDeque<State> = VecDeque::new();
    que.push_back(State {
        path: vec!["start".into()],
        twice_done: false,
    });
    while let Some(state) = que.pop_front() {
        let current = state.path.last().unwrap();
        if current == "end" {
            paths.push(state.path.clone());
            continue;
        }
        for next in &maze[current] {
            let avail = is_available(&state, &next, may_twice);
            if let Avail::Not = avail {
                continue;
            }
            let mut newstate = state.clone();
            newstate.path.push(next.clone());
            if let Avail::GoTwice = avail {
                newstate.twice_done = true;
            }
            que.push_back(newstate);
        }
    }
    //eprintln!("");
    //for p in &paths {
    //    eprintln!("{}", p.join("-"))
    //}
    paths.len()
}

enum Avail {
    Capital,
    Unvisited,
    GoTwice,
    Not,
}

fn is_available(state: &State, node: &String, may_twice: bool) -> Avail {
    if all_capitals(node) {
        Avail::Capital
    } else if !state.path.contains(node) {
        Avail::Unvisited
    } else if may_twice && !state.twice_done && node != "end" && node != "start" {
        Avail::GoTwice
    } else {
        Avail::Not
    }
}

fn all_capitals(node: &str) -> bool {
    node.chars().all(|c| c.is_uppercase())
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT1: &str = "start-A
    start-b
    A-c
    A-b
    b-d
    A-end
    b-end";

    const TEST_INPUT2: &str = "dc-end
    HN-start
    start-kj
    dc-start
    dc-HN
    LN-dc
    HN-end
    kj-sa
    kj-HN
    kj-dc";

    const TEST_INPUT3: &str = "fs-end
    he-DX
    fs-he
    start-DX
    pj-DX
    end-zg
    zg-sl
    zg-pj
    pj-he
    RW-he
    fs-DX
    pj-RW
    zg-RW
    start-pj
    he-WI
    zg-he
    pj-fs
    start-RW";

    #[test]
    fn test1a() {
        let commands: Vec<&str> = TEST_INPUT1.lines().map(|s| s.trim()).collect();
        assert_eq!(10, part1(&commands));
    }

    #[test]
    fn test1b() {
        let commands: Vec<&str> = TEST_INPUT2.lines().map(|s| s.trim()).collect();
        assert_eq!(19, part1(&commands));
    }

    #[test]
    fn test1c() {
        let commands: Vec<&str> = TEST_INPUT3.lines().map(|s| s.trim()).collect();
        assert_eq!(226, part1(&commands));
    }

    #[test]
    fn test2a() {
        let commands: Vec<&str> = TEST_INPUT1.lines().map(|s| s.trim()).collect();
        assert_eq!(36, part2(&commands));
    }

    #[test]
    fn test2b() {
        let commands: Vec<&str> = TEST_INPUT2.lines().map(|s| s.trim()).collect();
        assert_eq!(103, part2(&commands));
    }

    #[test]
    fn test2c() {
        let commands: Vec<&str> = TEST_INPUT3.lines().map(|s| s.trim()).collect();
        assert_eq!(3509, part2(&commands));
    }
}

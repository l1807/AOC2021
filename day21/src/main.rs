// --- Day 21: Dirac Dice ---

// There's not much to do as you slowly descend to the bottom of the ocean.
// The submarine computer challenges you to a nice game of Dirac Dice.

// This game consists of a single die, two pawns, and a game board with
// a circular track containing ten spaces marked 1 through 10 clockwise.
// Each player's starting space is chosen randomly (your puzzle input). Player 1 goes first.

// Players take turns moving.
// On each player's turn, the player rolls the die three times and adds up the results.
// Then, the player moves their pawn that many times forward around the track
// (that is, moving clockwise on spaces in order of increasing value, wrapping back around to 1 after 10).
// So, if a player is on space 7 and they roll 2, 2, and 1,
// they would move forward 5 times, to spaces 8, 9, 10, 1, and finally stopping on 2.

// After each player moves, they increase their score by the value of the space their pawn stopped on.
// Players' scores start at 0.
// So, if the first player starts on space 7 and rolls a total of 5,
// they would stop on space 2 and add 2 to their score (for a total score of 2).
// The game immediately ends as a win for any player whose score reaches at least 1000.

// Since the first game is a practice game,
// the submarine opens a compartment labeled deterministic dice and a 100-sided die falls out.
// This die always rolls 1 first, then 2, then 3, and so on up to 100,
// after which it starts over at 1 again. Play using this die.

// For example, given these starting positions:

// Player 1 starting position: 4
// Player 2 starting position: 8

// This is how the game would go:

//     Player 1 rolls 1+2+3 and moves to space 10 for a total score of 10.
//     Player 2 rolls 4+5+6 and moves to space 3 for a total score of 3.
//     Player 1 rolls 7+8+9 and moves to space 4 for a total score of 14.
//     Player 2 rolls 10+11+12 and moves to space 6 for a total score of 9.
//     Player 1 rolls 13+14+15 and moves to space 6 for a total score of 20.
//     Player 2 rolls 16+17+18 and moves to space 7 for a total score of 16.
//     Player 1 rolls 19+20+21 and moves to space 6 for a total score of 26.
//     Player 2 rolls 22+23+24 and moves to space 6 for a total score of 22.

// ...after many turns...

//     Player 2 rolls 82+83+84 and moves to space 6 for a total score of 742.
//     Player 1 rolls 85+86+87 and moves to space 4 for a total score of 990.
//     Player 2 rolls 88+89+90 and moves to space 3 for a total score of 745.
//     Player 1 rolls 91+92+93 and moves to space 10 for a final score, 1000.

// Since player 1 has at least 1000 points, player 1 wins and the game ends.
// At this point, the losing player had 745 points and the die had been rolled a total of 993 times;
// 745 * 993 = 739785.

// Play a practice game using the deterministic 100-sided die.
// The moment either player wins, what do you get if
// you multiply the score of the losing player by the number of times the die was rolled during the game?

use std::collections::{HashMap, VecDeque}; //HashMap, HashSet, VecDeque};
use std::fs;
use std::io::{self, BufRead};
use std::time::Instant;

const INPUTFN: &str = "input21.txt";

fn main() {
    let file = fs::File::open(INPUTFN).expect(&format!("Cannot find file '{}'\n", INPUTFN));
    let input: Vec<String> = io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect();
    let input2 = input.iter().map(|s| s.as_ref()).collect::<Vec<&str>>();
    let now = Instant::now();
    println!(
        "Part1: {} in {} ms",
        part1(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
    let now = Instant::now();
    println!(
        "Part2: {} in {} ms",
        part2(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
}

fn part1(input: &Vec<&str>) -> u64 {
    let mut pos: Vec<_> = input
        .iter()
        .map(|ln| {
            let (_, right) = ln.split_once(":").unwrap();
            right.trim().parse::<u64>().unwrap()
        })
        .collect();
    let mut points = vec![0; pos.len()];
    let mut dice = (1..=100).cycle();
    let mut throws = 0;
    let mut players = (0..pos.len()).cycle();
    while !points.iter().any(|&p| p >= 1000) {
        let player = players.next().unwrap();
        let advance = sum_next3(&mut dice);
        throws += 3;
        pos[player] = (pos[player] - 1 + advance) % 10 + 1;
        points[player] += pos[player];
    }
    if points[0] >= 1000 {
        println!(
            "Winner (1) with {} points. Loser (2) with {} points, after {} throws",
            points[0], points[1], throws
        );
        points[1] * throws
    } else {
        println!(
            "Winner (2) with {} points. Loser (1) with {} points, after {} throws",
            points[1], points[0], throws
        );
        points[0] * throws
    }
}

/// Every turn there are 3 throws with an outcome of 3, 4, 5, 6, 7, 8 or 9
/// These give rise to 1, 3, 6, 7, 6, 3, 1 identical universes
/// Each turn these universes multiply again by another factor, etc.
fn part2(input: &Vec<&str>) -> u64 {
    let pos: Vec<_> = input
        .iter()
        .map(|ln| {
            let (_, right) = ln.split_once(":").unwrap();
            right.trim().parse::<u8>().unwrap()
        })
        .collect();
    let throws2factor = HashMap::from([(3, 1), (4, 3), (5, 6), (6, 7), (7, 6), (8, 3), (9, 1)]);
    let mut playerwins: Vec<u64> = vec![0, 0]; // Holds the number of wins of completed games
    let mut universes: VecDeque<Universe> = VecDeque::new(); // Holds games in progress
    let universe0 = Universe {
        // Starting game state.
        points: vec![0, 0],
        pos: pos,
        factor: 1,
        turn: 0,
    };
    universes.push_back(universe0);
    while let Some(base) = universes.pop_front() {
        for throw in [3, 4, 5, 6, 7, 8, 9] {
            let mut nxt = base.clone();
            let turn = base.turn as usize;
            nxt.pos[turn] = (nxt.pos[turn] - 1 + throw) % 10 + 1;
            nxt.points[turn] += nxt.pos[turn];
            nxt.factor *= throws2factor[&throw];
            if nxt.points[turn] >= 21 {
                playerwins[turn] += nxt.factor;
                continue;
            } else {
                nxt.turn = ((turn + 1) % 2) as u8;
                universes.push_back(nxt);
            }
        }
    }
    println!("1: {}\n2: {}", playerwins[0], playerwins[1]);
    playerwins[0].max(playerwins[1])
}

#[derive(Clone, Debug)]
struct Universe {
    points: Vec<u8>, // points for player 1 and player 2
    pos: Vec<u8>,    // position for plauer 1 and player 2
    factor: u64,     // number of identical universes
    turn: u8,        // 0 or 1: this player throws the dice
}

fn sum_next3<T, U>(dice: &mut T) -> U
where
    T: Iterator<Item = U>,
    U: std::ops::Add<Output = U>,
{
    dice.next().unwrap() + dice.next().unwrap() + dice.next().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT1: &str = "\
    Player 1 starting position: 4
    Player 2 starting position: 8";

    #[test]
    fn test1() {
        let commands: Vec<&str> = TEST_INPUT1.lines().map(|s| s.trim()).collect();
        assert_eq!(739785, part1(&commands));
    }

    #[test]
    fn test2() {
        let commands: Vec<&str> = TEST_INPUT1.lines().map(|s| s.trim()).collect();
        assert_eq!(444356092776315, part2(&commands));
    }
}

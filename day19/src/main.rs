// --- Day 19: Beacon Scanner ---

// As your probe drifted down through this area,
// it released an assortment of beacons and scanners into the water.
// It's difficult to navigate in the pitch black open waters of the ocean trench,
// but if you can build a map of the trench using data from the scanners,
// you should be able to safely reach the bottom.

// The beacons and scanners float motionless in the water;
// they're designed to maintain the same position for long periods of time.
// Each scanner is capable of detecting all beacons in a large cube centered on the scanner;
// beacons that are at most 1000 units away from the scanner
// in each of the three axes (x, y, and z) have their precise position determined relative to the scanner.
// However, scanners cannot detect other scanners.
// The submarine has automatically summarized the relative positions of beacons
// detected by each scanner (your puzzle input).

// For example, if a scanner is at x,y,z coordinates 500,0,-500 and
// there are beacons at -500,1000,-1500 and 1501,0,-500,
//the scanner could report that the first beacon is at -1000,1000,-1000 (relative to the scanner)
// but would not detect the second beacon at all.

// Unfortunately, while each scanner can report the positions of all detected beacons relative to itself,
// the scanners do not know their own position.
// You'll need to determine the positions of the beacons and scanners yourself.

// The scanners and beacons map a single contiguous 3d region.
// This region can be reconstructed by finding pairs of scanners that have overlapping detection regions
// such that there are at least 12 beacons that both scanners detect within the overlap.
// By establishing 12 common beacons, you can precisely determine where the scanners are relative to each other,
// allowing you to reconstruct the beacon map one scanner at a time.

// For a moment, consider only two dimensions. Suppose you have the following scanner reports:

// --- scanner 0 ---
// 0,2
// 4,1
// 3,3

// --- scanner 1 ---
// -1,-1
// -5,0
// -2,1

// Drawing x increasing rightward, y increasing upward, scanners as S, and beacons as B, scanner 0 detects this:

// ...B.
// B....
// ....B
// S....

// Scanner 1 detects this:

// ...B..
// B....S
// ....B.

// For this example, assume scanners only need 3 overlapping beacons.
// Then, the beacons visible to both scanners overlap to produce the following complete map:

// ...B..
// B....S
// ....B.
// S.....

// Unfortunately, there's a second problem: the scanners also don't know their rotation or facing direction.
// Due to magnetic alignment, each scanner is rotated
// some integer number of 90-degree turns around all of the x, y, and z axes.
// That is, one scanner might call a direction positive x, while another scanner might call that direction negative y.
// Or, two scanners might agree on which direction is positive x, but
// one scanner might be upside-down from the perspective of the other scanner.
// In total, each scanner could be in any of 24 different orientations:
// facing positive or negative x, y, or z, and considering any of four directions "up" from that facing.

// For example, here is an arrangement of beacons as seen from a scanner
// in the same position but in different orientations:

// --- scanner 0 ---
// -1,-1,1
// -2,-2,2
// -3,-3,3
// -2,-3,1
// 5,6,-4
// 8,0,7

// --- scanner 0 ---
// 1,-1,1
// 2,-2,2
// 3,-3,3
// 2,-1,3
// -5,4,-6
// -8,-7,0

// --- scanner 0 ---
// -1,-1,-1
// -2,-2,-2
// -3,-3,-3
// -1,-3,-2
// 4,6,5
// -7,0,8

// --- scanner 0 ---
// 1,1,-1
// 2,2,-2
// 3,3,-3
// 1,3,-2
// -4,-6,5
// 7,0,8

// --- scanner 0 ---
// 1,1,1
// 2,2,2
// 3,3,3
// 3,1,2
// -6,-4,-5
// 0,7,-8

// By finding pairs of scanners that both see at least 12 of the same beacons,
// you can assemble the entire map. For example, consider the following report:

// --- scanner 0 ---
// 404,-588,-901
// 528,-643,409
// -838,591,734
// 390,-675,-793
// -537,-823,-458
// -485,-357,347
// -345,-311,381
// -661,-816,-575
// -876,649,763
// -618,-824,-621
// 553,345,-567
// 474,580,667
// -447,-329,318
// -584,868,-557
// 544,-627,-890
// 564,392,-477
// 455,729,728
// -892,524,684
// -689,845,-530
// 423,-701,434
// 7,-33,-71
// 630,319,-379
// 443,580,662
// -789,900,-551
// 459,-707,401

// --- scanner 1 ---
// 686,422,578
// 605,423,415
// 515,917,-361
// -336,658,858
// 95,138,22
// -476,619,847
// -340,-569,-846
// 567,-361,727
// -460,603,-452
// 669,-402,600
// 729,430,532
// -500,-761,534
// -322,571,750
// -466,-666,-811
// -429,-592,574
// -355,545,-477
// 703,-491,-529
// -328,-685,520
// 413,935,-424
// -391,539,-444
// 586,-435,557
// -364,-763,-893
// 807,-499,-711
// 755,-354,-619
// 553,889,-390

// --- scanner 2 ---
// 649,640,665
// 682,-795,504
// -784,533,-524
// -644,584,-595
// -588,-843,648
// -30,6,44
// -674,560,763
// 500,723,-460
// 609,671,-379
// -555,-800,653
// -675,-892,-343
// 697,-426,-610
// 578,704,681
// 493,664,-388
// -671,-858,530
// -667,343,800
// 571,-461,-707
// -138,-166,112
// -889,563,-600
// 646,-828,498
// 640,759,510
// -630,509,768
// -681,-892,-333
// 673,-379,-804
// -742,-814,-386
// 577,-820,562

// --- scanner 3 ---
// -589,542,597
// 605,-692,669
// -500,565,-823
// -660,373,557
// -458,-679,-417
// -488,449,543
// -626,468,-788
// 338,-750,-386
// 528,-832,-391
// 562,-778,733
// -938,-730,414
// 543,643,-506
// -524,371,-870
// 407,773,750
// -104,29,83
// 378,-903,-323
// -778,-728,485
// 426,699,580
// -438,-605,-362
// -469,-447,-387
// 509,732,623
// 647,635,-688
// -868,-804,481
// 614,-800,639
// 595,780,-596

// --- scanner 4 ---
// 727,592,562
// -293,-554,779
// 441,611,-461
// -714,465,-776
// -743,427,-804
// -660,-479,-426
// 832,-632,460
// 927,-485,-438
// 408,393,-506
// 466,436,-512
// 110,16,151
// -258,-428,682
// -393,719,612
// -211,-452,876
// 808,-476,-593
// -575,615,604
// -485,667,467
// -680,325,-822
// -627,-443,-432
// 872,-547,-609
// 833,512,582
// 807,604,487
// 839,-516,451
// 891,-625,532
// -652,-548,-490
// 30,-46,-14

// Because all coordinates are relative, in this example,
// all "absolute" positions will be expressed relative to scanner 0
// (using the orientation of scanner 0 and as if scanner 0 is at coordinates 0,0,0).

// Scanners 0 and 1 have overlapping detection cubes;
// the 12 beacons they both detect (relative to scanner 0) are at the following coordinates:

// -618,-824,-621
// -537,-823,-458
// -447,-329,318
// 404,-588,-901
// 544,-627,-890
// 528,-643,409
// -661,-816,-575
// 390,-675,-793
// 423,-701,434
// -345,-311,381
// 459,-707,401
// -485,-357,347

// These same 12 beacons (in the same order) but from the perspective of scanner 1 are:

// 686,422,578
// 605,423,415
// 515,917,-361
// -336,658,858
// -476,619,847
// -460,603,-452
// 729,430,532
// -322,571,750
// -355,545,-477
// 413,935,-424
// -391,539,-444
// 553,889,-390

// Because of this, scanner 1 must be at 68,-1246,-43 (relative to scanner 0).

// Scanner 4 overlaps with scanner 1; the 12 beacons they both detect (relative to scanner 0) are:

// 459,-707,401
// -739,-1745,668
// -485,-357,347
// 432,-2009,850
// 528,-643,409
// 423,-701,434
// -345,-311,381
// 408,-1815,803
// 534,-1912,768
// -687,-1600,576
// -447,-329,318
// -635,-1737,486

// So, scanner 4 is at -20,-1133,1061 (relative to scanner 0).

// Following this process, scanner 2 must be at 1105,-1205,1229 (relative to scanner 0)
// and scanner 3 must be at -92,-2380,-20 (relative to scanner 0).

// The full list of beacons (relative to scanner 0) is:

// -892,524,684
// -876,649,763
// -838,591,734
// -789,900,-551
// -739,-1745,668
// -706,-3180,-659
// -697,-3072,-689
// -689,845,-530
// -687,-1600,576
// -661,-816,-575
// -654,-3158,-753
// -635,-1737,486
// -631,-672,1502
// -624,-1620,1868
// -620,-3212,371
// -618,-824,-621
// -612,-1695,1788
// -601,-1648,-643
// -584,868,-557
// -537,-823,-458
// -532,-1715,1894
// -518,-1681,-600
// -499,-1607,-770
// -485,-357,347
// -470,-3283,303
// -456,-621,1527
// -447,-329,318
// -430,-3130,366
// -413,-627,1469
// -345,-311,381
// -36,-1284,1171
// -27,-1108,-65
// 7,-33,-71
// 12,-2351,-103
// 26,-1119,1091
// 346,-2985,342
// 366,-3059,397
// 377,-2827,367
// 390,-675,-793
// 396,-1931,-563
// 404,-588,-901
// 408,-1815,803
// 423,-701,434
// 432,-2009,850
// 443,580,662
// 455,729,728
// 456,-540,1869
// 459,-707,401
// 465,-695,1988
// 474,580,667
// 496,-1584,1900
// 497,-1838,-617
// 527,-524,1933
// 528,-643,409
// 534,-1912,768
// 544,-627,-890
// 553,345,-567
// 564,392,-477
// 568,-2007,-577
// 605,-1665,1952
// 612,-1593,1893
// 630,319,-379
// 686,-3108,-505
// 776,-3184,-501
// 846,-3110,-434
// 1135,-1161,1235
// 1243,-1093,1063
// 1660,-552,429
// 1693,-557,386
// 1735,-437,1738
// 1749,-1800,1813
// 1772,-405,1572
// 1776,-675,371
// 1779,-442,1789
// 1780,-1548,337
// 1786,-1538,337
// 1847,-1591,415
// 1889,-1729,1762
// 1994,-1805,1792

// In total, there are 79 beacons.

// Assemble the full map of beacons. How many beacons are there?

// Notes:
// - You can only map from scanners that have a known position and orientation,
// So start with anyone, i.e. scanner 0,
// Distances are equal no matter what the orientation.
// So start mapping all distances from the beacons of all scanners.
// See if there are at least 12 equal: there may be overlap between these scanners.

// --- Part Two ---

// Sometimes, it's a good idea to appreciate just how big the ocean is.
// Using the Manhattan distance, how far apart do the scanners get?

// In the above example, scanners 2 (1105,-1205,1229) and 3 (-92,-2380,-20) are the largest Manhattan distance apart.
// In total, they are 1197 + 1175 + 1249 = 3621 units apart.

// What is the largest Manhattan distance between any two scanners?

use std::collections::{HashMap, HashSet, VecDeque};
use std::fmt;
use std::fs;
use std::io::{self, BufRead};
use std::ops::{Add, Neg, Sub};
use std::time::Instant;

const INPUTFN: &str = "input19.txt";

fn main() {
    let file = fs::File::open(INPUTFN).expect(&format!("Cannot find file '{}'\n", INPUTFN));
    let input: Vec<String> = io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect();
    let input2 = input.iter().map(|s| s.as_ref()).collect::<Vec<&str>>();
    let now = Instant::now();
    println!(
        "Part1: {} in {} ms",
        part1(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
    let now = Instant::now();
    println!(
        "Part2: {} in {} ms",
        part2(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
}

fn part1(input: &Vec<&str>) -> u64 {
    part1_2(input, false)
}

fn part2(input: &Vec<&str>) -> u64 {
    part1_2(input, true)
}

fn part1_2(input: &Vec<&str>, is_part2: bool) -> u64 {
    let mut scanners = Scanner::read_n(&input);
    let num_scanners = scanners.len();
    for s in &mut scanners {
        s.make_distances();
    }

    let mut scanner_map = ScannerMap::default(); // Completely empty

    // A list of all transformations
    let transforms = make_transforms();

    // Determine the matching distances if there are 12 or more beacons involved
    // Store the result in scanner_map.matching_d
    for s1 in &scanners {
        for s2 in &scanners {
            let matches = s1.find_equal_dists(s2);
            // N beacons make (N-1) + (N-2) + .... + 2 + 1
            // For 12 beacons you need 66 matching distances
            if matches.len() <= 60 {
                // 0 matches: scanners not adjacent
                // 1 match: coincidence
                // 2 or 3 matches: 3 beacons, 1 distance may be double
                // 14 or 15 matches: 5 beacons
                // 65 or 66 matches: 12 beacons
                continue;
            }
            if let Ok(transform) = s1.find_scannertransform(s2, &matches) {
                //Record the matching distances
                scanner_map.matching_d.insert((s1.id, s2.id), matches);
                // Record the transform to map a vector in s2 to a vector in s1
                if !scanner_map.trf_idxs.contains_key(&(s1.id, s2.id)) {
                    scanner_map.trf_idxs.insert((s1.id, s2.id), transform);
                }
            }
        }
    }
    // The scanners form the nodes of a maze with edges between all scanners that share 12 beacons
    // and where a transform has been found
    // We must find the Shortest Path for every node back to node 0.
    let edges: HashSet<(usize, usize)> = scanner_map.trf_idxs.keys().map(|&e| e).collect();
    let predecessors = map_out_maze(scanners.len(), &edges);

    // For each scanner determine the translation it needs in the space of its predecessor
    let mut scanner_translations: HashMap<(usize, usize), Point> = HashMap::new();
    for s in &scanners[1..] {
        let pred = &scanners[predecessors[&s.id][1]];
        let translation = scanner_map.get_scanner_translation(pred, s);
        scanner_translations.insert((pred.id, s.id), translation);
    }

    // Determine the location of each scanner in S0 space.
    // Going back to 0 per step:
    // Suppose the offset so far for a scanner in S2 space is knwon (initially (0,0,0)
    //  - determine the transformation to get anything from S2 into S1
    //  - transform the direction of the known vector in S2 to a direction in S1
    //  - Add the local translation to get from S1 to S2
    // At the same time, transform (1,0,0), (0,1,0) and (0,0,1), without any translation
    // This results in a cumulative transformation of any direction in scid to a direction in S0
    // Then beacons of scid can be found by applying this transformation and the final translation
    for scid in 0..num_scanners {
        let path = &predecessors[&scid];
        let mut local_tr = Point::default();
        let mut curr_n = 0; // index into path for S2
        while curr_n < path.len() - 1 {
            // Here local_tr is the vector S2 --> scid in the space of S2
            let pred_n = curr_n + 1; // index into path for S1
            let s1id = path[pred_n];
            let s2id = path[curr_n];
            let transform = &transforms[scanner_map.trf_idxs[&(s1id, s2id)]]; // Transforms anything in S2 to S1
            let local_trf_tr = local_tr.transform(transform); // vector S2-->scid, in space S1
            local_tr = scanner_translations[&(s1id, s2id)] + local_trf_tr;
            curr_n += 1;
        }
        // println!("Location of scanner S{} is {}", scid, local_tr);
        scanner_map.scanners.insert(scid, local_tr);
    }
    if !is_part2 {
        for s in &scanners {
            for bc in &s.bc {
                let path = &predecessors[&s.id];
                let mut local_tr = *bc; // Start with the translation of the bc in S2
                let mut curr_n = 0; // index into path for S2
                while curr_n < path.len() - 1 {
                    // Here local_tr is the vector S2 --> bc in the space of S2
                    let pred_n = curr_n + 1; // index into path for S1
                    let s1id = path[pred_n];
                    let s2id = path[curr_n];
                    let transform = &transforms[scanner_map.trf_idxs[&(s1id, s2id)]]; // Transforms anything in S2 to S1
                    let local_trf_tr = local_tr.transform(transform); // vector S2-->bc, in space S1
                    local_tr = scanner_translations[&(s1id, s2id)] + local_trf_tr; // S1-->bc in space S1
                    curr_n += 1;
                }
                scanner_map.beacons.insert(local_tr);
            }
        }
        scanner_map.beacons.len() as u64
    } else {
        // Part 2
        let mut maxmanhattan = 0;
        for s1 in 0..num_scanners {
            for s2 in s1 + 1..num_scanners {
                let loc1 = scanner_map.scanners[&s1];
                let loc2 = scanner_map.scanners[&s2];
                let dloc = loc2 - loc1;
                let manhattan = dloc.x.abs() + dloc.y.abs() + dloc.z.abs();
                maxmanhattan = maxmanhattan.max(manhattan);
            }
        }
        maxmanhattan as u64
    }
}

/// Holds the initializing data for each scanner
/// Does not hold the colected data as searching progresses: that would require changing the stuct while it is
/// being read, and that makes the borrow checker bite.
struct Scanner {
    id: usize,
    bc: Vec<Point>,                         // Coordinates of beacons as detected
    dist_map: HashMap<u64, (usize, usize)>, // square of distance between the beacons.mapped to index of beacons
    doubles: HashMap<u64, (usize, usize)>,  // Set of non-unique distances
    dists: Vec<u64>,                        // Ordered set of distances
}

/// ScannerMap holds the data for all scanners and beacons that is being derived
/// This is the mut strux being changed while nonmutably borrowing a Scanner
/// In the end, it is the data for scanner 0 that is required.
/// There should be only 1 of these
#[derive(Default)]
struct ScannerMap {
    matching_d: HashMap<(usize, usize), Vec<u64>>, // The distances used to link the 2 scanners
    trf_idxs: HashMap<(usize, usize), usize>, // Holds the transformation index to map s2 points to s1 space
    scanners: HashMap<usize, Point>, // The coordinates of scanners, relative to S0 at (0,0,0)
    beacons: HashSet<Point>,         // The coordinates of beacons, relative to a base scanner
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Default, PartialOrd, Ord)]
struct Point {
    x: isize,
    y: isize,
    z: isize,
}

impl Scanner {
    /// returns a vector of all distances that are common to S1 and S2
    fn find_equal_dists(&self, o: &Scanner) -> Vec<u64> {
        if self.id == o.id {
            return vec![];
        }
        let mut result = vec![];
        for d in &self.dists {
            if o.dist_map.contains_key(d)
                && !self.doubles.contains_key(d)
                && !o.doubles.contains_key(d)
            {
                result.push(*d);
            }
        }
        result
    }

    /// Calculate all the distances between all beacons
    /// Using floats makes comparing tedious.
    /// We do not need the actual distances: their squares will do just fine
    /// We will store:
    /// - An ordered vector of distances
    /// - A map of distances to an ordered pair of indexes
    /// - A set of non-unique distances
    fn make_distances(&mut self) {
        let cap = self.bc.len() ^ 2 / 2;
        let mut dists: Vec<u64> = Vec::with_capacity(cap);
        let mut distmap: HashMap<u64, (usize, usize)> = HashMap::with_capacity(cap);
        let mut doubles: HashMap<u64, (usize, usize)> = HashMap::new();
        for (n1, bc1) in self.bc.iter().enumerate() {
            for (n2, bc2) in self.bc[n1 + 1..].iter().enumerate() {
                let distance = bc1.dist(bc2);
                if distmap.contains_key(&distance) {
                    // not a unique distance.
                    doubles.insert(distance, (n1, n1 + 1 + n2));
                } else {
                    dists.push(distance);
                    distmap.insert(distance, (n1, n1 + 1 + n2));
                }
            }
        }
        dists.sort();
        self.dists = dists;
        self.dist_map = distmap;
        self.doubles = doubles;
    }

    /// First find a matching distance where the difference vector for s2 (S2dbc) transforms
    /// with 1 of the transform functions in the difference vecotr for s1 (s1dbc): s1dbc == trf(s2dbc)
    /// This in independent of any translation: the direction and length will be the same after transforming
    /// Then use this transform to convert the direction of a beacon in s2 to the corresonding direction in s1.
    /// trf(s2bc)
    /// Then Vs2 + trf(s2bc) = Vbc --> Vs2 = Vbc - trf(s2bc)
    ///
    fn find_scannertransform(&self, o: &Scanner, matches: &Vec<u64>) -> Result<usize, String> {
        let transforms = make_transforms();
        let mut trf_index = None;
        'outer: for m in matches {
            let s1dbc = self.dist2vector(*m);
            let s2dbc = o.dist2vector(*m);
            for (trf_i, trf) in transforms.iter().enumerate() {
                let bc_for_s1 = s2dbc.transform(trf);
                if bc_for_s1 == s1dbc || -bc_for_s1 == s1dbc {
                    trf_index = Some(trf_i);
                    break 'outer;
                }
            }
        }
        trf_index.ok_or("Cannot find transform".to_string()) // If this happens, the transforms are not correct
    }

    /// Return a vector from bc1 to bc2 for the given distance, in the orientation of self
    fn dist2vector(&self, dist: u64) -> Point {
        let (ibc1, ibc2) = self.dist_map[&dist]; // The indexes
        let bc1 = self.bc[ibc1]; // The beacon vectors relative to the scanner
        let bc2 = self.bc[ibc2];
        bc2 - bc1
    }

    /// Read all scanners in the input you can find
    /// Each scanner starts with a line with "--- scanner n ---" where n is the scanner ID
    /// and ends with an empty line, or end of 'file'
    /// The input consists of single lines of text
    fn read_n(inp: &Vec<&str>) -> Vec<Self> {
        let mut scanners = vec![];
        let mut nxtidx = 0;
        loop {
            let (end, scanner) = Self::from_strs(&inp[nxtidx..]);
            match scanner {
                Ok(s) => scanners.push(s),
                Err(_s) => {
                    // eprintln!("{} at line {}", s, nxtidx + end);
                    break;
                }
            }
            nxtidx += end;
            if end >= inp.len() {
                break;
            }
        }
        scanners
    }

    /// Read a single scanner from the input.
    /// Returns a Scanner, and the index in the input just after the last line of the scanner
    /// This may be just AFTER the input!
    fn from_strs(inp: &[&str]) -> (usize, Result<Self, String>) {
        const SCANPREFIX: &str = "--- scanner ";
        let mut idx = 0;
        let mut beacons: Vec<Point> = vec![];
        while idx < inp.len() && inp[idx].len() == 0 {
            idx += 1;
        }
        if idx >= inp.len() {
            return (idx, Err("Done".to_string()));
        }
        if !inp[idx].starts_with(SCANPREFIX) {
            return (idx, Err("Scanner not starting with key string".to_string()));
        }
        let id = inp[idx]
            .split_whitespace()
            .nth(2)
            .unwrap()
            .parse::<usize>()
            .unwrap();
        idx += 1;
        while idx < inp.len() && inp[idx].len() > 1 {
            let bc: Vec<isize> = inp[idx]
                .split(",")
                .map(|e| e.parse::<isize>().unwrap())
                .collect();
            beacons.push(Point {
                x: bc[0],
                y: bc[1],
                z: bc[2],
            });
            idx += 1;
        }
        (
            idx,
            Ok(Scanner {
                id: id,
                bc: beacons,
                dists: vec![],
                dist_map: HashMap::new(),
                doubles: HashMap::new(),
            }),
        )
    }

    fn print_dists(&self) {
        println!("\nDistances for scannner {}", self.id);
        println!("Number of distances: {}", self.dists.len());
        for dist in &self.dists {
            let (n1, n2) = self.dist_map[dist];
            let p1 = &self.bc[n1];
            let p2 = &self.bc[n2];
            println!(
                "  {:7} {} -> {}: Vec{}: Point {} -> Point {}",
                dist,
                n1,
                n2,
                *p2 - *p1,
                p1,
                p2
            );
        }
        if self.doubles.len() > 0 {
            println!("\nDoubles:");
            for dist in self.doubles.keys() {
                let (n1, n2) = self.doubles[dist];
                let p1 = &self.bc[n1];
                let p2 = &self.bc[n2];
                println!(
                    " {:7}: {} -> {}: Vec{}: Point {} -> Point {}",
                    dist,
                    n1,
                    n2,
                    *p2 - *p1,
                    p1,
                    p2
                );
            }
        }
    }

    fn print_matching_distances(&self, o: &Scanner, matches: &Vec<u64>) {
        let d = matches[0];
        let (n1, n2) = self.dist_map[&d];
        let (m1, m2) = o.dist_map[&d];
        println!(
            "{} matches between scanner {} and scanner {}, e.g. {}: Vec{}\n  P{} -> P{}  /  P{} -> P{}",
            matches.len(),
            self.id,
            o.id,
            d,
            self.bc[n2] - self.bc[n1],
            self.bc[n1], self.bc[n2], o.bc[m1], o.bc[m2]
        );
        print!("Also: ");
        for m in matches[1..].iter() {
            print!("  {}", m);
        }
        println!();
    }
}

impl ScannerMap {
    /// Get the translation for s2 in the space of s1.
    /// Pre: s1 is predecessor of s2
    /// Scan the matching distances, and find a matching pair of translations
    fn get_scanner_translation(&self, s1: &Scanner, s2: &Scanner) -> Point {
        let transf = &make_transforms()[self.trf_idxs[&(s1.id, s2.id)]];
        for dist in &self.matching_d[&(s1.id, s2.id)] {
            let (bc1_1, bc1_2) = s1.dist_map[&dist]; // indexes into beacons list for bc 1 and 2 in space S1
            let (bc2_1, bc2_2) = s2.dist_map[&dist]; // Indexes into beacons list for bc 1 and 2 in space S2
            let bc2_1_1 = s2.bc[bc2_1].transform(transf); // direction for bc1 in S2 translated to location in S1
            let bc2_2_1 = s2.bc[bc2_2].transform(transf); // direction for bc2 in S2 translated to location in S1
            let translations = vec![
                s1.bc[bc1_1] - bc2_1_1,
                s1.bc[bc1_2] - bc2_1_1,
                s1.bc[bc1_1] - bc2_2_1,
                s1.bc[bc1_2] - bc2_2_1,
            ];
            if let Some(translation) = find_multiples(&translations, 2) {
                return translation;
            }
        }
        panic!(" No translation found");
    }
}

/// Find duplicates in a vector
fn find_multiples<T>(list: &Vec<T>, num: usize) -> Option<T>
where
    T: PartialEq + Copy,
{
    let counts: Vec<usize> = list
        .iter()
        .map(|e| list.iter().filter(|&e2| e2 == e).count())
        .collect();
    for (n, &c) in counts.iter().enumerate() {
        if c == num {
            return Some(list[n]);
        }
    }
    None
}

impl Point {
    fn new(x: isize, y: isize, z: isize) -> Self {
        Self { x, y, z }
    }

    /// Return square if distance
    fn dist(&self, p2: &Point) -> u64 {
        ((self.x - p2.x).pow(2) + (self.y - p2.y).pow(2) + (self.z - p2.z).pow(2)) as u64
    }

    fn transform<F>(&self, f: F) -> Self
    where
        F: Fn(isize, isize, isize) -> (isize, isize, isize),
    {
        let (x, y, z) = f(self.x, self.y, self.z);
        Point { x, y, z }
    }

    fn from_str(inp: &str) -> Self {
        let p: Vec<isize> = inp
            .split(",")
            .map(|e| e.parse::<isize>().unwrap())
            .collect();
        Self {
            x: p[0],
            y: p[1],
            z: p[2],
        }
    }
}

impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl Sub for Point {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl Neg for Point {
    type Output = Self;

    fn neg(self) -> Self {
        Self {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {}, {})", self.x, self.y, self.z)
    }
}

fn make_transforms() -> Vec<Box<dyn Fn(isize, isize, isize) -> (isize, isize, isize)>> {
    vec![
        // forward = +x up = +z, -z, +y, -y
        Box::new(|x, y, z| (x, y, z)), // identity: belongs to S0
        Box::new(|x, y, z| (x, -y, -z)),
        Box::new(|x, y, z| (x, z, -y)),
        Box::new(|x, y, z| (x, -z, y)),
        // forward = -x up = +z, -z, +y, -y
        Box::new(|x, y, z| (-x, -y, z)),
        Box::new(|x, y, z| (-x, y, -z)),
        Box::new(|x, y, z| (-x, z, y)),
        Box::new(|x, y, z| (-x, -z, -y)),
        // forward = +y up = +z, -z, +x, -x
        Box::new(|x, y, z| (-y, x, z)),
        Box::new(|x, y, z| (y, x, -z)),
        Box::new(|x, y, z| (z, x, y)),
        Box::new(|x, y, z| (-z, x, -y)),
        // forward = -y up = +z, -z, +x, -x
        Box::new(|x, y, z| (y, -x, z)),
        Box::new(|x, y, z| (-y, -x, -z)),
        Box::new(|x, y, z| (z, -x, -y)),
        Box::new(|x, y, z| (-z, -x, y)),
        // forward = +z up = +x, -x, +y, -y
        Box::new(|x, y, z| (z, -y, x)),
        Box::new(|x, y, z| (-z, y, x)),
        Box::new(|x, y, z| (y, z, x)),
        Box::new(|x, y, z| (-y, -z, x)),
        // forward = -z up = +x, -x, +y, -y
        Box::new(|x, y, z| (z, y, -x)),
        Box::new(|x, y, z| (-z, -y, -x)),
        Box::new(|x, y, z| (-y, z, -x)),
        Box::new(|x, y, z| (y, -z, -x)),
    ]
}

/// map_out_maze assumes a list of nodes, numbered 0 ..num_nodes and a list of edges
/// All edges have the same weight and are unidirecionally encoded as node1 to node2, but
/// the other direction is in the map too.
/// The return HasMap maps a node to a most efficient predecessor to get to that node.
fn map_out_maze(
    num_nodes: usize,
    edge_set: &HashSet<(usize, usize)>,
) -> HashMap<usize, Vec<usize>> {
    #[derive(Clone)]
    struct NodePath {
        path: Vec<usize>,
        length: usize,
    }
    let mut edges: HashMap<usize, Vec<usize>> = HashMap::with_capacity(num_nodes);
    for (from, to) in edge_set.iter() {
        edges.entry(*from).or_insert(Vec::new()).push(*to);
    }
    let mut que: VecDeque<NodePath> = VecDeque::with_capacity(num_nodes);
    let mut predecessors: HashMap<usize, Vec<usize>> = HashMap::with_capacity(num_nodes);
    let start = NodePath {
        path: vec![0],
        length: 0,
    };
    que.push_back(start);
    while let Some(ndp) = que.pop_front() {
        let node = ndp.path[ndp.path.len() - 1];
        // Store the path, reversed from end to 0.
        predecessors.insert(node, ndp.path.iter().rev().map(|&e| e).collect());
        for next in &edges[&node] {
            if predecessors.contains_key(&next) {
                continue;
            }
            let mut ndpn = ndp.clone();
            ndpn.path.push(*next);
            ndpn.length += 1;
            que.push_back(ndpn);
        }
    }
    predecessors
}

fn print_all_dists(scanners: &[Scanner]) {
    let mut distances = vec![];
    let mut d2s: HashMap<u64, Vec<usize>> = HashMap::new();
    for s in scanners {
        for d in &s.dists {
            if !d2s.contains_key(d) {
                distances.push(*d);
            }
            d2s.entry(*d).or_insert(Vec::new()).push(s.id);
        }
    }
    distances.sort();
    println!("All distances: {}\n", d2s.len());
    for d in &distances {
        println!("{:8}: {:?}", d, d2s.get(d).unwrap());
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT1: &str = "--- scanner 0 ---
    404,-588,-901
    528,-643,409
    -838,591,734
    390,-675,-793
    -537,-823,-458
    -485,-357,347
    -345,-311,381
    -661,-816,-575
    -876,649,763
    -618,-824,-621
    553,345,-567
    474,580,667
    -447,-329,318
    -584,868,-557
    544,-627,-890
    564,392,-477
    455,729,728
    -892,524,684
    -689,845,-530
    423,-701,434
    7,-33,-71
    630,319,-379
    443,580,662
    -789,900,-551
    459,-707,401
    
    --- scanner 1 ---
    686,422,578
    605,423,415
    515,917,-361
    -336,658,858
    95,138,22
    -476,619,847
    -340,-569,-846
    567,-361,727
    -460,603,-452
    669,-402,600
    729,430,532
    -500,-761,534
    -322,571,750
    -466,-666,-811
    -429,-592,574
    -355,545,-477
    703,-491,-529
    -328,-685,520
    413,935,-424
    -391,539,-444
    586,-435,557
    -364,-763,-893
    807,-499,-711
    755,-354,-619
    553,889,-390
    
    --- scanner 2 ---
    649,640,665
    682,-795,504
    -784,533,-524
    -644,584,-595
    -588,-843,648
    -30,6,44
    -674,560,763
    500,723,-460
    609,671,-379
    -555,-800,653
    -675,-892,-343
    697,-426,-610
    578,704,681
    493,664,-388
    -671,-858,530
    -667,343,800
    571,-461,-707
    -138,-166,112
    -889,563,-600
    646,-828,498
    640,759,510
    -630,509,768
    -681,-892,-333
    673,-379,-804
    -742,-814,-386
    577,-820,562
    
    --- scanner 3 ---
    -589,542,597
    605,-692,669
    -500,565,-823
    -660,373,557
    -458,-679,-417
    -488,449,543
    -626,468,-788
    338,-750,-386
    528,-832,-391
    562,-778,733
    -938,-730,414
    543,643,-506
    -524,371,-870
    407,773,750
    -104,29,83
    378,-903,-323
    -778,-728,485
    426,699,580
    -438,-605,-362
    -469,-447,-387
    509,732,623
    647,635,-688
    -868,-804,481
    614,-800,639
    595,780,-596
    
    --- scanner 4 ---
    727,592,562
    -293,-554,779
    441,611,-461
    -714,465,-776
    -743,427,-804
    -660,-479,-426
    832,-632,460
    927,-485,-438
    408,393,-506
    466,436,-512
    110,16,151
    -258,-428,682
    -393,719,612
    -211,-452,876
    808,-476,-593
    -575,615,604
    -485,667,467
    -680,325,-822
    -627,-443,-432
    872,-547,-609
    833,512,582
    807,604,487
    839,-516,451
    891,-625,532
    -652,-548,-490
    30,-46,-14";

    #[test]
    fn test1a() {
        let commands: Vec<&str> = TEST_INPUT1.lines().map(|s| s.trim()).collect();
        assert_eq!(79, part1(&commands));
    }

    #[test]
    fn test2() {
        let commands: Vec<&str> = TEST_INPUT1.lines().map(|s| s.trim()).collect();
        assert_eq!(3621, part2(&commands));
    }
}

// --- Day 24: Arithmetic Logic Unit ---

// Magic smoke starts leaking from the submarine's arithmetic logic unit (ALU).
// Without the ability to perform basic arithmetic and logic functions,
// the submarine can't produce cool patterns with its Christmas lights!
// It also can't navigate. Or run the oxygen system.
// Don't worry, though - you probably have enough oxygen left to give you enough time to build a new ALU.

// The ALU is a four-dimensional processing unit:
// it has integer variables w, x, y, and z.
// These variables all start with the value 0.
// The ALU also supports six instructions:

//     inp a - Read an input value and write it to variable a.
//     add a b - Add the value of a to the value of b, then store the result in variable a.
//     mul a b - Multiply the value of a by the value of b, then store the result in variable a.
//     div a b - Divide the value of a by the value of b, truncate the result to an integer,
//               then store the result in variable a. (Here, "truncate" means to round the value toward zero.)
//     mod a b - Divide the value of a by the value of b, then store the remainder in variable a.
//               (This is also called the modulo operation.)
//     eql a b - If the value of a and b are equal, then store the value 1 in variable a.
//               Otherwise, store the value 0 in variable a.

// In all of these instructions, a and b are placeholders;
// a will always be the variable where the result of the operation is stored (one of w, x, y, or z),
// while b can be either a variable or a number. Numbers can be positive or negative, but will always be integers.

// The ALU has no jump instructions;
// in an ALU program, every instruction is run exactly once in order from top to bottom.
// The program halts after the last instruction has finished executing.

// (Program authors should be especially cautious;
//    attempting to execute div with b=0 or
//    attempting to execute mod with a<0 or b<=0 will
// cause the program to crash and might even damage the ALU.
// These operations are never intended in any serious ALU program.)

// For example, here is an ALU program which takes an input number, negates it, and stores it in x:

// inp x
// mul x -1

// Here is an ALU program which takes two input numbers,
// then sets z to 1 if the second input number is three times larger than the first input number,
// or sets z to 0 otherwise:

// inp z
// inp x
// mul z 3
// eql z x

// Here is an ALU program which takes a non-negative integer as input,
// converts it into binary, and stores the lowest (1's) bit in z,
// the second-lowest (2's) bit in y, the third-lowest (4's) bit in x, and the fourth-lowest (8's) bit in w:

// inp w
// add z w
// mod z 2
// div w 2
// add y w
// mod y 2
// div w 2
// add x w
// mod x 2
// div w 2
// mod w 2

// Once you have built a replacement ALU, you can install it in the submarine,
// which will immediately resume what it was doing when the ALU failed:
// validating the submarine's model number.
// To do this, the ALU will run the MOdel Number Automatic Detector program (MONAD, your puzzle input).

// Submarine model numbers are always fourteen-digit numbers consisting only of digits 1 through 9.
// The digit 0 cannot appear in a model number.

// When MONAD checks a hypothetical fourteen-digit model number,
// it uses fourteen separate inp instructions,
// each expecting a single digit of the model number in order of most to least significant.
// (So, to check the model number 13579246899999, you would give 1 to the first inp instruction,
// 3 to the second inp instruction, 5 to the third inp instruction, and so on.)
// This means that when operating MONAD, each input instruction should only ever be given
// an integer value of at least 1 and at most 9.

// Then, after MONAD has finished running all of its instructions, it will indicate that
// the model number was valid by leaving a 0 in variable z.
// However, if the model number was invalid, it will leave some other non-zero value in z.

// MONAD imposes additional, mysterious restrictions on model numbers,
// and legend says the last copy of the MONAD documentation was eaten by a tanuki.
// You'll need to figure out what MONAD does some other way.

// To enable as many submarine features as possible,
// find the largest valid fourteen-digit model number that contains no 0 digits.
// What is the largest model number accepted by MONAD?

// First attempt:
// Make the ALU and try all numbers.
// That all looks mighty fine, but just generating the numbers and doing nothing with them takes forever.
//
// This site (https://blog.jverkamp.com/2021/12/24/aoc-2021-day-24-aluinator/) points in the right direction, but not quite.
// It is indeed obvious that there is a lot of repeating code after each input.
// Each block has inputs:
// - Previous z value,
// - the block's input
// - a constant 1 or 26 (zdiv),
// - a constant added to x (cx) and
// - a constatnt added t y (cy)
// The code of each block after an input to register W is as follows:
//  X := Z%26 + cx
//  Z := Z // zdiv
//  if X != In:
//      Z := 26Z + In + cy
// Previous Z inputs get multiplied and divided by 26, making like a stack of numbers between 0 and 26.
// Therefore, using TOS (Top of Stack) and pop and push
// x := ToS + cx
// if zdiv==26:
//     pop
// if x != In:
//     push In+cy
// So the stack can grow 1, reduce 1 or stay even at every step.
// Acceptance of a num14 is if z == 0. This can only be if the stack is emty at the end, so there
// must be as  many pops as pushes.
// Looking at the numbers, there are 7 pops (where zdiv == 26) and 7 sure pushes (where Tos + cx > 9 and therefore always != In).
// That means that all unsure push conditions must be false.
// Using a spreadsheet to play out the algorithm by hand, and assuming that every possible push must be avoided,
// i.e ToS + cx == In wherever possible, you find:
//
// In4 == In3 + 3
// In6 == In5 - 6
// In7 == In2 + 8
// I11 == I10 - 5
// I12 == I9 + 1
// I13 == I8 + 5
// I14 == I1 - 4
//
// This way, the possible numbers are greatly reduced
// For the largest number, we set the inputs that must be substracted from to 9
// For the smallest numer we set the inputs that must be added to to 1
//
//  1   I14 + 4 ------\     9   5
//  2   I7  - 8 --\    |    1   1
//  3   I4  - 3 \  |   |    6   1
//  4   I3  + 3 /  |   |    9   4
//  5   I6  + 6 \  |   |    9   7
//  6   I5  - 6 /  |   |    3   1
//  7   I2  + 8 --/    |    9   9
//  8   I13 - 5 ----\  |    4   1
//  9   I12 - 1 --\  | |    8   1
// 10   I11 + 5 \  | | |    9   6
// 11   I10 - 5 /  | | |    4   1
// 12   I9  + 1 --/  | |    9   2
// 13   I8  + 5 ----/  |    9   6
// 14   I1  - 4 ------/     5   1
//
// So: largest:  91699394894995
//     smallest: 51147191161261
//
// There is no additional coding necessary to find this

use std::collections::{HashMap, VecDeque}; //HashMap, HashSet, VecDeque};
use std::fmt;
use std::fs;
use std::io::{self, BufRead};
use std::iter::IntoIterator;
use std::time::Instant;

use anyhow::Result;

const INPUTFN: &str = "input24.txt";

fn main() {
    let file = fs::File::open(INPUTFN).expect(&format!("Cannot find file '{}'\n", INPUTFN));
    let input: Vec<String> = io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect();
    let input2 = input.iter().map(|s| s.as_ref()).collect::<Vec<&str>>();
    let now = Instant::now();
    println!(
        "Part1: {} in {} ms",
        part1(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
    let now = Instant::now();
    println!(
        "Part2: {} in {} ms",
        part2(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
}

fn part1(input: &Vec<&str>) -> u64 {
    let mut alu = Alu::new();
    let mut program: Vec<Instr> = Vec::with_capacity(input.len());
    match program_from_input(input) {
        Ok(prog) => {
            program = prog;
        }
        Err(e) => {
            println!("Cannot parse porgram: {e}");
            return 0;
        }
    }
    let num14 = Number14gen::from(11111111111111..99999999999999).unwrap();
    let mut result = [0; 14];
    let mut n_print = num14.end;
    for num in num14 {
        if is_num14_ok(&program, &num) {
            result = num;
            break;
            // } else {
            //     if n_print[..9] != num[..9] {
            //         println!("{num:?}");
            //         n_print = num;
            //     }
        }
    }
    num14_to_int(&result)
}

fn part2(input: &Vec<&str>) -> u64 {
    0
}

enum Register {
    W,
    X,
    Y,
    Z,
}

enum Value {
    W,
    X,
    Y,
    Z,
    Num(i64),
    None,
}

#[derive(Debug)]
struct Alu {
    w: i64,
    x: i64,
    y: i64,
    z: i64,
}

struct Instr {
    opc: Opcode,
    dst: Register,
    src: Value,
}

enum Opcode {
    Inp,
    Add,
    Mul,
    Div,
    Mod,
    Eql,
}

struct Number14gen {
    start: [u8; 14],
    end: [u8; 14],
    reversed: bool,
}

impl Number14gen {
    fn from(rng: std::ops::Range<u64>) -> Result<Self> {
        let mut start = rng.start;
        let mut end = rng.end;
        let mut reversed = false;
        if rng.start > rng.end {
            start = rng.end;
            end = rng.start;
            reversed = true;
        }
        let startslc = Self::int2digits(start);
        if startslc.iter().any(|&d| d == 0) {
            anyhow::bail!("start contains zeroes");
        }
        let endslc = Self::int2digits(end);
        if endslc.iter().any(|&d| d == 0) {
            anyhow::bail!("end contains zeroes");
        }
        Ok(Number14gen {
            start: startslc,
            end: endslc,
            reversed,
        })
    }

    fn int2digits(input: u64) -> [u8; 14] {
        let mut result = [0_u8; 14];
        let mut num = input;
        let mut idx = 14;
        while num > 0 && idx > 0 {
            idx -= 1;
            result[idx] = (num % 10) as u8;
            num /= 10;
        }
        result
    }

    fn iter(&self) -> Number14Iter {
        Number14Iter {
            current: if self.reversed { self.end } else { self.start },
            end: if self.reversed { self.start } else { self.end },
            end_found: false,
            reversed: self.reversed,
        }
    }
}

impl IntoIterator for Number14gen {
    type Item = [u8; 14];
    type IntoIter = Number14Iter;

    fn into_iter(self) -> Self::IntoIter {
        Number14Iter {
            current: if self.reversed { self.end } else { self.start },
            end: if self.reversed { self.start } else { self.end },
            end_found: false,
            reversed: self.reversed,
        }
    }
}

impl IntoIterator for &Number14gen {
    type Item = [u8; 14];
    type IntoIter = Number14Iter;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

struct Number14Iter {
    current: [u8; 14],
    end: [u8; 14],
    end_found: bool,
    reversed: bool,
}

impl Iterator for Number14Iter {
    type Item = [u8; 14];

    fn next(&mut self) -> Option<[u8; 14]> {
        if self.end_found {
            return None;
        }
        let result = self.current;
        self.end_found = self.current == self.end;
        if self.reversed {
            for n in (0..13).rev() {
                if self.current[n] > 1 {
                    self.current[n] -= 1;
                    if n == 12 {
                        self.current[13] = self.current[12] + 3;
                    }

                    break;
                }
                if n == 0 {
                    return None; // Cannot go below all 1's
                }
                self.current[n] = 9;
            }
        } else {
            for n in (0..13).rev() {
                if self.current[n] < 9 {
                    self.current[n] += 1;
                    if n == 12 {
                        self.current[13] = self.current[12] + 3;
                    }
                    break;
                }
                if n == 0 {
                    return None; // Cannot go beyond all 9's
                }
                self.current[n] = 1;
            }
        }
        Some(result)
    }
}

fn num14_to_int(input: &[u8; 14]) -> u64 {
    let mut result = 0;
    for d in input {
        result = 10 * result + *d as u64;
    }
    result
}

// Retrieve a program from a vector of strings
fn program_from_input(input: &Vec<&str>) -> Result<Vec<Instr>> {
    let mut result: Vec<Instr> = Vec::with_capacity(input.len());
    for (lnr, &line) in input.iter().enumerate() {
        match Instr::parse(line) {
            Ok(instr) => result.push(instr),
            Err(e) => {
                anyhow::bail!("error parsing instruction at line {}: {e}", lnr + 1);
            }
        }
    }
    Ok(result)
}

fn is_num14_ok(program: &Vec<Instr>, num: &[u8; 14]) -> bool {
    let mut alu = Alu::new();
    let inputs: Vec<i64> = num.iter().map(|n| *n as i64).collect();
    alu.execute(program, &inputs);
    println!("Num {num:?} --> {alu:?}");
    alu.z == 1
}

impl Alu {
    fn new() -> Self {
        Self {
            w: 0,
            x: 0,
            y: 0,
            z: 0,
        }
    }

    // set all registers of the ALU to 0
    fn null(&mut self) {
        self.w = 0;
        self.x = 0;
        self.y = 0;
        self.z = 0;
    }

    // Execute a program.
    // Note: the registers are not initialized prior to executing next program
    fn execute(&mut self, program: &Vec<Instr>, inputs: &Vec<i64>) {
        let mut vals: VecDeque<i64> = inputs.iter().map(|&e| e).collect();
        for instr in program {
            match instr.opc {
                Opcode::Inp => self.inp(&mut vals, &instr),
                Opcode::Add => self.add(&instr),
                Opcode::Mul => self.mul(&instr),
                Opcode::Div => self.div(&instr),
                Opcode::Mod => self.modulo(&instr),
                Opcode::Eql => self.eql(&instr),
            }
        }
    }

    // Execute an inp instruction: dst := input
    fn inp(&mut self, vals: &mut VecDeque<i64>, instr: &Instr) {
        let value = vals.pop_front().unwrap();
        self.set_dst(&instr.dst, value);
    }

    // Execute Add instruction: dst := dst + src
    fn add(&mut self, instr: &Instr) {
        let src = self.get_src(instr);
        let val = self.get_dst(&instr.dst) + src;
        self.set_dst(&instr.dst, val);
    }

    // Execute Mul instruction: dst := dst * src
    fn mul(&mut self, instr: &Instr) {
        let src = self.get_src(instr);
        let val = self.get_dst(&instr.dst) * src;
        self.set_dst(&instr.dst, val);
    }

    // Execute Div instruction: dst := dst / src
    fn div(&mut self, instr: &Instr) {
        let src = self.get_src(instr);
        let val = self.get_dst(&instr.dst) / src;
        self.set_dst(&instr.dst, val);
    }

    // Execute Mod instruction: dst := dst % src
    fn modulo(&mut self, instr: &Instr) {
        let src = self.get_src(instr);
        let val = self.get_dst(&instr.dst) % src;
        self.set_dst(&instr.dst, val);
    }

    // Execute Eql instruction: dst := 0 if src == dst else 1
    fn eql(&mut self, instr: &Instr) {
        let src = self.get_src(instr);
        let val = if self.get_dst(&instr.dst) == src {
            1
        } else {
            0
        };
        self.set_dst(&instr.dst, val);
    }

    fn get_src(&self, instr: &Instr) -> i64 {
        match instr.src {
            Value::W => self.w,
            Value::X => self.x,
            Value::Y => self.y,
            Value::Z => self.z,
            Value::Num(v) => v,
            Value::None => panic!("No src value found to add"),
        }
    }

    fn get_dst(&self, dst: &Register) -> i64 {
        match dst {
            Register::W => self.w,
            Register::X => self.x,
            Register::Y => self.y,
            Register::Z => self.z,
        }
    }

    fn set_dst(&mut self, dst: &Register, value: i64) {
        match dst {
            Register::W => {
                self.w = value;
            }
            Register::X => {
                self.x = value;
            }
            Register::Y => {
                self.y = value;
            }
            Register::Z => {
                self.z = value;
            }
        }
    }
}

impl Instr {
    /// Parse a single line of program code.
    fn parse(inp: &str) -> Result<Self> {
        let mut instr_parts = inp.split_whitespace();
        let opc = Self::find_opc(instr_parts.next())?;
        let dst = Self::find_dst(instr_parts.next())?;
        let src = Self::find_val(instr_parts.next())?;
        Ok(Instr { opc, dst, src })
    }

    fn find_opc(opc_inp: Option<&str>) -> Result<Opcode> {
        if let Some(opcstr) = opc_inp {
            match opcstr {
                "inp" => Ok(Opcode::Inp),
                "add" => Ok(Opcode::Add),
                "mul" => Ok(Opcode::Mul),
                "div" => Ok(Opcode::Div),
                "mod" => Ok(Opcode::Mod),
                "eql" => Ok(Opcode::Eql),
                _ => anyhow::bail!("unknown opcode '{opcstr}'"),
            }
        } else {
            anyhow::bail!("no opcode found");
        }
    }

    fn find_dst(dst_inp: Option<&str>) -> Result<Register> {
        if let Some(dststr) = dst_inp {
            match dststr {
                "w" => Ok(Register::W),
                "x" => Ok(Register::X),
                "y" => Ok(Register::Y),
                "z" => Ok(Register::Z),
                _ => anyhow::bail!("unknown register name '{dststr}'"),
            }
        } else {
            anyhow::bail!("no destimatin register found")
        }
    }

    fn find_val(val_inp: Option<&str>) -> Result<Value> {
        if let Some(valstr) = val_inp {
            match valstr {
                "w" => Ok(Value::W),
                "x" => Ok(Value::X),
                "y" => Ok(Value::Y),
                "z" => Ok(Value::Z),
                _ => {
                    if let Ok(val) = valstr.parse::<i64>() {
                        Ok(Value::Num(val))
                    } else {
                        anyhow::bail!("invalid value '{valstr}'")
                    }
                }
            }
        } else {
            Ok(Value::None)
        }
    }
}

mod tests {
    use super::*;

    const TEST_INPUT1: &str = "\
";

    const TEST_INPUT2: &str = "\
";

    #[test]
    fn test_prog1() {
        let programtxt = "\
            inp x
            mul x -1";
        let programvec: Vec<&str> = programtxt.lines().collect();
        for input in [1, 2, 3, -1, -2, -3] {
            let mut alu = Alu::new();
            let program = program_from_input(&programvec).unwrap();
            let inp = vec![input];
            alu.execute(&program, &inp);
            assert_eq!(input * -1, alu.x);
        }
    }

    #[test]
    fn test_prog2() {
        let programtxt = "\
            inp z
            inp x
            mul z 3
            eql z x";
        let programvec: Vec<&str> = programtxt.lines().collect();
        for (a, b) in [(1, 3), (2, 3), (-1, -3), (1, -3)] {
            let mut alu = Alu::new();
            let program = program_from_input(&programvec).unwrap();
            let inp = vec![a, b];
            alu.execute(&program, &inp);
            assert_eq!(if b == 3 * a { 1 } else { 0 }, alu.z);
        }
    }

    #[test]
    fn test_prog3() {
        let programtxt = "\
            inp w
            add z w
            mod z 2
            div w 2
            add y w
            mod y 2
            div w 2
            add x w
            mod x 2
            div w 2
            mod w 2";
        let programvec: Vec<&str> = programtxt.lines().collect();
        for input in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17] {
            let mut alu = Alu::new();
            let program = program_from_input(&programvec).unwrap();
            let inp = vec![input];
            alu.execute(&program, &inp);
            assert_eq!(input >> 0 & 1, alu.z);
            assert_eq!(input >> 1 & 1, alu.y);
            assert_eq!(input >> 2 & 1, alu.x);
            assert_eq!(input >> 3 & 1, alu.w);
        }
    }

    #[test]
    fn test_int2digits() {
        assert_eq!([0; 14], Number14gen::int2digits(0));
        assert_eq!(
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            Number14gen::int2digits(1)
        );
        assert_eq!(
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
            Number14gen::int2digits(1000)
        );
        assert_eq!([1; 14], Number14gen::int2digits(11111111111111));
        assert_eq!([9; 14], Number14gen::int2digits(99999999999999));
    }

    #[test]
    fn test_num14_generator() {
        assert!(Number14gen::from(0..10).is_err());
        assert!(Number14gen::from(1111..2222).is_err());
        assert!(Number14gen::from(99999999999999..11111111111111).is_ok());
        assert!(Number14gen::from(11111111111111..99999999999999).is_ok());
        let mut num14gen = Number14gen::from(11111111111125..11111111111115)
            .unwrap()
            .into_iter();
        assert_eq!([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5], num14gen.end);
        assert_eq!([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 5], num14gen.current);
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 5],
            num14gen.next().unwrap()
        );
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 4],
            num14gen.next().unwrap()
        );
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 3],
            num14gen.next().unwrap()
        );
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2],
            num14gen.next().unwrap()
        );
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1],
            num14gen.next().unwrap()
        );
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9],
            num14gen.next().unwrap()
        );
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 8],
            num14gen.next().unwrap()
        );
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 7],
            num14gen.next().unwrap()
        );
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6],
            num14gen.next().unwrap()
        );
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5],
            num14gen.next().unwrap()
        );
        let mut num14gen = Number14gen::from(11111111111115..11111111111125)
            .unwrap()
            .into_iter();
        assert_eq!([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 5], num14gen.end);
        assert_eq!([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5], num14gen.current);
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5],
            num14gen.next().unwrap()
        );
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6],
            num14gen.next().unwrap()
        );
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 7],
            num14gen.next().unwrap()
        );
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 8],
            num14gen.next().unwrap()
        );
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9],
            num14gen.next().unwrap()
        );
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1],
            num14gen.next().unwrap()
        );
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2],
            num14gen.next().unwrap()
        );
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 3],
            num14gen.next().unwrap()
        );
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 4],
            num14gen.next().unwrap()
        );
        assert_eq!(
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 5],
            num14gen.next().unwrap()
        );
    }

    // #[test]
    // fn test1() {
    //     assert_eq!(12521, part1(&start_map));
    // }

    // #[test]
    // fn test2() {
    //     assert_eq!(44169, part2(&start_map));
    // }
}

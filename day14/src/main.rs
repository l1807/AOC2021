// --- Day 14: Extended Polymerization ---

// The incredible pressures at this depth are starting to put a strain on your submarine.
// The submarine has polymerization equipment that would
// produce suitable materials to reinforce the submarine,
// and the nearby volcanically-active caves should even have
// the necessary input elements in sufficient quantities.

// The submarine manual contains instructions for finding the optimal polymer formula;
// specifically, it offers a polymer template and a list of pair insertion rules (your puzzle input).
// You just need to work out what polymer would result after repeating the pair insertion process a few times.

// For example:

// NNCB

// CH -> B
// HH -> N
// CB -> H
// NH -> C
// HB -> C
// HC -> B
// HN -> C
// NN -> C
// BH -> H
// NC -> B
// NB -> B
// BN -> B
// BB -> N
// BC -> B
// CC -> N
// CN -> C

// The first line is the polymer template - this is the starting point of the process.

// The following section defines the pair insertion rules.
// A rule like AB -> C means that when elements A and B are immediately adjacent,
// element C should be inserted between them.
// These insertions all happen simultaneously.

// So, starting with the polymer template NNCB, the first step simultaneously considers all three pairs:

//     The first pair (NN) matches the rule NN -> C, so element C is inserted between the first N and the second N.
//     The second pair (NC) matches the rule NC -> B, so element B is inserted between the N and the C.
//     The third pair (CB) matches the rule CB -> H, so element H is inserted between the C and the B.

// Note that these pairs overlap: the second element of one pair is the first element of the next pair.
// Also, because all pairs are considered simultaneously,
// inserted elements are not considered to be part of a pair until the next step.

// After the first step of this process, the polymer becomes NCNBCHB.

// Here are the results of a few steps using the above rules:

// Template:     NNCB
// After step 1: NCNBCHB
// After step 2: NBCCNBBBCBHCB
// After step 3: NBBBCNCCNBBNBNBBCHBHHBCHB
// After step 4: NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB

// This polymer grows quickly. After step 5, it has length 97;
// After step 10, it has length 3073.
// After step 10, B occurs 1749 times, C occurs 298 times, H occurs 161 times, and N occurs 865 times;
// taking the quantity of the most common element (B, 1749) and
// subtracting the quantity of the least common element (H, 161) produces 1749 - 161 = 1588.

// Apply 10 steps of pair insertion to the polymer template and
// find the most and least common elements in the result.
// What do you get if you take the quantity of the most common element and
// subtract the quantity of the least common element?

// --- Part Two ---

// The resulting polymer isn't nearly strong enough to reinforce the submarine.
// You'll need to run more steps of the pair insertion process; a total of 40 steps should do it.

// In the above example, the most common element is B (occurring 2192039569602 times) and
// the least common element is H (occurring 3849876073 times); subtracting these produces 2188189693529.

// Apply 40 steps of pair insertion to the polymer template and find
// the most and least common elements in the result.
// What do you get if you take the quantity of the most common element and subtract the quantity of the least common element?

use std::collections::HashMap; //HashSet, VecDeque};
use std::fs;
use std::io::{self, BufRead};
use std::time::Instant;

const INPUTFN: &str = "input14.txt";

fn main() {
    let file = fs::File::open(INPUTFN).expect(&format!("Cannot find file '{}'\n", INPUTFN));
    let input: Vec<String> = io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect();
    let input2 = input.iter().map(|s| s.as_ref()).collect::<Vec<&str>>();
    let now = Instant::now();
    println!(
        "Part1: {} in {} ms",
        part1(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
    let now = Instant::now();
    println!(
        "Part2: {} in {} ms",
        part2(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
}

fn part1(input: &Vec<&str>) -> i32 {
    let mut formula = input[0].trim().to_string();
    let rules: HashMap<String, char> = input[2..]
        .iter()
        .filter(|line| line.len() > 1)
        .map(|line| {
            let (left, right) = line.split_once(" -> ").unwrap();
            (left.trim().into(), right.trim().chars().nth(0).unwrap())
        })
        .collect();
    for _n in 0..10 {
        // eprintln!("{}: {}:{:?}", n, formula.len(), &formula);
        formula = perform_step(&formula, &rules);
    }
    // eprintln!("10: {}:{}", formula.len(), &formula);

    score_part1(&formula) as i32
}

/// Part 1 is ok using a direct approach.
/// With 40 steps that won't get to a solution: too many steps!
/// I stole a spoiler from the internet (shame on me!) and the way to approach this is to
/// have a counter for each pair that is in the rules
/// - for each pair in the rules, store the result pairs;
///   e.g rule CH -> B is stored as CH -> CB, BH

fn part2(input: &Vec<&str>) -> i64 {
    let formula = input[0].trim().to_string();
    let rulesdata: Vec<&str> = input[2..]
        .iter()
        .filter(|line| line.len() > 1)
        .map(|line| *line)
        .collect();
    // Make the rules map
    let rules: HashMap<String, (String, String)> = rulesdata
        .iter()
        .map(|&line| {
            let (left, right) = line.split_once(" -> ").unwrap();
            let mut leftc = left.chars();
            let left1 = leftc.nth(0).unwrap();
            let left2 = leftc.nth(0).unwrap();
            let right1 = format!("{}{}", left1, right);
            let right2 = format!("{}{}", right, left2);
            (left.into(), (right1, right2))
        })
        .collect();
    // Make a vector of keys into the rulesmap and into the paircount map
    let rulekeys: Vec<String> = rulesdata
        .iter()
        .map(|&line| line.split(" -> ").nth(0).unwrap().into())
        .collect();
    // Make a map of paircounts, initialised to 0.
    let mut paircount: HashMap<String, i64> = rulekeys.iter().map(|k| (k.clone(), 0)).collect();
    // prime the paircount with the original formula
    let formula_vec: Vec<char> = formula.chars().collect(); // create something slice-able
    for pair in formula_vec[..].windows(2) {
        // so we can run windows() on it
        let key: String = pair.iter().collect(); // &[char] cannot serve as key
        *paircount.get_mut(&key).unwrap() += 1; // Count all pairs in the formula
    }
    for n in 0..40 {
        perform_step2(&mut paircount, &rulekeys, &rules);
    }
    score_part2(&paircount, &formula_vec)
}

fn perform_step(frm: &str, rules: &HashMap<String, char>) -> String {
    let mut result: String = String::with_capacity(frm.len() * 2);
    let mut prev: char = ' ';
    for c in frm.chars() {
        if !result.is_empty() {
            let mut key = prev.to_string();
            key.push(c);
            let middle = rules[&key];
            result.push(middle);
        }
        result.push(c);
        prev = c;
    }
    result
}

/// - for each pair in the rules, store how many times it occurs in the original polymer: the rule-count
/// - at each step, for each rule, add decrement the rule-count of the rule itself, and increment the count for the result pairs.
/// So if CH contains n, after the step, it contains n-1 and rules CB and BH are incremented.
fn perform_step2(
    paircount: &mut HashMap<String, i64>,
    rule_keys: &[String],
    rules: &HashMap<String, (String, String)>,
) {
    // For each key in ruleskey, have a changecount.
    // This way, the updates do not interfere with the original paircount
    let mut changes: HashMap<String, i64> = HashMap::with_capacity(rule_keys.len());
    for k in rule_keys {
        let v = *paircount.get(k).unwrap();
        let change = changes.entry(k.clone()).or_insert(0);
        *change -= v;
        let (v1, v2) = rules.get(k).unwrap();
        let change = changes.entry(v1.clone()).or_insert(0);
        *change += v;
        let change = changes.entry(v2.clone()).or_insert(0);
        *change += v;
    }
    for (k, v) in &changes {
        let value = paircount.get_mut(k).unwrap();
        *value += *v;
    }
}

fn score_part1(frm: &str) -> i64 {
    let mut scores: HashMap<char, i64> = HashMap::new();
    for c in frm.chars() {
        let score = scores.entry(c).or_insert(0);
        *score += 1;
    }
    let &max = scores.iter().map(|(_, v)| v).max().unwrap();
    let &min = scores.iter().map(|(_, v)| v).min().unwrap();
    eprintln!("{} {}", min, max);
    max - min
}

/// For every element pair there are 2 elements.
/// Every element occurs as a firs element and as a second element,
/// so the counts must be divided by 2.
/// However, the first element and the last element of the original formula
/// occurs only once and never changes place. These must be added before dividing by 2: +1.
fn score_part2(paircount: &HashMap<String, i64>, formula: &[char]) -> i64 {
    let mut scoremap: HashMap<char, i64> = HashMap::new();
    for (k, v) in paircount {
        for elm in k.chars() {
            let elmcnt = scoremap.entry(elm).or_insert(0);
            *elmcnt += v;
        }
    }
    let first = formula[0];
    let last = formula[formula.len() - 1];
    for (k, v) in &mut scoremap {
        if *k == last || *k == first {
            *v += 1;
        }
        *v /= 2;
    }
    let max = scoremap.iter().map(|(_, v)| v).max().unwrap();
    let min = scoremap.iter().map(|(_, v)| v).min().unwrap();
    eprintln!("{} {}", min, max);
    max - min
}
#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT1: &str = "NNCB

    CH -> B
    HH -> N
    CB -> H
    NH -> C
    HB -> C
    HC -> B
    HN -> C
    NN -> C
    BH -> H
    NC -> B
    NB -> B
    BN -> B
    BB -> N
    BC -> B
    CC -> N
    CN -> C";

    #[test]
    fn test1() {
        let commands: Vec<&str> = TEST_INPUT1.lines().map(|s| s.trim()).collect();
        assert_eq!(1588, part1(&commands));
    }

    #[test]
    fn test2() {
        let commands: Vec<&str> = TEST_INPUT1.lines().map(|s| s.trim()).collect();
        assert_eq!(2188189693529, part2(&commands));
    }
}

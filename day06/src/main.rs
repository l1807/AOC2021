// --- Day 6: Lanternfish ---

// The sea floor is getting steeper. Maybe the sleigh keys got carried this way?

// A massive school of glowing lanternfish swims past.
// They must spawn quickly to reach such large numbers - maybe exponentially quickly?
// You should model their growth rate to be sure.

// Although you know nothing about this specific species of lanternfish, you make some guesses about their attributes.
// Surely, each lanternfish creates a new lanternfish once every 7 days.

// However, this process isn't necessarily synchronized between every lanternfish - one lanternfish might have 2 days left until it creates another lanternfish, while another might have 4. So, you can model each fish as a single number that represents the number of days until it creates a new lanternfish.

// Furthermore, you reason, a new lanternfish would surely need slightly longer before
// it's capable of producing more lanternfish: two more days for its first cycle.

// So, suppose you have a lanternfish with an internal timer value of 3:

//     After one day, its internal timer would become 2.
//     After another day, its internal timer would become 1.
//     After another day, its internal timer would become 0.
//     After another day, its internal timer would reset to 6,
//            and it would create a new lanternfish with an internal timer of 8.
//     After another day, the first lanternfish would have an internal timer of 5,
//            and the second lanternfish would have an internal timer of 7.

// A lanternfish that creates a new fish resets its timer to 6, not 7
// (because 0 is included as a valid timer value).
// The new lanternfish starts with an internal timer of 8 and does not start counting down until the next day.

// Realizing what you're trying to do, the submarine automatically produces a list of
// the ages of several hundred nearby lanternfish (your puzzle input).
// For example, suppose you were given the following list:

// 3,4,3,1,2

// This list means that the first fish has an internal timer of 3,
// the second fish has an internal timer of 4, and so on until the fifth fish,
// which has an internal timer of 2. Simulating these fish over several days would proceed as follows:

// Initial state: 3,4,3,1,2
// After  1 day:  2,3,2,0,1
// After  2 days: 1,2,1,6,0,8
// After  3 days: 0,1,0,5,6,7,8
// After  4 days: 6,0,6,4,5,6,7,8,8
// After  5 days: 5,6,5,3,4,5,6,7,7,8
// After  6 days: 4,5,4,2,3,4,5,6,6,7
// After  7 days: 3,4,3,1,2,3,4,5,5,6
// After  8 days: 2,3,2,0,1,2,3,4,4,5
// After  9 days: 1,2,1,6,0,1,2,3,3,4,8
// After 10 days: 0,1,0,5,6,0,1,2,2,3,7,8
// After 11 days: 6,0,6,4,5,6,0,1,1,2,6,7,8,8,8
// After 12 days: 5,6,5,3,4,5,6,0,0,1,5,6,7,7,7,8,8
// After 13 days: 4,5,4,2,3,4,5,6,6,0,4,5,6,6,6,7,7,8,8
// After 14 days: 3,4,3,1,2,3,4,5,5,6,3,4,5,5,5,6,6,7,7,8
// After 15 days: 2,3,2,0,1,2,3,4,4,5,2,3,4,4,4,5,5,6,6,7
// After 16 days: 1,2,1,6,0,1,2,3,3,4,1,2,3,3,3,4,4,5,5,6,8
// After 17 days: 0,1,0,5,6,0,1,2,2,3,0,1,2,2,2,3,3,4,4,5,7,8
// After 18 days: 6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8

// Each day, a 0 becomes a 6 and adds a new 8 to the end of the list,
// while each other number decreases by 1 if it was present at the start of the day.

// In this example, after 18 days, there are a total of 26 fish.
// After 80 days, there would be a total of 5934.

// Find a way to simulate lanternfish. How many lanternfish would there be after 80 days?

// --- Part Two ---

// Suppose the lanternfish live forever and have unlimited food and space. 
// Would they take over the entire ocean?

// After 256 days in the example above, there would be a total of 26984457539 lanternfish!

// How many lanternfish would there be after 256 days?

use std::fs;
use std::io::{self, BufRead};

const INPUTFN: &str = "input6.txt";

fn main() {
    let file = fs::File::open(INPUTFN).expect(&format!("Cannot find file '{}'\n", INPUTFN));
    let input: Vec<String> = io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect();
    let input2 = input.iter().map(|s| s.as_ref()).collect::<Vec<&str>>();
    println!("Part1: {}", part1(&input2, 80));
    println!("Part2: {}", part2(&input2, 256));
}

fn part1(input: &Vec<&str>, turns: u32) -> i64 {
    let mut state: Vec<u8> = input[0]
                .split(',')
                .map(|s| s.trim()
                          .parse::<u8>()
                          .unwrap())
                .collect();
    // eprintln!("Start: {:?}", state);
    for _ in 0..turns {
        let mut count = 0;
        for n in 0..state.len() {
            if state[n] == 0 {
                count += 1;
                state[n] = 6;
            } else {
                state[n] -= 1;
            }
        }
        for _ in 0..count {
            state.push(8);
        }
    }
    state.len() as i64
}

// Simulating the individual fish timers is easy (see part 1), but explodes (as in part 2).
// Each fish has a same reaction if their timers are equal. So you could count the fish in a certain bin.
// Also each bin repeats after each 7 turns, except for the new ones.
// The new ones, after 2 turn, slide into the 6 bin of the old fish
// So at each turn:
// N0 <- N1
// N1 <- N2
// N2 <- N3
// N3 <- N4 
// N4 <- N5
// N5 <- N6
// N6 <- N7 + N0
// N7 <- N8
// N8 <- N0

fn part2(input: &Vec<&str>, turns: u32) -> u64 {
    let mut bins: Vec<u64> = vec![0; 9];

    let state: Vec<usize> = input[0]
        .split(',')
        .map(|s| s.trim()
              .parse::<usize>()
              .unwrap())
        .collect();
    for n in 0..state.len() {
        bins[state[n]] += 1;
    }
    for _ in 0..turns {
        let mut next: Vec<u64> = vec![0; 9];
        for n in 0..8 {
            next[n] = bins[n+1];
        }
        next[6] += bins[0];
        next[8] = bins[0];
        bins = next
    }
    bins.iter().sum()
}



#[cfg(test)]
mod tests {
    use super::*;


    #[test]
    fn test1a() {
        let input = "3,4,3,1,2";
        let commands: Vec<&str> = input.lines().map(|s| s.trim()).collect();
        assert_eq!(26, part1(&commands, 18));
    }

    #[test]
    fn test1b() {
        let input = "3,4,3,1,2";
        let commands: Vec<&str> = input.lines().map(|s| s.trim()).collect();
        assert_eq!(5934, part1(&commands, 80));
    }

    #[test]
    fn test2() {
        let input = "3,4,3,1,2";
        let commands: Vec<&str> = input.lines().map(|s| s.trim()).collect();
        assert_eq!(26984457539, part2(&commands, 256));
    }

}

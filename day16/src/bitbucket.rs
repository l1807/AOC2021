fn h2d(c: char) -> Option<u8> {
    match c.to_digit(16) {
        Some(v) => Some(v as u8),
        None => None,
    }
}

/// BitBucket handles a bit stream.
/// The capacity is determined by the from_str() input.
#[derive(Debug)]
pub struct BitBucket {
    digits: Vec<u8>, // A bunch of 4-bit digits
    ptr: usize,      // An index into the bit train
}

impl BitBucket {
    /// from_str() creates a BitBucket with the bits according to he inp str
    /// each char yields 4 bits.
    /// There is no way to add bits afterwards
    /// If a non-hex character is encountered the entire operation fails with an Result::Err
    pub fn from_str(inp: &str) -> Result<Self, String> {
        let digits: Vec<u8> = inp.chars().map_while(|c| h2d(c)).collect();
        if digits.len() == inp.len() {
            let ptr = 0;
            Ok(BitBucket { digits, ptr })
        } else {
            Err(format!(
                "Illegal character at position {}",
                inp.len() - digits.len()
            ))
        }
    }

    /// Unconsume all bits: restart consuming from scratch
    pub fn reset(&mut self) {
        self.ptr = 0;
    }

    /// len() returns the number of available bits
    pub fn len(&self) -> usize {
        4 * self.digits.len() - self.ptr
    }

    /// capacity returns the number of available bits after reset()
    pub fn capacity(&self) -> usize {
        4 * self.digits.len()
    }

    /// spent returns the number of bits used up so far
    pub fn spent(&self) -> usize {
        self.ptr
    }

    /// peek() returns the number that corresponds to the next n (n <= 64) bits, without consuming them
    /// Returns None if n > len()
    pub fn peek(&self, mut n: usize) -> Option<u64> {
        let mut ptr = self.ptr;
        if n > self.len() {
            None
        } else {
            let mut result = 0;
            let mut idx = self.ptr / 4;
            if ptr % 4 > 0 {
                for i in (ptr % 4)..4 {
                    let bit = if ((1 << (3 - i)) & self.digits[idx]) == 0 {
                        0
                    } else {
                        1
                    };
                    result = result * 2 + bit;
                    n -= 1;
                    ptr += 1;
                    if n == 0 {
                        return Some(result);
                    }
                }
            }
            // Over here, we ae at a digit boundary
            idx = ptr / 4;
            for _ in 0..n / 4 {
                result = result * 16 + self.digits[idx] as u64;
                idx += 1;
                n -= 4;
                ptr += 4;
            }
            // Last few bits, if any
            for i in 0..n {
                let bit = if ((1 << (3 - i)) & self.digits[idx]) == 0 {
                    0
                } else {
                    1
                };
                result = result * 2 + bit;
                n -= 1;
                ptr += 1;
            }
            Some(result)
        }
    }

    /// get() returns the number that corresponds to the next n (n <= 64) bits, while consuming them
    /// Returns None if n > len()
    pub fn get(&mut self, n: usize) -> Option<u64> {
        let result = self.peek(n)?;
        self.ptr += n;
        Some(result)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn bbtest() {
        let mut bb = BitBucket::from_str("0123456789AbCdEf").unwrap();
        // 0000 0001 0010 0011 0100 0101 0110 0111 1000 1001 1010 1011 1100 1101 1110 1111
        // ---- ---- ---- ---- -- 00 0000 0100 1000 1101 = 0x0048D
        // 1223 3344 4455 555x xxxx xxxx xxxx xxx
        // 0 0   0    4
        assert_eq!(64, bb.len());
        assert_eq!(64, bb.capacity());
        assert_eq!(0x0048D, bb.peek(18).unwrap());
        assert_eq!(0, bb.get(1).unwrap());
        assert_eq!(0, bb.get(2).unwrap());
        assert_eq!(0, bb.get(3).unwrap());
        assert_eq!(4, bb.get(4).unwrap());
        assert_eq!(17, bb.get(5).unwrap());
        assert_eq!(0xa2b3, bb.get(16).unwrap());
        assert_eq!(33, bb.len());
        assert_eq!(64, bb.capacity());
        assert_eq!(31, bb.spent());
        bb.reset();
        assert_eq!(64, bb.len());
        assert_eq!(1, bb.peek(8).unwrap());
        let bb2 = BitBucket::from_str("0123error");
        assert!(bb2.unwrap_err() == "Illegal character at position 4")
    }
}

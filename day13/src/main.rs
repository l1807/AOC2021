// --- Day 13: Transparent Origami ---

// You reach another volcanically active part of the cave.
// It would be nice if you could do some kind of thermal imaging
// so you could tell ahead of time which caves are too hot to safely enter.

// Fortunately, the submarine seems to be equipped with a thermal camera!
// When you activate it, you are greeted with:

// Congratulations on your purchase! To activate this infrared thermal imaging
// camera system, please enter the code found on page 1 of the manual.

// Apparently, the Elves have never used this feature.
// To your surprise, you manage to find the manual; as you go to open it, page 1 falls out.
// It's a large sheet of transparent paper!
// The transparent paper is marked with random dots and
// includes instructions on how to fold it up (your puzzle input).
// For example:

// 6,10
// 0,14
// 9,10
// 0,3
// 10,4
// 4,11
// 6,0
// 6,12
// 4,1
// 0,13
// 10,12
// 3,4
// 3,0
// 8,4
// 1,10
// 2,14
// 8,10
// 9,0

// fold along y=7
// fold along x=5

// The first section is a list of dots on the transparent paper.
// 0,0 represents the top-left coordinate.
// The first value, x, increases to the right. The second value, y, increases downward.
// So, the coordinate 3,0 is to the right of 0,0, and the coordinate 0,7 is below 0,0.
// The coordinates in this example form the following pattern,
// where # is a dot on the paper and . is an empty, unmarked position:

// ...#..#..#.
// ....#......
// ...........
// #..........
// ...#....#.#
// ...........
// ...........
// ...........
// ...........
// ...........
// .#....#.##.
// ....#......
// ......#...#
// #..........
// #.#........

// Then, there is a list of fold instructions.
// Each instruction indicates a line on the transparent paper and
// wants you to fold the paper up (for horizontal y=... lines) or left (for vertical x=... lines).
// In this example, the first fold instruction is fold along y=7,
// which designates the line formed by all of the positions where y is 7 (marked here with -):

// ...#..#..#.
// ....#......
// ...........
// #..........
// ...#....#.#
// ...........
// ...........
// -----------
// ...........
// ...........
// .#....#.##.
// ....#......
// ......#...#
// #..........
// #.#........

// Because this is a horizontal line, fold the bottom half up.
// Some of the dots might end up overlapping after the fold is complete,
// but dots will never appear exactly on a fold line.
// The result of doing this fold looks like this:

// #.##..#..#.
// #...#......
// ......#...#
// #...#......
// .#.#..#.###
// ...........
// ...........

// Now, only 17 dots are visible.

// Notice, for example, the two dots in the bottom left corner before the transparent paper is folded;
// after the fold is complete, those dots appear in the top left corner (at 0,0 and 0,1).
// Because the paper is transparent, the dot just below them in the result (at 0,3) remains visible,
// as it can be seen through the transparent paper.

// Also notice that some dots can end up overlapping; in this case, the dots merge together and become a single dot.

// The second fold instruction is fold along x=5, which indicates this line:

// #.##.|#..#.
// #...#|.....
// .....|#...#
// #...#|.....
// .#.#.|#.###
// .....|.....
// .....|.....

// Because this is a vertical line, fold left:

// #####
// #...#
// #...#
// #...#
// #####
// .....
// .....

// The instructions made a square!

// The transparent paper is pretty big, so for now, focus on just completing the first fold.
// After the first fold in the example above, 17 dots are visible -
// dots that end up overlapping after the fold is completed count as a single dot.

// How many dots are visible after completing just the first fold instruction on your transparent paper?

use std::collections::HashSet; // {HashMap, VecDeque};
use std::fs;
use std::io::{self, BufRead};
use std::time::Instant;

const INPUTFN: &str = "input13.txt";

fn main() {
    let file = fs::File::open(INPUTFN).expect(&format!("Cannot find file '{}'\n", INPUTFN));
    let input: Vec<String> = io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect();
    let input2 = input.iter().map(|s| s.as_ref()).collect::<Vec<&str>>();
    let now = Instant::now();
    println!(
        "Part1: {} in {} ms",
        part1(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
    let now = Instant::now();
    println!(
        "Part2: {} in {} ms",
        part2(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
}

fn part1(input: &Vec<&str>) -> i32 {
    let dots = build_grid(input);
    // eprintln!("Dots: ");
    // for d in dots.iter() {
    //    eprintln!("   ({}, {})", d.x, d.y);
    // }
    let fold_line = &get_fold_data(input)[0];
    let dots2 = fold(&dots, fold_line);
    // eprintln!("\nDots2: ");
    // for d in dots2.iter() {
    //   eprintln!("   ({}, {})", d.x, d.y);
    // }
    dots2.len() as i32
}

fn part2(input: &Vec<&str>) -> i32 {
    let mut dots = build_grid(input);
    let folds = get_fold_data(input);
    for foldline in folds {
        dots = fold(&dots, &foldline);
    }
    print_grid(&dots);
    0
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
struct Point {
    x: isize,
    y: isize,
}

struct Fold {
    along: char,
    offs: isize,
}

fn build_grid(input: &Vec<&str>) -> Vec<Point> {
    let mut result = Vec::new();
    for line in input.iter().take_while(|&s| !s.starts_with("fold")) {
        if let Some((xstr, ystr)) = line.split_once(",") {
            let x = xstr.parse::<isize>().unwrap();
            let y = ystr.parse::<isize>().unwrap();
            result.push(Point { x, y });
        }
    }
    result
}

fn get_fold_data(input: &Vec<&str>) -> Vec<Fold> {
    let mut result = Vec::new();
    for line in input.iter().filter(|&s| s.starts_with("fold")) {
        let (left, right) = line.split_once("=").unwrap();
        let along = left.chars().last().unwrap();
        let offs = right.parse::<isize>().unwrap();
        result.push(Fold { along, offs });
    }
    result
}

fn fold(points: &Vec<Point>, fold: &Fold) -> Vec<Point> {
    let mut result: Vec<Point> = points.iter().map(|p| folded_point(p, fold)).collect();
    result.sort();
    result.dedup();
    result
}

fn folded_point(p: &Point, fold: &Fold) -> Point {
    // eprint!("Folding ({}, {})", p.x, p.y);
    if fold.along == 'x' {
        let x = if p.x < fold.offs {
            p.x
        } else {
            2 * fold.offs - p.x
        };
        let y = p.y;
        // eprintln!(" to ({}, {})", x, y);
        Point { x, y }
    } else if fold.along == 'y' {
        let x = p.x;
        let y = if p.y < fold.offs {
            p.y
        } else {
            2 * fold.offs - p.y
        };
        // eprintln!(" to ({}, {})", x, y);
        Point { x, y }
    } else {
        panic!("Cannot fold along character {}", fold.along)
    }
}

fn print_grid(points: &Vec<Point>) {
    println!("");
    let xsz = 1 + points.iter().map(|p| p.x).max().unwrap();
    let ysz = 1 + points.iter().map(|p| p.y).max().unwrap();
    for y in 0..ysz {
        let mut line = String::with_capacity(xsz as usize);
        let dots: Vec<isize> = points.iter().filter(|p| p.y == y).map(|p| p.x).collect();
        for x in 0..xsz {
            line.push(if dots.contains(&x) { '#' } else { '.' });
        }
        println!("{}", line);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT1: &str = "6,10
    0,14
    9,10
    0,3
    10,4
    4,11
    6,0
    6,12
    4,1
    0,13
    10,12
    3,4
    3,0
    8,4
    1,10
    2,14
    8,10
    9,0
    
    fold along y=7
    fold along x=5";

    #[test]
    fn test1() {
        let commands: Vec<&str> = TEST_INPUT1.lines().map(|s| s.trim()).collect();
        assert_eq!(17, part1(&commands));
    }

    #[test]
    fn test2() {
        let commands: Vec<&str> = TEST_INPUT1.lines().map(|s| s.trim()).collect();
        assert_eq!(17, part2(&commands)); // Has to fail: the stdout counts
    }
}

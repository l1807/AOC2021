// --- Day 23: Amphipod ---

// A group of amphipods notice your fancy submarine and flag you down.
// "With such an impressive shell," one amphipod says,
// "surely you can help us with a question that has stumped our best scientists."

// They go on to explain that a group of timid, stubborn amphipods live in a nearby burrow.
// Four types of amphipods live there: Amber (A), Bronze (B), Copper (C), and Desert (D).
// They live in a burrow that consists of a hallway and four side rooms.
// The side rooms are initially full of amphipods, and the hallway is initially empty.

// They give you a diagram of the situation (your puzzle input),
// including locations of each amphipod (A, B, C, or D,
// each of which is occupying an otherwise open space), walls (#), and open space (.).

// For example:

// #############
// #...........#
// ###B#C#B#D###
//   #A#D#C#A#
//   #########

// The amphipods would like a method to organize every amphipod into side rooms so that
// each side room contains one type of amphipod and the types are sorted A-D going left to right, like this:

// #############
// #...........#
// ###A#B#C#D###
//   #A#B#C#D#
//   #########

// Amphipods can move up, down, left, or right so long as they are moving into an unoccupied open space.
// Each type of amphipod requires a different amount of energy to move one step:
//    Amber amphipods require 1 energy per step,
//    Bronze amphipods require 10 energy,
//    Copper amphipods require 100, and
//    Desert ones require 1000.
//
// The amphipods would like you to find a way to organize the amphipods that requires the least total energy.

// However, because they are timid and stubborn, the amphipods have some extra rules:

// -  Amphipods will never stop on the space immediately outside any room.
//    They can move into that space so long as they immediately continue moving.
//    (Specifically, this refers to the four open spaces in the hallway that are directly above an amphipod starting position.)
// -  Amphipods will never move from the hallway into a room unless
//        that room is their destination room and
//        that room contains no amphipods which do not also have that room as their own destination.
//        If an amphipod's starting room is not its destination room,
//        it can stay in that room until it leaves the room.
//        (For example, an Amber amphipod will not move from the hallway into the right three rooms,
//        and will only move into the leftmost room if that room is empty or if it only contains other Amber amphipods.)
// -  Once an amphipod stops moving in the hallway, it will stay in that spot until it can move into a room.
//    (That is, once any amphipod starts moving, any other amphipods currently in the hallway
//    are locked in place and will not move again until they can move fully into a room.)

// In the above example, the amphipods can be organized using a minimum of 12521 energy. One way to do this is shown below.

// Starting configuration:

// #############
// #...........#
// ###B#C#B#D###
//   #A#D#C#A#
//   #########

// One Bronze amphipod moves into the hallway, taking 4 steps and using 40 energy:

// #############
// #...B.......#
// ###B#C#.#D###
//   #A#D#C#A#
//   #########

// The only Copper amphipod not in its side room moves there, taking 4 steps and using 400 energy:

// #############
// #...B.......#
// ###B#.#C#D###
//   #A#D#C#A#
//   #########

// A Desert amphipod moves out of the way, taking 3 steps and using 3000 energy, and then the Bronze amphipod takes its place, taking 3 steps and using 30 energy:

// #############
// #.....D.....#
// ###B#.#C#D###
//   #A#B#C#A#
//   #########

// The leftmost Bronze amphipod moves to its room using 40 energy:

// #############
// #.....D.....#
// ###.#B#C#D###
//   #A#B#C#A#
//   #########

// Both amphipods in the rightmost room move into the hallway, using 2003 energy in total:

// #############
// #.....D.D.A.#
// ###.#B#C#.###
//   #A#B#C#.#
//   #########

// Both Desert amphipods move into the rightmost room using 7000 energy:

// #############
// #.........A.#
// ###.#B#C#D###
//   #A#B#C#D#
//   #########

// Finally, the last Amber amphipod moves into its room, using 8 energy:

// #############
// #...........#
// ###A#B#C#D###
//   #A#B#C#D#
//   #########

// What is the least energy required to organize the amphipods?

// As you prepare to give the amphipods your solution, you notice that
// the diagram they handed you was actually folded up.
// As you unfold it, you discover an extra part of the diagram.

// Between the first and second lines of text that contain amphipod starting positions, insert the following lines:

//   #D#C#B#A#
//   #D#B#A#C#

// So, the above example now becomes:

// #############
// #...........#
// ###B#C#B#D###
//   #D#C#B#A#
//   #D#B#A#C#
//   #A#D#C#A#
//   #########

// The amphipods still want to be organized into rooms similar to before:

// #############
// #...........#
// ###A#B#C#D###
//   #A#B#C#D#
//   #A#B#C#D#
//   #A#B#C#D#
//   #########

// In this updated example, the least energy required to organize these amphipods is 44169:

// #############
// #...........#
// ###B#C#B#D###
//   #D#C#B#A#
//   #D#B#A#C#
//   #A#D#C#A#
//   #########

// #############
// #..........D#
// ###B#C#B#.###
//   #D#C#B#A#
//   #D#B#A#C#
//   #A#D#C#A#
//   #########

// #############
// #A.........D#
// ###B#C#B#.###
//   #D#C#B#.#
//   #D#B#A#C#
//   #A#D#C#A#
//   #########

// #############
// #A........BD#
// ###B#C#.#.###
//   #D#C#B#.#
//   #D#B#A#C#
//   #A#D#C#A#
//   #########

// #############
// #A......B.BD#
// ###B#C#.#.###
//   #D#C#.#.#
//   #D#B#A#C#
//   #A#D#C#A#
//   #########

// #############
// #AA.....B.BD#
// ###B#C#.#.###
//   #D#C#.#.#
//   #D#B#.#C#
//   #A#D#C#A#
//   #########

// #############
// #AA.....B.BD#
// ###B#.#.#.###
//   #D#C#.#.#
//   #D#B#C#C#
//   #A#D#C#A#
//   #########

// #############
// #AA.....B.BD#
// ###B#.#.#.###
//   #D#.#C#.#
//   #D#B#C#C#
//   #A#D#C#A#
//   #########

// #############
// #AA...B.B.BD#
// ###B#.#.#.###
//   #D#.#C#.#
//   #D#.#C#C#
//   #A#D#C#A#
//   #########

// #############
// #AA.D.B.B.BD#
// ###B#.#.#.###
//   #D#.#C#.#
//   #D#.#C#C#
//   #A#.#C#A#
//   #########

// #############
// #AA.D...B.BD#
// ###B#.#.#.###
//   #D#.#C#.#
//   #D#.#C#C#
//   #A#B#C#A#
//   #########

// #############
// #AA.D.....BD#
// ###B#.#.#.###
//   #D#.#C#.#
//   #D#B#C#C#
//   #A#B#C#A#
//   #########

// #############
// #AA.D......D#
// ###B#.#.#.###
//   #D#B#C#.#
//   #D#B#C#C#
//   #A#B#C#A#
//   #########

// #############
// #AA.D......D#
// ###B#.#C#.###
//   #D#B#C#.#
//   #D#B#C#.#
//   #A#B#C#A#
//   #########

// #############
// #AA.D.....AD#
// ###B#.#C#.###
//   #D#B#C#.#
//   #D#B#C#.#
//   #A#B#C#.#
//   #########

// #############
// #AA.......AD#
// ###B#.#C#.###
//   #D#B#C#.#
//   #D#B#C#.#
//   #A#B#C#D#
//   #########

// #############
// #AA.......AD#
// ###.#B#C#.###
//   #D#B#C#.#
//   #D#B#C#.#
//   #A#B#C#D#
//   #########

// #############
// #AA.......AD#
// ###.#B#C#.###
//   #.#B#C#.#
//   #D#B#C#D#
//   #A#B#C#D#
//   #########

// #############
// #AA.D.....AD#
// ###.#B#C#.###
//   #.#B#C#.#
//   #.#B#C#D#
//   #A#B#C#D#
//   #########

// #############
// #A..D.....AD#
// ###.#B#C#.###
//   #.#B#C#.#
//   #A#B#C#D#
//   #A#B#C#D#
//   #########

// #############
// #...D.....AD#
// ###.#B#C#.###
//   #A#B#C#.#
//   #A#B#C#D#
//   #A#B#C#D#
//   #########

// #############
// #.........AD#
// ###.#B#C#.###
//   #A#B#C#D#
//   #A#B#C#D#
//   #A#B#C#D#
//   #########

// #############
// #..........D#
// ###A#B#C#.###
//   #A#B#C#D#
//   #A#B#C#D#
//   #A#B#C#D#
//   #########

// #############
// #...........#
// ###A#B#C#D###
//   #A#B#C#D#
//   #A#B#C#D#
//   #A#B#C#D#
//   #########

// Using the initial configuration from the full diagram, what is the least energy required to organize the amphipods?

use std::collections::{HashMap, VecDeque}; //HashMap, HashSet, VecDeque};
use std::fmt;
use std::fs;
use std::io::{self, BufRead};
use std::time::Instant;

const INPUTFN: &str = "input23.txt";
const HALWSZ: usize = 11;

fn main() {
    let file = fs::File::open(INPUTFN).expect(&format!("Cannot find file '{}'\n", INPUTFN));
    let input: Vec<String> = io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect();
    let input2 = input.iter().map(|s| s.as_ref()).collect::<Vec<&str>>();
    let now = Instant::now();
    println!(
        "Part1: {} in {} ms",
        part1(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
    let now = Instant::now();
    println!(
        "Part2: {} in {} ms",
        part2(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
}

fn part1(input: &Vec<&str>) -> u64 {
    let spread = Spread::from_input(&input);
    println!("Map:\n{}", spread.map());
    println!("Spread:     {}", spread);
    let endspread = Spread::from_str("...........AABBCCDD");
    println!("End spread: {}", endspread.to_string());
    let energy = move_amphs(&spread, &endspread);
    energy
}

fn part2(input: &Vec<&str>) -> u64 {
    let mut input = input.clone();
    input.insert(3, "  #D#C#B#A#");
    input.insert(4, "  #D#B#A#C#");
    let spread = Spread::from_input(&input);
    println!("Map:\n{}", spread.map());
    println!("Spread:     {}", spread);
    let endspread = Spread::from_str("...........AAAABBBBCCCCDDDD");
    println!("End spread: {}", endspread.to_string());
    let energy = move_amphs(&spread, &endspread);
    energy
}

// The map  constists of
//    1 Hall (y = 0, x = 0..=10) and
//    4 Siderooms at position y = 1 and 2, x = 2, 4, 6 or 8, for A, B, C and  respectively
// according to the destined amphs for that sideroom

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
enum Amph {
    A,
    B,
    C,
    D,
}

impl Amph {
    fn to_char(amph: Option<Self>) -> char {
        match amph {
            Some(Amph::A) => 'A',
            Some(Amph::B) => 'B',
            Some(Amph::C) => 'C',
            Some(Amph::D) => 'D',
            None => '.',
        }
    }

    fn from_c(c: char) -> Option<Self> {
        match c {
            'A' => Some(Amph::A),
            'B' => Some(Amph::B),
            'C' => Some(Amph::C),
            'D' => Some(Amph::D),
            _ => None,
        }
    }
}

//type Spread = [Option<Amph>; HALWSZ + 8];
#[derive(Hash, Eq, PartialEq, Clone)]
struct Spread {
    pos: Vec<Option<Amph>>,
    sidesz: usize,
    hallsz: usize,
    sidex: Vec<u8>,
}

// Place describes a location on the map.
#[derive(Debug)]
struct Place {
    x: u8,
    y: u8,
}

// A Spread is an struct with a hallway and 4 sidechambers
// The hall has HALWSZ positions, the the Sidechambers positons: (2,1), (2,2), (4.1), 4, 2) etc.
// In each position, there is an Option<Amph> where a an amph may be sitting.
// Spreads can be tested for equality, and are equal when amphs of the same kind are switched.
// A Spread is like a node in a maze. The difficult part is finding the legal successors to a certain Spread
impl Spread {
    // Return a completely empty but well-sized theater
    fn new(sidesz: usize) -> Self {
        Spread {
            pos: vec![None; HALWSZ + 4 * sidesz],
            sidesz,
            hallsz: HALWSZ,
            sidex: vec![2, 4, 6, 8],
        }
    }

    // As input, the spread has an empty halway
    fn from_input(input: &Vec<&str>) -> Self {
        let sidesz = input.len() - 3; // top, halway,  bottom of sides
        let mut spread = Spread::new(sidesz);
        let mut inp = input.iter();
        inp.next(); // top wall
        inp.next(); // hallway: should be empty

        for y in 0..sidesz {
            let line: Vec<char> = inp.next().unwrap().chars().collect(); // sidechambers row y
            for (n, &x) in [3, 5, 7, 9].iter().enumerate() {
                match line[x] {
                    'A' | 'B' | 'C' | 'D' => {
                        let amph = Amph::from_c(line[x]).unwrap();
                        spread.pos[spread.hallsz + n * sidesz + y] = Some(amph)
                    }
                    _ => (),
                };
            }
        }
        spread
    }

    // Same as from_inp, but coming from a simpler string
    // "..........AABBCCDD" would be a valid end spread
    fn from_str(inp: &str) -> Self {
        let sidesz = (inp.len() - HALWSZ) / 4;
        let mut spread = Spread::new(sidesz);
        for (n, c) in inp.chars().enumerate() {
            let amph = Amph::from_c(c);
            spread.pos[n] = amph;
        }
        spread
    }

    // Return a spread as a map
    fn map(&self) -> String {
        let string1 = self.pos[..HALWSZ]
            .iter()
            .map(|a| Amph::to_char(*a))
            .collect::<String>();
        let mut sidelines: Vec<String> = vec![];
        for ps in 0..self.sidesz {
            let mut string2 = String::new();
            for side in 0..4 {
                string2.push_str(&format!(
                    " {}",
                    Amph::to_char(self.pos[HALWSZ + side * self.sidesz + ps])
                ));
            }
            sidelines.push(string2);
        }
        let result = format!("{}\n {}", string1, sidelines.join("\n "));
        result
    }

    // Make the spread printable as a concise string
    fn to_string(&self) -> String {
        self.pos.iter().map(|a| Amph::to_char(*a)).collect()
    }

    fn iter(&self) -> std::slice::Iter<'_, Option<Amph>> {
        self.pos.iter()
    }

    fn len(&self) -> usize {
        self.pos.len()
    }

    fn place(&mut self, idx: usize, amph: Amph) {
        self.pos[idx] = Some(amph);
    }

    fn unplace(&mut self, idx: usize) {
        self.pos[idx] = None;
    }

    fn x(&self, pos: usize) -> usize {
        if pos < HALWSZ {
            pos
        } else {
            [2, 4, 6, 8][(pos - HALWSZ) / self.sidesz]
        }
    }

    fn y(&self, pos: usize) -> usize {
        if pos < HALWSZ {
            0
        } else {
            1 + (pos - HALWSZ) % self.sidesz
        }
    }
}

impl fmt::Display for Spread {
    // Return a spread as a map
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.to_string())
    }
}

impl std::ops::Index<usize> for Spread {
    type Output = Option<Amph>;

    fn index(&self, idx: usize) -> &Self::Output {
        &self.pos[idx]
    }
}

// Moving amphs into the target spread is like solving a maze
// The start spread is the initial position
// For each Amph position, all legal next positions are nodes adjacent to the current position,
// with a cost decided by the energy cost for the move
// The goal is to find the least cost route to the target spread, using a classic Dijkstra search

// For every generated spread (attempted move), it is checked if it is in visited.
// If not, it is inserted, with a new id, and the pred_id and the cost for the move is added to visited_data
struct VisitedData {
    pred_id: usize,
    cost: u64,
}

struct MazeState {
    spread: Spread,
    id: usize, // An index into visited where the predecessor and the min cost is stored
}

fn move_amphs(start: &Spread, trgt: &Spread) -> u64 {
    let mut visited = HashMap::<Spread, usize>::new();
    let mut visited_data: Vec<VisitedData> = Vec::new();
    let mut que = VecDeque::<MazeState>::new();
    let mut target_id = 0;

    // handle the start spread as a legal position
    visited.insert(start.clone(), visited_data.len());
    visited_data.push(VisitedData {
        pred_id: 0,
        cost: 0,
    });
    que.push_back(MazeState {
        spread: start.clone(),
        id: visited[start],
    });

    while let Some(mazestate) = que.pop_front() {
        // The visited_data of the spread in mazestate is known
        let spread = mazestate.spread;
        let state_id = mazestate.id;
        let pred_id = visited_data[state_id].pred_id;
        let cost0 = visited_data[state_id].cost;
        // println!(
        //     "Looking at {}: {} / {}: pre: {}",
        //     state_id, spread, cost0, pred_id
        // );

        if spread == *trgt {
            // This is a final position: record the id
            target_id = state_id;
            println!("Target! @cost {cost0}");
            continue; // No more moves from here
        }
        for (pos, amph_opt) in spread.iter().enumerate() {
            if let Some(amph) = amph_opt {
                for newpos in 0..spread.len() {
                    if is_legal_move(&spread, amph, pos, newpos, trgt) {
                        // print!("Moving {pos} to {newpos}  -> ");
                        let cost = cost0 + find_cost(&spread, pos, newpos, amph);
                        let mut new_spread = spread.clone();
                        new_spread.unplace(pos);
                        new_spread.place(newpos, *amph);
                        // println!("{new_spread}");
                        if visited.contains_key(&new_spread) {
                            // spread seen before
                            let id = visited[&new_spread];
                            let old_data = &mut visited_data[id];
                            let oldcost = old_data.cost;
                            // print!(
                            //     "{spread}: {} from {pos} to {newpos} --> known {id}: {new_spread} / cost ",
                            //     Amph::to_char(Some(*amph)),
                            // );
                            if oldcost > cost {
                                old_data.cost = cost;
                                old_data.pred_id = state_id;
                                // println!(" updated: {cost} < {oldcost}")
                                // } else {
                                //     println!(" is larger: {cost} > {oldcost}",);
                            }
                        } else {
                            // new spread
                            let new_data = VisitedData {
                                pred_id: state_id,
                                cost: cost,
                            };
                            let new_id = visited_data.len();
                            visited.insert(new_spread.clone(), new_id);
                            visited_data.push(new_data);
                            que.push_back(MazeState {
                                spread: new_spread,
                                id: new_id,
                            });
                            // println!(
                            //     "{}: {} from {} to {} --> new {}: {} / {}",
                            //     spread,
                            //     Amph::to_char(Some(*amph)),
                            //     pos,
                            //     newpos,
                            //     new_id,
                            //     new_spread,
                            //     cost
                            // );
                        }
                    }
                }
            }
        }
    }
    visited_data[target_id].cost
}

// - An Amph can move from (x0, y0) to (x1, y1):
//   - from a side chamber (y0 != 0) to points 0, 1, 3, 5, 7, 9, 10 in the halway (y1 == 0)
//   - from the halway (y0 == 0) to a target side chamber (y1 != 0),
//      but only if no strange amph is sitting on y=2
// - (x1, y1) must be empty
// - the cost is (|x1-x0| + |y1-y0|) * energy(Amph)
fn is_legal_move(
    spread: &Spread,
    amph: &Amph,
    pos: usize,
    new_pos: usize,
    endspread: &Spread,
) -> bool {
    // println!("Assessing: '{}': {} -> {}", spread, pos, new_pos);
    if spread[new_pos].is_some() {
        // println!("X: new_pos occupied");
        return false; // Cannot move to an occupied position (including the old pos)
    }
    if pos < HALWSZ {
        // Starting from the hallway
        if new_pos < HALWSZ {
            // println!("X: from hallway to hallway");
            return false; // Cannot move from hallway to hallway
        };
        if endspread[new_pos].unwrap() != *amph {
            // println!("X: wrong sidechamber");
            return false; // Can only move to final side chamber
        };
        let sidech0 = HALWSZ + (new_pos - HALWSZ) / spread.sidesz * spread.sidesz;
        for p in sidech0..sidech0 + spread.sidesz {
            if let Some(amph2) = spread[p] {
                if amph2 != *amph {
                    // println!(
                    //     "X: Some position in this target side chamber @ {sidech0} contains the wrong amph type"
                    // );
                    return false; // Some position in this target side chamber contains the wrong amph type
                }
            }
        }
    } else {
        // Moving out of a side chamber
        if new_pos >= HALWSZ {
            // println!("X: from sidechamber to sidechamber");
            return false; // Cannot move from sidechamber to sidechamber
        };
        if [2, 4, 6, 8].iter().any(|&p| p == new_pos) {
            // println!("X: just above side chamber");
            return false; // must move to legal position
        };
        if endspread[pos].unwrap() == *amph {
            // amph already in right end room
            let sidech = (pos - HALWSZ) / spread.sidesz;
            let mut stay = true;
            for p in pos + 1..HALWSZ + (sidech + 1) * spread.sidesz {
                if p >= spread.len() {
                    break; // in last position: stay == true
                }
                if spread[p].is_none() || spread[p].unwrap() != *amph {
                    stay = false; // room to move further down, or an alien present
                    break;
                }
            }
            if stay {
                // println!("X: already in end position");
                return false; // Already in end position
            }
        }
    }
    return !is_blocked(spread, pos, new_pos);
    // let blocked = is_blocked(spread, pos, new_pos);
    // if blocked {
    //     println!("X: blocked")
    // }
    // return !blocked;
}

// - no other Amph may be blocking. This is the case if
//   - the sidechamber is occupied for smaller positions then pos
//   - the sidechamber is occupied for smaller positions then new_pos
//   - (x, 0) is occupied where x1 < x < x0 or x0 < x < x1
fn is_blocked(spread: &Spread, pos: usize, new_pos: usize) -> bool {
    // If pos has y == n: see if corresponding y == 1..n is blocking the exit of the sidech
    if pos > HALWSZ {
        let side0 = HALWSZ + spread.sidesz * ((pos - HALWSZ) / spread.sidesz);
        if (side0..pos).any(|p| spread[p].is_some()) {
            // println!("B: exit of sidechamber is blocked");
            return true; // exit of sidechamber is blocked
        }
    }
    if new_pos > HALWSZ {
        let side0 = HALWSZ + spread.sidesz * ((new_pos - HALWSZ) / spread.sidesz);
        if (side0..new_pos).any(|p| spread[p].is_some()) {
            // println!("B: entrance to side chamber is blocked");
            return true; // entrance to side chamber position is blocked
        }
    }
    // Over here we can move in the hallway. The x is the same as the hallway position
    let x0 = spread.x(pos);
    let x1 = spread.x(new_pos);
    let (x0, x1) = if x1 < x0 { (x1, x0) } else { (x0, x1) }; // x0 is the smaller one
    let blocked = ((x0 + 1)..x1).any(|x| spread[x as usize].is_some());
    // if blocked {
    //     println!("B: amph in halway between {x0} and {x1}");
    // }
    blocked
}

fn find_cost(spread: &Spread, pos: usize, new_pos: usize, amph: &Amph) -> u64 {
    let energy = match amph {
        Amph::A => 1,
        Amph::B => 10,
        Amph::C => 100,
        Amph::D => 1000,
    };
    let p0x = spread.x(pos) as isize;
    let p0y = spread.y(pos) as isize;
    let p1x = spread.x(new_pos) as isize;
    let p1y = spread.y(new_pos) as isize;

    let dx = (p0x - p1x).abs();
    let dy = (p0y - p1y).abs();
    (energy * (dx + dy)) as u64
}

mod tests {
    use super::*;

    const TEST_INPUT1: &str = "\
#############
#...........#
###B#C#B#D###
  #A#D#C#A#
  #########
";

    const TEST_INPUT2: &str = "\
";

    #[test]
    fn test1a() {
        let start_map: Vec<&str> = TEST_INPUT1
            .lines()
            .filter(|s| s.trim().len() != 0)
            .collect();
        println!("{:#?}", start_map);
        assert_eq!(12521, part1(&start_map));
    }

    #[test]
    fn test2() {
        let start_map: Vec<&str> = TEST_INPUT1
            .lines()
            .filter(|s| s.trim().len() != 0)
            .collect();
        println!("{:#?}", start_map);
        assert_eq!(44169, part2(&start_map));
    }
}

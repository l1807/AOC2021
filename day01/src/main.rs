// As the submarine drops below the surface of the ocean, it automatically performs a sonar sweep of the nearby sea floor.
// On a small screen, the sonar sweep report (your puzzle input) appears:
// each line is a measurement of the sea floor depth as the sweep looks further and further away from the submarine.

// For example, suppose you had the following report:

// 199
// 200
// 208
// 210
// 200
// 207
// 240
// 269
// 260
// 263

// This report indicates that, scanning outward from the submarine, the sonar sweep found depths of 199, 200, 208, 210, and so on.
// The first order of business is to figure out how quickly the depth increases, just so you know what you're dealing with -
// you never know if the keys will get carried into deeper water by an ocean current or a fish or something.

// To do this, count the number of times a depth measurement increases from the previous measurement.
// (There is no measurement before the first measurement.) In the example above, the changes are as follows:

// 199 (N/A - no previous measurement)
// 200 (increased)
// 208 (increased)
// 210 (increased)
// 200 (decreased)
// 207 (increased)
// 240 (increased)
// 269 (increased)
// 260 (decreased)
// 263 (increased)

// In this example, there are 7 measurements that are larger than the previous measurement.

// How many measurements are larger than the previous measurement?

use std::fs;

const INPUTFN: &str = "input1.txt";

fn main() {
    let input = fs::read_to_string(INPUTFN).expect(&format!("Cannot find file '{}'\n", INPUTFN));
    let numbers = input
        .split_whitespace()
        .map(|s| s.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    println!("Part1: {}", part1(&numbers));
    println!("Part2: {}", part2(&numbers));
}

fn part1(numbers: &Vec<usize>) -> usize {
    numbers[..].windows(2).filter(|w| w[1] > w[0]).count()
}

fn part2(numbers: &Vec<usize>) -> usize {
    let sums = numbers[..]
        .windows(3)
        .map(|w| w[0] + w[1] + w[2])
        .collect::<Vec<usize>>();
    part1(&sums)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        let numbers: Vec<usize> = vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263];
        assert_eq!(7, part1(&numbers));
    }

    #[test]
    fn test2() {
        let numbers: Vec<usize> = vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263];
        assert_eq!(5, part2(&numbers));
    }
}

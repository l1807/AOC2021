// --- Day 17: Trick Shot ---

// You finally decode the Elves' message. HI, the message says.
// You continue searching for the sleigh keys.

// Ahead of you is what appears to be a large ocean trench.
// Could the keys have fallen into it? You'd better send a probe to investigate.

// The probe launcher on your submarine can fire the probe with any integer velocity
// in the x (forward) and y (upward, or downward if negative) directions.
// For example, an initial x,y velocity like 0,10 would fire the probe straight up,
// while an initial velocity like 10,-1 would fire the probe forward at a slight downward angle.

// The probe's x,y position starts at 0,0. Then, it will follow some trajectory by moving in steps.
// On each step, these changes occur in the following order:

//     The probe's x position increases by its x velocity.
//     The probe's y position increases by its y velocity.
//     Due to drag, the probe's x velocity changes by 1 toward the value 0; that is,
//          it decreases by 1 if it is greater than 0,
//             increases by 1 if it is less than 0, or
//             does not change if it is already 0.
//     Due to gravity, the probe's y velocity decreases by 1.

// For the probe to successfully make it into the trench,
// the probe must be on some trajectory that causes it to be within a target area after any step.
// The submarine computer has already calculated this target area (your puzzle input). For example:

// target area: x=20..30, y=-10..-5

// This target area means that you need to find initial x,y velocity values such that after any step,
// the probe's x position is at least 20 and at most 30,
// and the probe's y position is at least -10 and at most -5.

// Given this target area, one initial velocity that causes the probe
// to be within the target area after any step is 7,2:

// .............#....#............
// .......#..............#........
// ...............................
// S........................#.....
// ...............................
// ...............................
// ...........................#...
// ...............................
// ....................TTTTTTTTTTT
// ....................TTTTTTTTTTT
// ....................TTTTTTTT#TT
// ....................TTTTTTTTTTT
// ....................TTTTTTTTTTT
// ....................TTTTTTTTTTT

// In this diagram, S is the probe's initial position, 0,0.
// The x coordinate increases to the right, and the y coordinate increases upward.
// In the bottom right, positions that are within the target area are shown as T.
// After each step (until the target area is reached), the position of the probe is marked with #.
// (The bottom-right # is both a position the probe reaches and a position in the target area.)

// Another initial velocity that causes the probe to be within the target area after any step is 6,3:

// ...............#..#............
// ...........#........#..........
// ...............................
// ......#..............#.........
// ...............................
// ...............................
// S....................#.........
// ...............................
// ...............................
// ...............................
// .....................#.........
// ....................TTTTTTTTTTT
// ....................TTTTTTTTTTT
// ....................TTTTTTTTTTT
// ....................TTTTTTTTTTT
// ....................T#TTTTTTTTT
// ....................TTTTTTTTTTT

// Another one is 9,0:

// S........#.....................
// .................#.............
// ...............................
// ........................#......
// ...............................
// ....................TTTTTTTTTTT
// ....................TTTTTTTTTT#
// ....................TTTTTTTTTTT
// ....................TTTTTTTTTTT
// ....................TTTTTTTTTTT
// ....................TTTTTTTTTTT

// One initial velocity that doesn't cause the probe to be within the target area after any step is 17,-4:

// S..............................................................
// ...............................................................
// ...............................................................
// ...............................................................
// .................#.............................................
// ....................TTTTTTTTTTT................................
// ....................TTTTTTTTTTT................................
// ....................TTTTTTTTTTT................................
// ....................TTTTTTTTTTT................................
// ....................TTTTTTTTTTT..#.............................
// ....................TTTTTTTTTTT................................
// ...............................................................
// ...............................................................
// ...............................................................
// ...............................................................
// ................................................#..............
// ...............................................................
// ...............................................................
// ...............................................................
// ...............................................................
// ...............................................................
// ...............................................................
// ..............................................................#

// The probe appears to pass through the target area, but is never within it after any step.
// Instead, it continues down and to the right - only the first few steps are shown.

// If you're going to fire a highly scientific probe out of a super cool probe launcher,
//  you might as well do it with style. How high can you make the probe go while still reaching the target area?

// In the above example, using an initial velocity of 6,9 is the best you can do,
// causing the probe to reach a maximum y position of 45.
// (Any higher initial y velocity causes the probe to overshoot the target area entirely.)

// Find the initial velocity that causes the probe to reach the highest y position
// and still eventually be within the target area after any step.
// What is the highest y position it reaches on this trajectory?

// --- Part Two ---

// Maybe a fancy trick shot isn't the best idea; after all,
// you only have one probe, so you had better not miss.

// To get the best idea of what your options are for launching the probe,
// you need to find every initial velocity that causes the probe to
// eventually be within the target area after any step.

// In the above example, there are 112 different initial velocity values that meet these criteria:

// 23,-10  25,-9   27,-5   29,-6   22,-6   21,-7   9,0     27,-7   24,-5
// 25,-7   26,-6   25,-5   6,8     11,-2   20,-5   29,-10  6,3     28,-7
// 8,0     30,-6   29,-8   20,-10  6,7     6,4     6,1     14,-4   21,-6
// 26,-10  7,-1    7,7     8,-1    21,-9   6,2     20,-7   30,-10  14,-3
// 20,-8   13,-2   7,3     28,-8   29,-9   15,-3   22,-5   26,-8   25,-8
// 25,-6   15,-4   9,-2    15,-2   12,-2   28,-9   12,-3   24,-6   23,-7
// 25,-10  7,8     11,-3   26,-7   7,1     23,-9   6,0     22,-10  27,-6
// 8,1     22,-8   13,-4   7,6     28,-6   11,-4   12,-4   26,-9   7,4
// 24,-10  23,-8   30,-8   7,0     9,-1    10,-1   26,-5   22,-9   6,5
// 7,5     23,-6   28,-10  10,-2   11,-1   20,-9   14,-2   29,-7   13,-3
// 23,-5   24,-8   27,-9   30,-7   28,-5   21,-10  7,9     6,6     21,-5
// 27,-10  7,2     30,-9   21,-8   22,-7   24,-9   20,-6   6,9     29,-5
// 8,-2    27,-8   30,-5   24,-7

// How many distinct initial velocity values cause the probe to be within the target area after any step?

use std::fs;
use std::io::{self, BufRead};
use std::time::Instant;

const INPUTFN: &str = "input17.txt";

fn main() {
    let file = fs::File::open(INPUTFN).expect(&format!("Cannot find file '{}'\n", INPUTFN));
    let input: Vec<String> = io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect();
    let input2 = input.iter().map(|s| s.as_ref()).collect::<Vec<&str>>();
    let now = Instant::now();
    println!(
        "Part1: {} in {} ms",
        part1(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
    let now = Instant::now();
    println!(
        "Part2: {} in {} ms",
        part2(&input2),
        now.elapsed().as_micros() as f64 / 1000.0
    );
}

fn part1(input: &Vec<&str>) -> i64 {
    println!("{}", input[0]);
    let target = Target::from_str(input[0]);
    search_highest_path(&target)
}

fn part2(input: &Vec<&str>) -> i64 {
    let target = Target::from_str(input[0]);
    search_all_shots(&target)
}

#[derive(PartialEq, Debug)]
struct Point {
    x: i64,
    y: i64,
}

struct Probe {
    x: i64,
    y: i64,
    vx: i64,
    vy: i64,
    traj: Vec<Point>,
}

struct Target {
    tl: Point,
    br: Point,
}

/// Search for a trajectory hitting the target with the highesst intermediate altitude
/// Do this by making a new probe with an different initial velocity every round.
/// A equivalent x, y velocity probably makes for the widest trajectoy.i64
/// If the x is over the target, the attempt stops usuccessfully
/// If the y is under the target, the attempt stops unsuccessfully
/// If the probe's x never is in the target's range, the velocity is too low.
/// This happens if 1 + 2 + 3 + .... +  x < target.minx
/// IF the probe's x goes around the targets range, the velocity is too high, or aimed too low
/// If the probe goes around the x target range, the aim must be higher
/// IF the probe goes around the y target range, the ymax is too high: reduce velocity, then aim higher
/// If the target is hit, try aiming higher, and then increase velocity

fn search_highest_path(target: &Target) -> i64 {
    let mut high = 0;
    let minx = (1..target.tl.x)
        .find(|x| (0..*x).sum::<i64>() >= target.tl.x)
        .unwrap();
    'outer: for vx in minx - 1..10000 {
        for vy in 0..1000 {
            let result = Probe::launch(&target, vx, vy);
            let h = result.traj.iter().map(|p| p.y).max().unwrap();
            let last = result.traj.iter().last().unwrap();
            let ok = target.inside(&last);
            if ok {
                high = high.max(h);
            }
            //if ok {
            println!(
                "{}, {} -> {}, {:?} {}",
                vx,
                vy,
                high,
                last,
                if ok { "!" } else { "X" }
            );
            //}
            if !ok && target.over(last) {
                println!("x overshoot at vx = {}", result.vx);
                if result.traj.len() == 1 {
                    return high; // Overshoot in 1 step: done.
                }
                if result.vx == 0 && target.under(last) {
                    break;
                }
                break; // Overshoot after several steps: just try next y
            }
        }
    }
    high
}

fn search_all_shots(target: &Target) -> i64 {
    let mut hit = 0;
    let minx = (1..target.tl.x)
        .find(|x| (0..*x).sum::<i64>() >= target.tl.x)
        .unwrap();
    for vx in minx - 2..10000 {
        for vy in -1000..1000 {
            let result = Probe::launch(&target, vx, vy);
            let h = result.traj.iter().map(|p| p.y).max().unwrap();
            let last = result.traj.iter().last().unwrap();
            let ok = target.inside(&last);
            if ok {
                hit += 1;
            }
            if ok {
                println!(
                    "{}, {} -> {}, {:?} {}",
                    vx,
                    vy,
                    hit,
                    last,
                    if ok { "!" } else { "X" }
                );
            }
            if !ok && target.over(last) {
                println!("x overshoot");
                if result.traj.len() == 1 {
                    return hit; // Overshoot in 1 step: done.
                }
                break; // Overshoot after several steps: just try next y
            }
        }
    }
    hit
}

impl Target {
    fn from_str(input: &str) -> Self {
        let (xrange, yrange) = input["target area: ".len()..].split_once(", ").unwrap();
        let (xmin, xmax) = xrange["x=".len()..].split_once("..").unwrap();
        let (ymin, ymax) = yrange["y=".len()..].split_once("..").unwrap();
        Target {
            tl: Point {
                x: xmin.parse().unwrap(),
                y: ymax.parse().unwrap(),
            },
            br: Point {
                x: xmax.parse().unwrap(),
                y: ymin.parse().unwrap(),
            },
        }
    }

    fn inside(&self, pos: &Point) -> bool {
        pos.x <= self.br.x && pos.x >= self.tl.x && pos.y <= self.tl.y && pos.y >= self.br.y
    }

    fn under(&self, pos: &Point) -> bool {
        pos.y < self.br.y
    }

    fn over(&self, pos: &Point) -> bool {
        pos.x > self.br.x
    }
}

impl Probe {
    fn get_pos(&self) -> Point {
        Point {
            x: self.x,
            y: self.y,
        }
    }

    /// launch() launches a probe with the given x and y velocity and
    /// follows the probe over its trajectory.
    /// It does this by calling do_Step, updating the probe state, and then see if the
    /// trajectory needs to be aborted. In the mean time the reached positions are recorded.
    /// At the end a vecor of reached points is returned
    fn launch(target: &Target, vx: i64, vy: i64) -> Self {
        let mut probe = Probe {
            x: 0,
            y: 0,
            vx,
            vy,
            traj: vec![],
        };
        let mut stop_test = false;
        probe.traj.push(probe.get_pos());
        while !stop_test {
            probe.do_step();
            let p = probe.get_pos();
            stop_test = target.over(&p) || target.under(&p) || target.inside(&p);
            probe.traj.push(p);
        }
        probe
    }

    fn do_step(&mut self) {
        self.x += self.vx;
        self.y += self.vy;
        self.vx += if self.vx > 0 {
            -1
        } else if self.vx < 0 {
            1
        } else {
            0
        };
        self.vy -= 1;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT1: &str = "target area: x=20..30, y=-10..-5";

    #[test]
    fn test1() {
        let commands: Vec<&str> = TEST_INPUT1.lines().map(|s| s.trim()).collect();
        assert_eq!(45, part1(&commands));
    }

    #[test]
    fn trajectory1() {
        let target =
            Target::from_str(TEST_INPUT1.lines().map(|s| s.trim()).collect::<Vec<&str>>()[0]);
        let result = Probe::launch(&target, 7, 2);
        let pl = &result.traj[result.traj.len() - 1];
        assert_eq!(&Point { x: 28, y: -7 }, pl)
    }

    #[test]
    fn trajectory2() {
        let target =
            Target::from_str(TEST_INPUT1.lines().map(|s| s.trim()).collect::<Vec<&str>>()[0]);
        let result = Probe::launch(&target, 6, 3);
        let pl = &result.traj[result.traj.len() - 1];
        assert_eq!(&Point { x: 21, y: -9 }, pl)
    }

    #[test]
    fn trajectory3() {
        let target =
            Target::from_str(TEST_INPUT1.lines().map(|s| s.trim()).collect::<Vec<&str>>()[0]);
        let result = Probe::launch(&target, 9, 0);
        let pl = &result.traj[result.traj.len() - 1];
        assert_eq!(&Point { x: 30, y: -6 }, pl)
    }

    #[test]
    fn trajectory4() {
        let target =
            Target::from_str(TEST_INPUT1.lines().map(|s| s.trim()).collect::<Vec<&str>>()[0]);
        let result = Probe::launch(&target, 17, -4);
        let pl = &result.traj[result.traj.len() - 1];
        assert_eq!(&Point { x: 33, y: -9 }, pl)
    }

    #[test]
    fn trajectory5() {
        let target =
            Target::from_str(TEST_INPUT1.lines().map(|s| s.trim()).collect::<Vec<&str>>()[0]);
        let result = Probe::launch(&target, 6, 9);
        println!("{:?}", result.traj);
        let high = result.traj.iter().map(|p| p.y).max().unwrap();
        assert_eq!(45, high)
    }

    #[test]
    fn test2() {
        let commands: Vec<&str> = TEST_INPUT1.lines().map(|s| s.trim()).collect();
        assert_eq!(112, part2(&commands));
    }
}
